﻿using UnityEngine;
using System.Collections;

public class AudioFadeOut : MonoBehaviour {

	public float timespan;
	//public Component image;
	private float initialTimespan;
	private float initialVolume;
	private AudioSource audioS;
	// Use this for initialization
	void Start () {
		initialTimespan = timespan;
		audioS = gameObject.GetComponent<AudioSource>();
		initialVolume = audioS.volume;
		//initialAlpha = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		timespan -= Time.deltaTime;
		float newVolume =  (timespan / initialTimespan) * initialVolume;
		audioS.volume = newVolume;
		
		if (timespan < 0) Destroy (this);
	}
}
