﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeOut : MonoBehaviour {

	public float timespan;
	//public Component image;
	private float initialTimespan;
	private Color initialColor;
	public Image PanelFade;
	// Use this for initialization
	void Start () {
		initialTimespan = timespan;

		initialColor = PanelFade.color;
	}
	
	// Update is called once per frame
	void Update () {
		timespan -= Time.deltaTime;
		Color newColor = new Color(initialColor.r, initialColor.g, initialColor.b, (timespan / initialTimespan) * initialColor.a);
        PanelFade.color = newColor;

        if (timespan < 0) Destroy (this);
	}
}
