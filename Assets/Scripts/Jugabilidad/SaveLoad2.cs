﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SaveLoad2 : MonoBehaviour {

    public PlayerHealthController PHC;
    public PlayerMovementAndroid PMA;
    public PlayerMovement PM;
	// Update is called once per frame
	void Update () {
        if (PHC.GetHealth() <= 0.0f)
        {
            Invoke("ReloadCurrentLevel", 3.0f);
            //PMA.speed = 0f;
            PM.speed = 0.0f;
        }

        if (Input.GetKey("f11")){
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }

    private void ReloadCurrentLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
