﻿using UnityEngine;
using System.Collections;

public abstract class SteeringAI : MonoBehaviour {
    [SerializeField]
    protected float mass = 1f;

    [SerializeField]
    protected float maxVelocity = 1f;

    [SerializeField]
    protected float maxForce = 0.1f;

    [SerializeField]
    protected bool blockRotation;

    protected Vector3 velocity;

    protected Vector3 acceleration;
    protected Vector3 steeringForce;
    protected Transform myTransform;

    private float scaleSign = 1;

    public virtual void Awake()
    {
        myTransform = transform;
    }

    public virtual void Start() {

    }

    public virtual void OnDisable()
    {
        StopAllCoroutines();
    }

    public virtual void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(SteeringFixedUpdate());
        StartCoroutine(SteeringUpdate());
    }

    IEnumerator SteeringFixedUpdate () {
        while (true)
        {
            steeringForce = steeringForce.Truncate(maxForce);
            acceleration = steeringForce / mass;
            velocity = (velocity + acceleration).Truncate(maxVelocity);

            myTransform.position += velocity * Time.fixedDeltaTime;

            yield return new WaitForFixedUpdate();
        }
    }


    IEnumerator SteeringUpdate()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
        }

    }

#region Behaviours
/// <summary>
/// Seek behaviour
/// </summary>
/// <param name="target">Target position</param>
/// <returns></returns>
    protected Vector3 Seek(Vector3 target)
    {
        return (target - myTransform.position).normalized * maxVelocity;
    }

    /// <summary>
    /// Flee behaviour
    /// </summary>
    /// <param name="target">Target position</param>
    /// <returns></returns>
    protected Vector3 Flee(Vector3 target)
    {
        return (myTransform.position - target).normalized * maxVelocity;
    }

    /// <summary>
    /// Flee behaviour with anticipation
    /// </summary>
    /// <param name="target">Target position</param>
    /// <param name="targetVelocity">Target velocity</param>
    /// <param name="T">Anticipation weight</param>
    /// <returns></returns>
    protected Vector3 Pursuit(Vector3 target, Vector3 targetVelocity, float T)
    {
        return Seek(target + targetVelocity * T);
    }

    /// <summary>
    /// Seek with anticipation
    /// </summary>
    /// <param name="target">Target position</param>
    /// <param name="targetVelocity">Target velocity</param>
    /// <param name="T">Anticipation weight</param>
    /// <returns></returns>
    protected Vector3 Evade(Vector3 target, Vector3 targetVelocity, float T)
    {
        return Flee(target + targetVelocity * T);
    }

    /// <summary>
    /// Seek with arrival smooth.
    /// </summary>
    /// <param name="target">Target position</param>
    /// <param name="radius">Smooth radius</param>
    /// <returns></returns>
    protected Vector3 Arrival(Vector3 target, float radius)
    {
        float distance = (target - myTransform.position).magnitude;
        if (distance < radius)
            return Seek(target) * (distance / radius);
        else
            return Seek(target);
    }

    /// <summary>
    /// Flocking Behaviour.
    /// </summary>
    /// <param name="neighbors">neighbours list</param>
    /// <param name="radius">action radius</param>
    /// <param name="alignmentWeight"></param>
    /// <param name="cohesionWeight"></param>
    /// <param name="separationWeight"></param>
    /// <returns></returns>
    protected Vector3 Flock(SteeringAI[] neighbors, float radius,
        float alignmentWeight, float cohesionWeight, float separationWeight)
    {
        Vector3 alignmentVelocity = Vector3.zero;
        Vector3 cohesionVelocity = Vector3.zero;
        Vector3 separationVelocity = Vector3.zero;

        int neighborsCount = 0;

        for (int i = 0; i < neighbors.Length; i++) {
            SteeringAI other = neighbors[i];
            if (other != this)
            {
                if (Vector3.Distance(myTransform.position, other.myTransform.position) <= radius)
                {
                    if (velocity.sqrMagnitude < 0.1f && other.velocity.sqrMagnitude < 0.1f)
                        break;

                    alignmentVelocity += other.velocity;

                    cohesionVelocity += other.myTransform.position;

                    separationVelocity += Flee(other.myTransform.position);

                    neighborsCount++;
                }
            }
        }
        if (neighborsCount > 0)
        {
            alignmentVelocity = (alignmentVelocity / neighborsCount).normalized;

            cohesionVelocity = (cohesionVelocity / neighborsCount);
            cohesionVelocity -= myTransform.position;
            cohesionVelocity.Normalize();
            separationVelocity = (separationVelocity / neighborsCount).normalized;

            return alignmentVelocity * alignmentWeight + cohesionVelocity *
                cohesionWeight + separationVelocity * separationWeight;
        }

        else return Vector3.zero;
       
    }


    //Implementation for any steering behaviour
    protected abstract void SteerringBehaviour();

    protected void LookAt(Vector2 direction)
    {
        float look = Mathf.Atan2(direction.normalized.y, direction.normalized.x) * Mathf.Rad2Deg;

        if (direction.x < 0)
        {

            if (Mathf.Sign(myTransform.localScale.x) != scaleSign)
            {
                myTransform.localScale =
                    new Vector3(myTransform.localScale.x * -1, myTransform.localScale.y,
                    myTransform.localScale.z);
            }
        }

        else if (Mathf.Sign(myTransform.localScale.x) != -scaleSign)
        {
            myTransform.localScale =
                new Vector3(myTransform.localScale.x * -1, myTransform.localScale.y,
                myTransform.localScale.z);
        }

        if (Mathf.Abs(look) > 90)
            look -= 180 * Mathf.Sign(myTransform.localScale.x);

        myTransform.rotation = Quaternion.Slerp(myTransform.rotation,
            Quaternion.Euler(0f, 0f, look), 5 * Time.deltaTime);
    } 
    #endregion
}
