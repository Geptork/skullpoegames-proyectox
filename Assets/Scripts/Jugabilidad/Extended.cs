﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Extended
{

    #region IList extensions

    /// <summary>
    /// Shuffle the specified list.
    /// </summary>
    /// <param name="list">List.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static void Shuffle<T>(this IList<T> list)
    {
        System.Security.Cryptography.RNGCryptoServiceProvider provider = new System.Security.Cryptography.RNGCryptoServiceProvider();
        int n = list.Count;
        while (n > 1)
        {
            byte[] box = new byte[1];
            do provider.GetBytes(box);
            while (!(box[0] < n * (System.Byte.MaxValue / n)));
            int k = (box[0] % n);
            n--;
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    /// <summary>
    /// Swap 2 elemtents of the specified list,.
    /// </summary>
    /// <param name="list">List.</param>
    /// <param name="indexA">First index.</param>
    /// <param name="indexB">Second index.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static void Swap<T>(this IList<T> list, int indexA, int indexB)
    {

        T tmp = list[indexA];
        list[indexA] = list[indexB];
        list[indexB] = tmp;
    }

    /// <summary>
    /// Returns the last element of a list.
    /// </summary>
    /// <param name="list">List.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static T Last<T>(this IList<T> list)
    {
        return list[list.Count - 1];
    }

    /// <summary>
    /// Returns the first element of a list
    /// </summary>
    /// <param name="list">List.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static T First<T>(this IList<T> list)
    {
        return list[0];
    }

    /// <summary>
    /// Returns a random element from a list.
    /// </summary>
    /// <param name="list">List.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static T Random<T>(this IList<T> list)
    {
        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    public static void MoveRight<T>(this IList<T> list, int positions = 1)
    {

        // Sitúa la última pieza de la colección al inicio.
        for (int i = 0; i < positions; i++)
        {
            T last = list.Last();
            list.Remove(last);
            list.Insert(0, last);
        }
    }

    public static void MoveLeft<T>(this IList<T> list, int positions = 1)
    {

        // Sitúa la última pieza de la colección al inicio.
        for (int i = 0; i < positions; i++)
        {
            T first = list.First();
            list.Remove(first);
            list.Insert(list.Count - 1, first);
        }
    }

    #endregion

    #region parent-child extensions

    /// <summary>
    /// Adds a child to an specific object.
    /// </summary>
    /// <param name="parent">Parent.</param>
    /// <param name="child">Child.</param>
    /// <param name="position">Position.</param>
    /// <param name="rotation">Rotation.</param>
    public static void AddChild(this Transform parent, Transform child, Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion))
    {
        Vector3 originalScale = child.localScale;

        child.parent = parent;
        child.localPosition = position;
        child.localRotation = rotation;
        child.localScale = originalScale;
    }


    #endregion

    #region number extensions

    /// <summary>
    /// Determines if a number is between the specified bottom and top.
    /// </summary>
    /// <returns><c>true</c> if is between the specified bottom and top; otherwise, <c>false</c>.</returns>
    /// <param name="number">Number.</param>
    /// <param name="bottom">Bottom.</param>
    /// <param name="top">Top.</param>
    /// <param name="inclusive">If set to <c>true</c>, top and bottom values will be checked.</param>
    public static bool IsBetween(this float number, float bottom, float top, bool inclusive = true)
    {

        if (inclusive)
        {
            return (number >= bottom && number <= top);

        }
        else
        {
            return (number > bottom && number < top);
        }

    }


    #endregion

    #region Vector Extensions
    public static Vector2 Truncate(this Vector2 v, float magnitude)
    {
        float vMagnitude = v.magnitude;

        if (vMagnitude > magnitude)
            return v.normalized * magnitude;
        else
            return v;
    }

    public static Vector3 Truncate(this Vector3 v, float magnitude)
    {
        float vMagnitude = v.magnitude;

        if (vMagnitude > magnitude)
            return v.normalized * magnitude;
        else
            return v;
    }

    public static Vector3 Magnituded(this Vector3 v, float magnitude)
    {
        return v.normalized * magnitude;
    }

    public static Vector3 Randomize(this Vector3 v, Vector3 bounds)
    {
        Vector3 value = new Vector3();
        value.x = UnityEngine.Random.Range(-bounds.x, bounds.x);
        value.y = UnityEngine.Random.Range(-bounds.y, bounds.y);
        value.z = UnityEngine.Random.Range(-bounds.z, bounds.z);
        return value;
    }

    public static Vector3 GetRelativeRadiusPosition(float minRadius, float maxRadius)
    {
        Vector2 value = new Vector2();
        do
        {
            value = UnityEngine.Random.insideUnitCircle * maxRadius;
        }
        while (Mathf.Pow(value.x, 2) + Mathf.Pow(value.y, 2) < Mathf.Pow(minRadius, 2) || value.x < 0);

        return value;
    }

    public static bool IsInsideBounds(this Vector3 v, Vector2 cameraBounds)
    {
        Vector3 bounds = Camera.main.ViewportToWorldPoint(new Vector3(cameraBounds.x + 0.5f , cameraBounds.y + 0.5f)) / 2;
        return v.x.IsBetween(-bounds.x, bounds.x, true) && v.y.IsBetween(-bounds.y, bounds.y, true);
    }
    
    #endregion
}
