﻿using UnityEngine;
using System.Collections;
using System;

public class ObstacleController : MonoBehaviour {

	#region events
	
	/// <summary>
	/// Occurs when on obstacle destroyed.
	/// </summary>
	public static event Action<ObstacleController> onObstacleDestroyed = delegate{};

	#endregion
	
	#region fields

	/// <summary>
	/// The health.
	/// </summary>
	[SerializeField] protected int health;

	/// <summary>
	/// The current health.
	/// </summary>
	protected int currentHealth;

	/// <summary>
	/// The points.
	/// </summary>
	[SerializeField] protected int points;

	#endregion
	
	#region properties
	
	/// <summary>
	/// Gets the points.
	/// </summary>
	/// <value>The points.</value>
	public int Points {
		get { return points; }
	}
	
	#endregion

	#region runtime

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	#endregion
	
	#region methods
	
	/// <summary>
	/// Hit this instance with the specified strength.
	/// </summary>
	/// <param name="strength">Strength.</param>
	public virtual void Hit(int strength) {
		this.health -= strength;
		
		if (this.health <= 0) Kill();
	}
	
	/// <summary>
	/// Kill this instance.
	/// </summary>
	protected virtual void Kill() {
	
		// TODO explosions and blah
		
		ObstacleController.onObstacleDestroyed(this);
	}
	
	#endregion
}
