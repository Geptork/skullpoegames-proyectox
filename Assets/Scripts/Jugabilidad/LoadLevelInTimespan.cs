﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadLevelInTimespan : MonoBehaviour {

	public float timespan;
	public string levelName;
	//public Component image;
	private float initialTimespan;

    public FadeIn Fade;
    float TimeFade;
    // Use this for initialization
    void Start () {
		initialTimespan = timespan;

        TimeFade = Fade.timespan;

        Fade.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
		timespan -= Time.deltaTime;

        if (timespan < 0 + TimeFade)
        {
            Fade.enabled = true;
        }
        if(timespan < 0)
        {
            SceneManager.LoadScene(levelName);
        }
    }
}
