﻿using UnityEngine;
using System.Collections;

public class StopAt : MonoBehaviour {

    public bool softStop = false;

    [Range(1, 50)]
    public float softIterations = 22f;

    [Range(0.01f, 5f)]
    public float TimeBetwenIterations = 0.5f;
    private bool StartingSoftStop = false;

    private float Iterations = 0;

    public bool Xaxis = false;
    public bool Yaxis = false;

    public float X;
    public float Y;
     [Range(0.1f, 1f)]
	public float  offset = 0f;

    private float startVelocityX = 0;
    private float startVelocityY = 0;


    // Update is called once per frame
    void Update () {
	    if (Xaxis)
        {
            if (transform.position.x > X - offset && transform.position.x < X + offset)
            {
                if (!softStop)
                {
                    gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
                }
                else
                {
                    if (!StartingSoftStop)
                    {
                        startVelocityX = gameObject.GetComponent<Rigidbody2D>().velocity.x;
                        startVelocityY = gameObject.GetComponent<Rigidbody2D>().velocity.y;
                        StartCoroutine(StartSoftSoping());
                        StartingSoftStop = true;
                    }
                }
            }
        }

        if (Yaxis)
        {
            if (transform.position.y > Y - offset && transform.position.y < Y + offset)
            {
                if (!softStop)
                {
                    gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
                }
                else
                {
                    if (!StartingSoftStop)
                    {
                        startVelocityX = gameObject.GetComponent<Rigidbody2D>().velocity.x;
                        startVelocityY = gameObject.GetComponent<Rigidbody2D>().velocity.y;
                        StartCoroutine(StartSoftSoping());
                        StartingSoftStop = true;
                    }
                }
            }
        }

    }

    IEnumerator StartSoftSoping()
    {
        Iterations++;
        float softParameter = 1-(Iterations/softIterations);
        yield return new WaitForSeconds(TimeBetwenIterations);
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(startVelocityX* softParameter, startVelocityY* softParameter);
        if (Iterations == softIterations)
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
            StopAllCoroutines();
        }
        else
        {
            StartCoroutine(StartSoftSoping());
        }

    }

}
