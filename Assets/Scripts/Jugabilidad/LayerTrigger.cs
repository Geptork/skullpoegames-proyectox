﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D), typeof(Rigidbody2D))]
public class LayerTrigger : MonoBehaviour {

	[SerializeField] private ParallaxLayer layer;

	[SerializeField] private GameObject trigger;

	public ParallaxLayer Layer {
		get { return layer; }
	}
	
	void Awake() {
		if (trigger == null) {
			trigger = FindObjectOfType<DestructorDeMundos>().gameObject;
		}
		
		layer.CanMove = false;
	}
	
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject == trigger) {
			layer.CanMove = true;
		}	
	}
}
