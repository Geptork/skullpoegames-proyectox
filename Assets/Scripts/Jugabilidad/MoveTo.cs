﻿using UnityEngine;
using System.Collections;

public class MoveTo : MonoBehaviour {
	
	//This is crappy
	public GameObject pointToMove;
	public float speed;
	private GameObject player;

	// Use this for initialization
	void Start () {
		gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0f);
		player = GameObject.Find ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < pointToMove.transform.position.x)
		{
			player.GetComponent<EnterPlanet>().enabled = true;
			gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);;
			Destroy(this);
		}
	}
}
