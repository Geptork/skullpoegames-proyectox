﻿using UnityEngine;
using System.Collections;

public class cent : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        EnemyHealthControl EC;
        EC = GetComponent<EnemyHealthControl>();
        if (EC != null)
        {
            if (EC.GetHealth() < 0)
            {
                Destroy(gameObject, 2.5f);
                Destroy(gameObject.GetComponentInChildren<CentShoot>());
            }
        }
	
	}
}
