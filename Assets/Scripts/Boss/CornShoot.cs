using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;


public class CornShoot: MonoBehaviour {

	public float escaladovelocidad;
	public float AnguloDispersion;
    public float  DamagePerHit;
    private BoxCollider2D colli;
    private SpriteRenderer SpriteRen;
    private float resizeSpeedCollider = 20f;
    public float offsetWidthCollider = 3;
    private GameObject NewCornShot;
    private float offsetColliderPosition;
    private float angle;



    Animator anim;
	public void Shoot () {
        anim = transform.parent.GetComponent<Animator>();
        if (!anim.GetBool("isShootingHorn"))
        {
            anim.SetBool("isShootingHorn", true);
            Invoke("InstShoot", 0.8f);
            Invoke("SetFalse", 0.83f);
        }
       
    }

    private void InstShoot()
    {

            
            string PrefabPath = "Prefabs/Enemies/Blinzerion/Cuerno/RAYO_CUERNO";
            string Name = "RAYO_CUERNO";
            GameObject Target = GameObject.Find("Player");
            Vector3 TargetPosition = Target.transform.position;
            Vector3 CuernoPosition = this.gameObject.transform.position;
            float TargetX = float.Parse(TargetPosition.x.ToString());
            float TargetY = float.Parse(TargetPosition.y.ToString());
            float MyPositionX = float.Parse(CuernoPosition.x.ToString());
            float MyPositionY = float.Parse(CuernoPosition.y.ToString());
            Vector3 PosShoot = new Vector3(MyPositionX, MyPositionY, 0.0f);
            float velocityX = Mathf.Pow(Mathf.Abs(TargetX - MyPositionX), 1);
            float velocityY = Mathf.Pow(Mathf.Abs(TargetY - MyPositionY), 1);
            float Modulo = Mathf.Sqrt(velocityX + velocityY);
            float dis = UnityEngine.Random.Range(-1.0f, 1.0f) * AnguloDispersion;

            Quaternion rotacion = Quaternion.FromToRotation(Vector3.right, -TargetPosition + PosShoot);
            Quaternion rotacionReferencia = Quaternion.identity;
            NewCornShot = GameObject.FindGameObjectWithTag("Instanciador").GetComponent<Instanciador>().GiveMeObject(Name);
            if (NewCornShot == null)
            {
                NewCornShot = (GameObject)Instantiate(Resources.Load(PrefabPath), PosShoot, transform.rotation);
            }
            NewCornShot.SetActive(false);
            SpriteRen = NewCornShot.GetComponent<SpriteRenderer>();
            NewCornShot.transform.parent = this.gameObject.transform;
            colli = NewCornShot.GetComponent<BoxCollider2D>();
            float SizeX = colli.size.x;
            PosShoot = new Vector3(MyPositionX + SizeX / 4, MyPositionY, 0.0f);
            NewCornShot.transform.position = PosShoot;
            Vector3 targetDir = -TargetPosition + PosShoot;

            angle = Quaternion.Angle(rotacionReferencia, rotacion);
            if (MyPositionY < TargetY)
            {
                angle = -angle;
            }
            angle = angle + 180;
            colli.transform.RotateAround(CuernoPosition, Vector3.forward, angle);
            NewCornShot.name = Name;
            Rigidbody2D rb;
            bulletController2 Bc;
            Bc = NewCornShot.GetComponent<bulletController2>();
            Bc.destroyOnImpact = false;
            Bc.damage = DamagePerHit;
            rb = NewCornShot.GetComponent<Rigidbody2D>();
            if (rb == null)
            {
                rb = NewCornShot.AddComponent<Rigidbody2D>();
            }
            if (rb.gravityScale != null)
            {
                rb.gravityScale = 0;
            }

            if (TargetX > MyPositionX)
            {
                Destroy(NewCornShot);
            }

            NewCornShot.SetActive(true);
            colli.size = new Vector2(0, 2);
            Destroy(NewCornShot, 0.83f);
            this.gameObject.name = "Cuerno"; //?
    }
    void SetFalse()
    {
        anim.SetBool("isShootingHorn", false);
    }
    void Update()
    {
      if (NewCornShot != null)
        {
            colli.offset = new Vector2(10, 0);
            if (colli.size.x < SpriteRen.bounds.extents.x*3.2*2.25)
            {
                colli.size = new Vector2(colli.size.x + 1f, 2);
            }
        }
    }
}
