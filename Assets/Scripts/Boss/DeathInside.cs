﻿using UnityEngine;
using System.Collections;




//SCRIPT PARA CONTROLAR QUE EL JUGADOR NO SE META DENTRO DEL BLINZ
public class DeathInside : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            collider.GetComponent<PlayerHealthController>().SetHealth(0.0f);
            collider.GetComponent<PlayerHealthController>().DoDamage(0f);
        }
    }
}
