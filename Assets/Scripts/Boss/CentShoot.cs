﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class CentShoot : MonoBehaviour
{
    public enum CentType { None, Blue, Green};
    public float AnguloDispersion;
    private GameObject NewShot;
    public float FireRate;
    private SpriteRenderer SpriteRen;
    public float DamagePerHit;
    BoxCollider2D colli;
    public CentType TypeOfCent;
    private string Name;

    void Start()
    {

        InvokeRepeating("Shoot", 0, FireRate);
    }
    public void Shoot()
    {
        string PrefabPath = "null";
        switch (TypeOfCent)
        {
            case CentType.None:
                PrefabPath = "Prefabs/Enemies/Cent/BulletsSprite/RayoCentGreen";
                Name = "RayoCentGreen";
                break;
            case CentType.Blue:
                PrefabPath = "Prefabs/Enemies/Cent/BulletsSprite/RayoCentGreen";
                Name = "RayoCentGreen";
                break;
            case CentType.Green:
                //Crear el prefab del rayo y poner la direccion dentro del Resource
                PrefabPath = "Prefabs/Enemies/Cent/BulletsSprite/RayoCentGreen";
                Name = "RayoCentGreen";
                break;
        }
        GameObject Target = GameObject.Find("Player");
        Vector3 TargetPosition = Target.transform.position;
        Vector3 WeapPosition = this.gameObject.transform.position;
        float TargetX = float.Parse(TargetPosition.x.ToString());
        float TargetY = float.Parse(TargetPosition.y.ToString());
        float MyPositionX = float.Parse(WeapPosition.x.ToString());
        float MyPositionY = float.Parse(WeapPosition.y.ToString());
        Vector3 PosShoot = new Vector3(MyPositionX, MyPositionY, 0.0f);
        float velocityX = Mathf.Pow(Mathf.Abs(TargetX - MyPositionX), 1);
        float velocityY = Mathf.Pow(Mathf.Abs(TargetY - MyPositionY), 1);
        float Modulo = Mathf.Sqrt(velocityX + velocityY);
        float dis = UnityEngine.Random.Range(-1.0f, 1.0f) * AnguloDispersion;
        Quaternion rotacion = Quaternion.FromToRotation(Vector3.right, -TargetPosition + PosShoot);
        NewShot = GameObject.FindGameObjectWithTag("Instanciador").GetComponent<Instanciador>().GiveMeObject(Name);
        if (NewShot == null)
        {
            NewShot = (GameObject)Instantiate(Resources.Load(PrefabPath), PosShoot, transform.rotation);
        }

        NewShot.SetActive(false);
        NewShot.transform.parent = this.gameObject.transform;
        colli = NewShot.GetComponent<BoxCollider2D>();
        float SizeX = colli.size.x;
        PosShoot = new Vector3(MyPositionX - SizeX / 2, MyPositionY, 0.0f);
        NewShot.transform.position = PosShoot;
        Vector3 targetDir = -TargetPosition + PosShoot;
        float angle = Quaternion.Angle(transform.rotation, rotacion);
        if (MyPositionY < TargetY)
        {
            angle = -angle;
        }
        NewShot.transform.RotateAround(WeapPosition, Vector3.forward, angle);
        NewShot.name = name;
        Rigidbody2D rb;
        bulletController2 Bc;
        Bc = NewShot.GetComponent<bulletController2>();
        Bc.damage = DamagePerHit;
        rb = NewShot.GetComponent<Rigidbody2D>();
        if (rb == null)
        {
            rb = NewShot.AddComponent<Rigidbody2D>();
        }
        if (rb.gravityScale != null)
        {
            rb.gravityScale = 0;
        }

        if (TargetX > MyPositionX)
        {
            Destroy(NewShot);
        }
        NewShot.SetActive(true);
        colli.size = new Vector2(0, 2);
        Destroy(NewShot, 0.83f); //AJUSTAR
        this.gameObject.name = "Eyes";
    }

    public void Sleep()
    {
        CancelInvoke("Shoot");
        Destroy(gameObject);
    }

    void Update()
    {
        if (NewShot != null)
        {
            SpriteRen = NewShot.GetComponent<SpriteRenderer>();
            colli.offset = new Vector2(10, 0);
            if (colli.size.x < SpriteRen.bounds.extents.x * 3.2)
            {
                colli.size = new Vector2(colli.size.x + 0.85f, 0.5f);
            }
        }
    }
}
