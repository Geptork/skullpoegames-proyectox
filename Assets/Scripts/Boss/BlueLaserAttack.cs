﻿using UnityEngine;
using System.Collections;

public class BlueLaserAttack : MonoBehaviour {

    //To use this script you must add the BlueLaser prefab and the blue laser clip.
    //You can adjust the laser collider modifying resizeSpeedCollider and offsetWidthCollider.

    //Publics vars: laserPrefab, laserClip, resize speed collider and offsetWidthCollider
    public AnimationClip blueLaserClip;
    public GameObject blueLaserPrefab;
    public float resizeSpeedCollider = 2.5f;
    public float offsetWidthCollider = 2;
    public float offsetXStartPos = -4f;
    private Vector3 _offsetXstarPos;

    public float DamagePerHit;

    //Private vars: to get prefab SpriteRenderer, currentShot and laserClip time, and add BoxCollider2D
    private SpriteRenderer blueLaserSprite;
    private GameObject currentBlueShot;
    private BoxCollider2D boxBlueLaserCollider;
    private float blueLaserClipTime;
    private float offsetColliderPosition;

    private bool shooting; 

    //Cambiar el estado del animator
    private Animator anim;

	// Use this for initialization
	void Start () {
        _offsetXstarPos = new Vector3(offsetXStartPos, 0, 0);

        //Get prefab SpriteRendered and laserClip time
        blueLaserSprite = blueLaserPrefab.GetComponent<SpriteRenderer>();
        blueLaserClipTime = blueLaserClip.length;

        //Add component BoxCollider2D and set as Trigger
        boxBlueLaserCollider = gameObject.AddComponent<BoxCollider2D>();
        boxBlueLaserCollider.isTrigger = true;
        boxBlueLaserCollider.enabled = false;

       
        shooting = false; 
    }

    //Method to be called in other Script
    public void doAttack()
    {
        //Comprobar si se esta disparando...
        if (!shooting)
        {
            shooting = true;
            anim = transform.parent.GetComponent<Animator>();
            //AQUI EMPIEZA EL DISPARO SE PUEDE PONER EL BOOL QUE CONTROLA LA IA A TRUE
            anim.SetBool("isShootingWeapon", true);
            
            //Stop previous coroutines
            StopAllCoroutines();
            //To get the lenght/2 of the Sprite to do the offset position and Instantiate the shot
            Vector3 offsetSprite = new Vector3(blueLaserSprite.bounds.extents.x, 0, 0);
            currentBlueShot = Instantiate(blueLaserPrefab, _offsetXstarPos + transform.position - offsetSprite, Quaternion.identity) as GameObject;
            //Set the shot as child to follow the Boss movement
            currentBlueShot.transform.parent = transform.parent;

            //Scale BoxCollider to min size
            boxBlueLaserCollider.size = new Vector2(0.1f, (blueLaserSprite.bounds.extents.y * 0.5f) - offsetWidthCollider);
            //Set correct position of BoxCollider2D
            offsetColliderPosition = boxBlueLaserCollider.size.x * 0.5f;

            //Collider enabled and start coroutine to resize it
            boxBlueLaserCollider.enabled = true;
            StartCoroutine(reSizeCollider());
            
            //Destroy shot when the clip is over
            Destroy(currentBlueShot, blueLaserClipTime);
        }
    }

    IEnumerator reSizeCollider()
    {
        //If current shot is not over
        if(currentBlueShot != null)
        {
            //If collider is smaller than the sprite
            if(boxBlueLaserCollider.size.x * 4 < blueLaserSprite.bounds.size.x)
            {
                //Calcul next collider size
                float nextSizeCollider = boxBlueLaserCollider.size.x + resizeSpeedCollider * Time.deltaTime;
                //Resize collider
                boxBlueLaserCollider.size = new Vector2(nextSizeCollider, (blueLaserSprite.bounds.extents.y * 0.5f) - offsetWidthCollider);
                //Set correct position of BoxCollider2D
                offsetColliderPosition = boxBlueLaserCollider.size.x * 0.5f;
                boxBlueLaserCollider.offset = new Vector2(offsetColliderPosition, 0);
                yield return null;
                StartCoroutine(reSizeCollider());
            }
            //If collider is bigger than the sprite
            else
            {
                yield return null;
                StartCoroutine(reSizeCollider());
            }
        }
        //If current shot is over disabled laser, stop couroutines and set the bool controlled by IA at false
        else
        {
            boxBlueLaserCollider.enabled = false;
            StopAllCoroutines();

            //AQUI HA TERMINADO EL DISPARO POR LO QUE SE PODRÍA PONER EL BOOL QUE CONTROLARÁ LA IA A FALSE
            anim.SetBool("isShootingWeapon", false);
            shooting = false;
        }
        
    }

 }
