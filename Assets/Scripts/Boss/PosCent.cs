using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class PosCent: MonoBehaviour {

public float VelMov;
public float Yposition;
protected bool fallinginreverse = true;

void DeactivateScript(){
  enabled = false;
}
    void Awake()
    {
        if (Yposition <= transform.position.y)
        {
            fallinginreverse = false;
        }
    }


    void Done()
    {
        Rigidbody2D rb;
        EnemyHealthControl ec;
        EnemyBehaviourV2 eb;
        CentShoot cs;
        rb = GetComponent<Rigidbody2D>();
        ec = GetComponent<EnemyHealthControl>();
        eb = GetComponent<EnemyBehaviourV2>();
        cs = GetComponentInChildren<CentShoot>();
        DeactivateScript();
        rb.velocity = new Vector3(0, 0, 0);
        ec.enabled = true;
        eb.enabled = true;
        cs.enabled = true;
    }


  public void Update(){
        Rigidbody2D rb;
        rb = GetComponent<Rigidbody2D>();
        if (transform.position.y < Yposition && fallinginreverse){
      rb.velocity = new Vector3 (0, VelMov, 0);
    }else if(transform.position.y >= Yposition && fallinginreverse)
        {
        Done();
    }

    if (transform.position.y > Yposition && !fallinginreverse)
    {
     rb.velocity = new Vector3(0, -VelMov, 0);
    }
    else if(transform.position.y <= Yposition && !fallinginreverse)
        {
    Done();
    } 
    }

}
