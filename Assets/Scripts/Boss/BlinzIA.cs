﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;

public class BlinzIA : MonoBehaviour
{
    public Instanciador Inst;
    public GameObject DeathArea;
    SpaceHUD HUD;

    [HideInInspector]
    public float VidaFase1 = 1;
    [HideInInspector]
    public float VidaFase2 = 1;
    [HideInInspector]
    public float VidaFase3 = 1;

    public float TimeDeath1Duration = 2.0f;
    public float TimeDeath2Duration = 3.0f;
    public float TimeDeath3Duration = 10.0f;
    public float TimebShoots = 3.0f;
    public float ProbShootFase1;
    public float ProbShootFase2;
    public float ProbHeadRayFase2;

    float PrevProbShoot;

    public float ProbHeadRayFase3;

    public GameObject Cent1;
    public GameObject Cent2;
    public GameObject Blinz0;
    public GameObject Blinz1;
    public GameObject Blinz2;
    public GameObject Blinz3;

    private bool fase1 = false;
    private bool fase2 = false;
    private bool fase3 = false;
    private bool Dying = false;
    private bool CentEnabled = false;
    private bool ShootingHorn = false;
    private bool ShootingGun = false;
    private bool isGoingOff = false;
    [HideInInspector]
    public float MaxHealth = -1;

    private float RandomNumber; 
    private CornShoot cs;
    private Animator anim;
    private EnemyHealthControl EnemyHealth;
    private GameObject CurrentBlinz = null;
    private GameObject NextBlinz = null;
    private BlueLaserAttack laser;

    //sonido
    private AudioSource Audio;
    private AudioSource Audio2;

    [Header ("Audio Clips")]
    public AudioClip BlinRoar;
    public AudioClip BlinDaño1;
    public AudioClip BlinDaño2;
    public AudioClip WeaponExplosion;
    public AudioClip BlueLaser;
    public AudioClip BrazoFuera;
    public AudioClip HornLaser;
    public AudioClip BlinGiño;
    public AudioClip BlinMuerte;
    public AudioClip Motor1;
    public AudioClip Motor2;
    public AudioClip Motor3;



    public void Restart()
    {
        Blinz0.SetActive(false);
        Blinz1.SetActive(false);
        Blinz2.SetActive(false);
        Blinz3.SetActive(false);

        CentEnabled = false;
        ShootingHorn = false;
        ShootingGun = false;
        CurrentBlinz = null;
        NextBlinz = null;

        fase1 = true;
        fase2 = false;
        fase3 = false;

        InitBlinz(Blinz0);
        CurrentBlinz = Blinz0;
        CurrentBlinz.SetActive(true);
        MaxHealth = Blinz0.GetComponent<EnemyHealthControl>().GetHealth() + Blinz2.GetComponent<EnemyHealthControl>().GetHealth() + Blinz3.GetComponent<EnemyHealthControl>().GetHealth();
    }


        void Start()
    {
        DeathArea.SetActive(true);
        Audio = GameObject.Find("Sounds_Blin").GetComponent<AudioSource>();
        Audio.PlayOneShot(BlinRoar, 0.4f);
        Audio2 = GameObject.Find("Sounds2_Blin").GetComponent<AudioSource>();
        Audio2.PlayOneShot(Motor1, 0.8f);
        Invoke("ChargeAttack", 0.5f);
        GameObject.Find("Main Camera").GetComponent<ShakeCam>().ShakeScreen(3.0f);
        InitBlinz(Blinz0);
        InvokeRepeating("RandomGeneration", 0, 1f); 

        fase1 = true;
        fase2 = false;
        fase3 = false;

        CurrentBlinz = Blinz0;
        CurrentBlinz.SetActive(true);
        MaxHealth = Blinz0.GetComponent<EnemyHealthControl>().GetHealth() + Blinz2.GetComponent<EnemyHealthControl>().GetHealth() + Blinz3.GetComponent<EnemyHealthControl>().GetHealth();

        VidaFase1 = Blinz0.GetComponent<EnemyHealthControl>().GetHealth();
        VidaFase2 = Blinz2.GetComponent<EnemyHealthControl>().GetHealth();
        VidaFase3 = Blinz3.GetComponent<EnemyHealthControl>().GetHealth();

        PrevProbShoot = ProbShootFase1;
        StartCoroutine(WaitForShooting(5f));
        ProbShootFase1 = 0f;

        HUD = GameObject.Find("GameplayPrueba").GetComponent<SpaceHUD>();
        HUD.BossUI.SetActive(true);

    }

    IEnumerator WaitForShooting(float timeDelayed)
    {
        yield return new WaitForSeconds(timeDelayed);
        ProbShootFase1 = PrevProbShoot;
    }

    void RandomGeneration()
    {
        RandomNumber = UnityEngine.Random.Range(0.0F, 1.0F);
    }
     
    void BlueLaserAttack()
    {       
        CurrentBlinz.GetComponentInChildren<BlueLaserAttack>().enabled = true;
        CurrentBlinz.GetComponent<BoxCollider2D>().enabled = true;
        CurrentBlinz.GetComponentInChildren<BlueLaserAttack>().doAttack();
    }
    void ChargeAttack()
    {
        CurrentBlinz.GetComponentInChildren<Animator>().SetBool("Charge", false);
        CurrentBlinz.GetComponent<ChargeAttack>().doAttack();
    }

    void InitBlinz(GameObject ThisBlinz)
    {
        EnemyHealth = ThisBlinz.GetComponent<EnemyHealthControl>();
        cs = ThisBlinz.GetComponentInChildren<CornShoot>();
        anim = ThisBlinz.GetComponent<Animator>();
    }

    void BlinzEnable()
    {
        NextBlinz.SetActive(true);
        CancelInvoke("BlinzEnable");
    }

    void ActivateBlinz(float time)
    {
        Invoke("BlinzEnable", time);
    }

    void Attack()
    {
        if (fase1)
        {
            if (ProbShootFase1 >= RandomNumber)
            {
                if (!ShootingGun)
                { 
                    Audio.PlayOneShot(BlueLaser, 0.8f);
                    Invoke("setShootingFalse", TimebShoots);
                    ShootingGun = true;
                    BlueLaserAttack();
                }
            }
        }
        if (fase2)
        {
            if (!CentEnabled)
            {
                //Cent1.SetActive(true);
                //Cent2.SetActive(true);
                CentEnabled = true;
            }
            if (ProbShootFase2 > RandomNumber)
            {
                //Disparo antonio
            }
            if (ProbHeadRayFase2 > RandomNumber)
            {
                //Disparo cuerno
                if (cs != null)
                {
                    if (!ShootingHorn)
                    {
                        cs.Shoot();
                        Audio.PlayOneShot(HornLaser, 0.8f);

                        ShootingHorn = true;
                        Invoke("setShootingFalse", TimebShoots);
                    }
                }
                else {
                    InitBlinz(CurrentBlinz);
                    if (!ShootingHorn)
                    {
                        cs.Shoot();
                        Audio.PlayOneShot(HornLaser, 0.8f);

                        ShootingHorn = true;
                        Invoke("setShootingFalse", TimebShoots);
                    }
                }
            }
        }
        if (fase3)
        {
            if (ProbHeadRayFase3 > RandomNumber)
            {
                //Disparo cuerno
                if (cs != null)
                {
                    if (!ShootingHorn)
                    {
                        cs.Shoot();
                        Audio.PlayOneShot(HornLaser, 1.0f);

                        ShootingHorn = true;
                        Invoke("setShootingFalse", TimebShoots);
                    }
                    else
                    {
                        InitBlinz(CurrentBlinz);
                        if (!ShootingHorn)
                        {
                            cs.Shoot();
                            Audio.PlayOneShot(HornLaser, 1.0f);

                            ShootingHorn = true;
                            Invoke("setShootingFalse", TimebShoots);
                        }

                    }
                }else
                {
                    cs = Blinz3.GetComponentInChildren<CornShoot>();
                }
            }
        }
        }

      public  void SelectBlinz()
    {

            if (CurrentBlinz == Blinz0 && EnemyHealth.GetHealth() <= 0)
            {
                GameObject.Find("Main Camera").GetComponent<ShakeCam>().ShakeScreen(2.5f);
                Destroy(CurrentBlinz);
                NextBlinz = Blinz1;
                ActivateBlinz(0.0f);
                fase1 = false;
                fase2 = true;
                fase3 = false;
                InitBlinz(NextBlinz);
            }

            if (CurrentBlinz == Blinz1)
            {
                if (!isGoingOff)
                {
                    Audio.PlayOneShot(WeaponExplosion);
                    StartCoroutine(BloodEventDelayed(2.1f, "SangreBrazoIzquierdo+"));
                    Audio2.clip = Motor2;
                    Audio2.volume = 0.8f;
                    Audio2.PlayDelayed(1.0f);
                    Destroy(CurrentBlinz, 2.0f);
                    NextBlinz = Blinz2;
                    ActivateBlinz(2.0f);
                    fase1 = false;
                    fase2 = true;
                    fase3 = false;
                    InitBlinz(NextBlinz);
                    isGoingOff = true;
                    Invoke("setIsGoinOffFalse", TimeDeath1Duration);
                }
            }

            if (CurrentBlinz == Blinz2 && EnemyHealth.GetHealth() <= 0)
            {
            if (!isGoingOff)
                {

                    SplashBlood();
                    StartCoroutine(BloodEventDelayed(2.0f, "SangreBrazoDerecho"));
                    StartCoroutine(BloodEventDelayed(2.1f, "SangreBrazoIzquierdo-"));
                    StartCoroutine(BloodEventDelayed(0.1f, "ExplosionDeSangre1"));
                    InitBlinz(NextBlinz);
                    Audio.PlayOneShot(BrazoFuera, 0.6f);
                    
                    Destroy(CurrentBlinz, 1.8f);
                    NextBlinz = Blinz3;
                    ActivateBlinz(1.8f);
                    fase1 = false;
                    fase2 = false;
                    fase3 = true;
                    anim.SetBool("F2", false);
                    anim.SetBool("F3", true);
                    isGoingOff = true;
                    Invoke("setIsGoinOffFalse", TimeDeath2Duration);
            }

            }


            if (CurrentBlinz == Blinz3 && Blinz3.GetComponent<EnemyHealthControl>().GetHealth() <= 0)
            {
                if (!isGoingOff && !Dying)
                {

                    SplashBlood();
                    StartCoroutine(BloodEventDelayed(0.1f, "ExplosionDeSangre1"));
                    StartCoroutine(BloodEventDelayed(6f, "ExplosionDeSangre2"));
                    StartCoroutine(BloodEventDelayed(7f, "ExplosionDeSangre3"));
                    StartCoroutine(BloodEventDelayed(8f, "ExplosionDeSangre4"));
                    fase1 = false;
                    fase2 = false;
                    fase3 = false;
                    DeathArea.SetActive(false);
                    Audio.clip= BlinMuerte;
                    Audio.volume = 0.8f;
                    Audio.PlayDelayed(1f);
                    Audio2.clip = BlinGiño;
                    Audio2.volume = 0.6f;
                    Audio2.PlayDelayed(9f);

                    Dying = true;
                    isGoingOff = true;
                    Invoke("setIsGoinOffFalse", TimeDeath3Duration);
                    //InitBlinz(NextBlinz);
                    //anim.SetBool("isDead", true);

                    //ESTA ES LA LINEA QUE METE LOS CREDITOS Y TO LA PESCA DAVIIIIIIIIIIIIIIIIIIIIID!!!!!!
                    StartCoroutine(TERMINARLEVEL(20f, 5f));
                    //ESTA ES LA LINEA QUE METE LOS CREDITOS Y TO LA PESCA DAVIIIIIIIIIIIIIIIIIIIIID!!!!!!

                if (anim != null)
                    {
                        anim.SetBool("isDead", true);
                    }else
                    {
                        InitBlinz(CurrentBlinz);
                        anim.SetBool("isDead", true);
                    }
                    Destroy(CurrentBlinz, 10.0f);
                Invoke("DisableBossBar", 11f);
            }

        }
    }
    private void DisableBossBar()
    {
        HUD.BossUI.SetActive(false);
    }
    private void setShootingFalse()
    {
        ShootingHorn = false;
        ShootingGun = false;
    }

    private void setIsGoinOffFalse()
    {
        isGoingOff = false;
    }

    void FixedUpdate()
    {
        if (CurrentBlinz != null)
        {
            SelectBlinz();
            Attack();
        }
        else
        {
            CurrentBlinz = NextBlinz;
        }

    }

    private void SplashBlood()
    {
        /* Hay que esperar que desparezcan las anteriores manchas para poner las siguientes.
         * Evidentemente en el juego real no va a haber problema porque ni de coña el jugador 
         * va a cargarse a 2 Blinz en menos de 10 segundos, pero como se que te vas a rayar te pongo esto.
         * Me voy a dormir zorra.
         * 
         * PD: mete particulas por ahi a tope en los cambios de fase que todavia esta muy descafeinado.
         * PD2: no dejes que ilu meta horteradas por favor.
         * 
         * 
         *                8888  8888888
                   888888888888888888888888
                8888:::8888888888888888888888888
              8888::::::8888888888888888888888888888
             88::::::::888:::8888888888888888888888888
           88888888::::8:::::::::::88888888888888888888
         888 8::888888::::::::::::::::::88888888888   888
            88::::88888888::::m::::::::::88888888888    8
          888888888888888888:M:::::::::::8888888888888
         88888888888888888888::::::::::::M88888888888888
         8888888888888888888888:::::::::M8888888888888888
          8888888888888888888888:::::::M888888888888888888
         8888888888888888::88888::::::M88888888888888888888
       88888888888888888:::88888:::::M888888888888888   8888
      88888888888888888:::88888::::M::;o*M*o;888888888    88
     88888888888888888:::8888:::::M:::::::::::88888888    8
    88888888888888888::::88::::::M:;:::::::::::888888888
   8888888888888888888:::8::::::M::aAa::::::::M8888888888       8
   88   8888888888::88::::8::::M:::::::::::::888888888888888 8888
  88  88888888888:::8:::::::::M::::::::::;::88:88888888888888888
  8  8888888888888:::::::::::M::"@@@@@@@"::::8w8888888888888888
   88888888888:888::::::::::M:::::"@a@":::::M8i888888888888888
  8888888888::::88:::::::::M88:::::::::::::M88z88888888888888888
 8888888888:::::8:::::::::M88888:::::::::MM888!888888888888888888
 888888888:::::8:::::::::M8888888MAmmmAMVMM888*88888888   88888888
 888888 M:::::::::::::::M888888888:::::::MM88888888888888   8888888
 8888   M::::::::::::::M88888888888::::::MM888888888888888    88888
  888   M:::::::::::::M8888888888888M:::::mM888888888888888    8888
   888  M::::::::::::M8888:888888888888::::m::Mm88888 888888   8888
    88  M::::::::::::8888:88888888888888888::::::Mm8   88888   888
    88  M::::::::::8888M::88888::888888888888:::::::Mm88888    88
    8   MM::::::::8888M:::8888:::::888888888888::::::::Mm8     4
        8M:::::::8888M:::::888:::::::88:::8888888::::::::Mm    2
       88MM:::::8888M:::::::88::::::::8:::::888888:::M:::::M
      8888M:::::888MM::::::::8:::::::::::M::::8888::::M::::M
     88888M:::::88:M::::::::::8:::::::::::M:::8888::::::M::M
    88 888MM:::888:M:::::::::::::::::::::::M:8888:::::::::M:
    8 88888M:::88::M:::::::::::::::::::::::MM:88::::::::::::M
      88888M:::88::M::::::::::*88*::::::::::M:88::::::::::::::M
     888888M:::88::M:::::::::88@@88:::::::::M::88::::::::::::::M
     888888MM::88::MM::::::::88@@88:::::::::M:::8::::::::::::::*8
     88888  M:::8::MM:::::::::*88*::::::::::M:::::::::::::::::88@@
     8888   MM::::::MM:::::::::::::::::::::MM:::::::::::::::::88@@
      888    M:::::::MM:::::::::::::::::::MM::M::::::::::::::::*8
      888    MM:::::::MMM::::::::::::::::MM:::MM:::::::::::::::M
       88     M::::::::MMMM:::::::::::MMMM:::::MM::::::::::::MM
        88    MM:::::::::MMMMMMMMMMMMMMM::::::::MMM::::::::MMM
         88    MM::::::::::::MMMMMMM::::::::::::::MMMMMMMMMM
          88   8MM::::::::::::::::::::::::::::::::::MMMMMM
           8   88MM::::::::::::::::::::::M:::M::::::::MM
               888MM::::::::::::::::::MM::::::MM::::::MM
              88888MM:::::::::::::::MMM:::::::mM:::::MM
              888888MM:::::::::::::MMM:::::::::MMM:::M
             88888888MM:::::::::::MMM:::::::::::MM:::M
            88 8888888M:::::::::MMM::::::::::::::M:::M
            8  888888 M:::::::MM:::::::::::::::::M:::M:
               888888 M::::::M:::::::::::::::::::M:::MM
              888888  M:::::M::::::::::::::::::::::::M:M
              888888  M:::::M:::::::::@::::::::::::::M::M
              88888   M::::::::::::::@@:::::::::::::::M::M
             88888   M::::::::::::::@@@::::::::::::::::M::M
            88888   M:::::::::::::::@@::::::::::::::::::M::M
           88888   M:::::m::::::::::@::::::::::Mm:::::::M:::M
           8888   M:::::M:::::::::::::::::::::::MM:::::::M:::M
          8888   M:::::M:::::::::::::::::::::::MMM::::::::M:::M
         888    M:::::Mm::::::::::::::::::::::MMM:::::::::M::::M
       8888    MM::::Mm:::::::::::::::::::::MMMM:::::::::m::m:::M
      888      M:::::M::::::::::::::::::::MMM::::::::::::M::mm:::M
   8888       MM:::::::::::::::::::::::::MM:::::::::::::mM::MM:::M:
              M:::::::::::::::::::::::::M:::::::::::::::mM::MM:::Mm
             MM::::::m:::::::::::::::::::::::::::::::::::M::MM:::MM
             M::::::::M:::::::::::::::::::::::::::::::::::M::M:::MM
            MM:::::::::M:::::::::::::M:::::::::::::::::::::M:M:::MM
            M:::::::::::M88:::::::::M:::::::::::::::::::::::MM::MMM
            M::::::::::::8888888888M::::::::::::::::::::::::MM::MM
            M:::::::::::::88888888M:::::::::::::::::::::::::M::MM
            M::::::::::::::888888M:::::::::::::::::::::::::M::MM
            M:::::::::::::::88888M:::::::::::::::::::::::::M:MM
            M:::::::::::::::::88M::::::::::::::::::::::::::MMM
            M:::::::::::::::::::M::::::::::::::::::::::::::MMM
            MM:::::::::::::::::M::::::::::::::::::::::::::MMM
             M:::::::::::::::::M::::::::::::::::::::::::::MMM
             MM:::::::::::::::M::::::::::::::::::::::::::MMM
              M:::::::::::::::M:::::::::::::::::::::::::MMM
              MM:::::::::::::M:::::::::::::::::::::::::MMM
               M:::::::::::::M::::::::::::::::::::::::MMM
               MM:::::::::::M::::::::::::::::::::::::MMM
                M:::::::::::M:::::::::::::::::::::::MMM
                MM:::::::::M:::::::::::::::::::::::MMM
                 M:::::::::M::::::::::::::::::::::MMM
                 MM:::::::M::::::::::::::::::::::MMM
                  MM::::::M:::::::::::::::::::::MMM
                  MM:::::M:::::::::::::::::::::MMM
                   MM::::M::::::::::::::::::::MMM
                   MM:::M::::::::::::::::::::MMM
                    MM::M:::::::::::::::::::MMM
                    MM:M:::::::::::::::::::MMM
                     MMM::::::::::::::::::MMM
                     MM::::::::::::::::::MMM
                      M:::::::::::::::::MMM
                     MM::::::::::::::::MMM
                     MM:::::::::::::::MMM
                     MM::::M:::::::::MMM:
                     mMM::::MM:::::::MMMM
                      MMM:::::::::::MMM:M
                      mMM:::M:::::::M:M:M
                       MM::MMMM:::::::M:M
                       MM::MMM::::::::M:M
                       mMM::MM::::::::M:M
                        MM::MM:::::::::M:M
                        MM::MM::::::::::M:m
                        MM:::M:::::::::::MM
                        MMM:::::::::::::::M:
                        MMM:::::::::::::::M:
                        MMM::::::::::::::::M
                        MMM::::::::::::::::M
                        MMM::::::::::::::::Mm
                         MM::::::::::::::::MM
                         MMM:::::::::::::::MM
                         MMM:::::::::::::::MM
                         MMM:::::::::::::::MM
                         MMM:::::::::::::::MM
                          MM::::::::::::::MMM
                          MMM:::::::::::::MM
                          MMM:::::::::::::MM
                          MMM::::::::::::MM
                           MM::::::::::::MM
                           MM::::::::::::MM
                           MM:::::::::::MM
                           MMM::::::::::MM
                           MMM::::::::::MM
                            MM:::::::::MM
                            MMM::::::::MM
                            MMM::::::::MM
                             MM::::::::MM
                             MMM::::::MM
                             MMM::::::MM
                              MM::::::MM
                              MM::::::MM
                               MM:::::MM
                               MM:::::MM:
                               MM:::::M:M
                               MM:::::M:M
                               :M::::::M:
                              M:M:::::::M
                             M:::M::::::M
                            M::::M::::::M
                           M:::::M:::::::M
                          M::::::MM:::::::M
                          M:::::::M::::::::M
                          M;:;::::M:::::::::M
                          M:m:;:::M::::::::::M
                          MM:m:m::M::::::::;:M
                           MM:m::MM:::::::;:;M
                            MM::MMM::::::;:m:M
                             MMMM MM::::m:m:MM
                                   MM::::m:MM
                                    MM::::MM
                                     MM::MM
                                      MMMM

                estoy como una puta cabra.
         */

        int BloodIndex = 0;
        GameObject Sangre = GameObject.Find("Sangre");
        SpriteAnimator _SpriteAnim = null;
        SpriteRenderer _SpriteRenderer = null;

        if (Sangre != null)
        {
            _SpriteAnim = Sangre.GetComponent<SpriteAnimator>();
            _SpriteRenderer = Sangre.GetComponent<SpriteRenderer>();
            _SpriteRenderer.enabled = true;
            Sangre.transform.parent = transform.parent;
            Sangre.GetComponent<Disappear>().enabled = true;
            Sangre.GetComponent<Disappear>().startDisappear();
        }
        if (_SpriteRenderer != null)
        {
            _SpriteRenderer.enabled = true;
        }
        if (_SpriteAnim != null)
        {
            _SpriteAnim.enabled = true;
        }

        while (Sangre != null)
        {
            BloodIndex++;
            Sangre = GameObject.Find("Sangre" + BloodIndex.ToString());
            if (Sangre != null)
            {
                _SpriteRenderer = Sangre.GetComponent<SpriteRenderer>();
                _SpriteAnim = Sangre.GetComponent<SpriteAnimator>();
            }


            if (_SpriteAnim != null)
            {
                _SpriteAnim.enabled = true;
            }

            if (_SpriteRenderer != null)
            {
                _SpriteRenderer.enabled = true;

            }

            if (Sangre != null)
            {
                Sangre.transform.parent = transform.parent;
                Sangre.GetComponent<Disappear>().enabled = true;
                Sangre.GetComponent<Disappear>().startDisappear();
            }

        }

    }

    private void BloodExplode(string EVENT)
    {
        if(EVENT == "SangreBrazoDerecho")
        {
            string BloodPath2 = "Prefabs/Particles/SangreChorro";
            GameObject MyParent = GameObject.Find("HUESO F2");
            Vector3 InstPos2 = MyParent.transform.position + new Vector3(0.2F, 0, 0) ;
            GameObject Blood2;
            Blood2 = Inst.GiveMeObject("SangreChorro");
            if(Blood2 == null)
            {
                Blood2 = (GameObject)Instantiate(Resources.Load(BloodPath2), InstPos2, transform.rotation);
            }
            Blood2.transform.position = InstPos2;
            //Blood.GetComponent<ParticleSystem>().startColor = new Color(1, 1, 1);
            Blood2.transform.parent = MyParent.transform;
            Blood2.GetComponent<ParticleSystem>().simulationSpace = ParticleSystemSimulationSpace.World;
            Blood2.transform.Rotate(0, 0, 63.4F);
            Blood2.SetActive(true);
            Blood2.transform.localScale = new Vector3(2, 2, 2);
            //Destroy(Blood2, 10.0f);
        }

        if (EVENT == "SangreBrazoIzquierdo-")
        {
            string BloodPath2 = "Prefabs/Particles/SangreChorro";
            GameObject MyParent = GameObject.Find("HOMBRO IZQ F1");
            Vector3 InstPos2 = MyParent.transform.position + new Vector3(0,-2,0);
            GameObject Blood2;
            Blood2 = Inst.GiveMeObject("SangreChorro");
            if (Blood2 == null)
            {
                Blood2 = (GameObject)Instantiate(Resources.Load(BloodPath2), InstPos2, transform.rotation);
            }
            Blood2.transform.position = InstPos2;
            //Blood.GetComponent<ParticleSystem>().startColor = new Color(1, 1, 1);
            Blood2.transform.parent = MyParent.transform;
            Blood2.GetComponent<ParticleSystem>().simulationSpace = ParticleSystemSimulationSpace.World;
            Blood2.transform.Rotate(0, 0, 160F);
            Blood2.SetActive(true);
            Blood2.transform.localScale = new Vector3(1, 1, 1);
            //Destroy(Blood2, 200.0f);
        }

        if (EVENT == "SangreBrazoIzquierdo+")
        {
            string BloodPath2 = "Prefabs/Particles/SangreChorro";
            GameObject MyParent = GameObject.Find("HOMBRO IZQ F1");
            Vector3 InstPos2 = MyParent.transform.position + new Vector3(0, -2, 0);
            GameObject Blood2;
            Blood2 = Inst.GiveMeObject("SangreChorro");
            if (Blood2 == null)
            {
                Blood2 = (GameObject)Instantiate(Resources.Load(BloodPath2), InstPos2, transform.rotation);
            }
            Blood2.transform.position = InstPos2;
            //Blood.GetComponent<ParticleSystem>().startColor = new Color(1, 1, 1);
            Blood2.transform.parent = MyParent.transform;
            Blood2.GetComponent<ParticleSystem>().simulationSpace = ParticleSystemSimulationSpace.World;
            Blood2.transform.Rotate(0, 0, 160F);
            Blood2.SetActive(true);
            Blood2.transform.localScale = new Vector3(4, 4, 4);
            //Destroy(Blood2, 200.0f);
        }

        if (EVENT == "ExplosionDeSangre1")
        {
            string BloodPath = "Prefabs/Particles/SangreSalpicaduraHC";
            Vector3 InstPos = new Vector3(7, -1, 0);
            GameObject Blood;
            Blood = Inst.GiveMeObject("SangreSalpicaduraHC");
            if (Blood == null)
            {
                Blood = (GameObject)Instantiate(Resources.Load(BloodPath), InstPos, transform.rotation);
            }
            Blood.transform.position = InstPos;
            Blood.GetComponent<ParticleSystem>().simulationSpace = ParticleSystemSimulationSpace.World;
            Blood.transform.Rotate(0, 0, 0);
            Blood.SetActive(true);
            Blood.transform.localScale = new Vector3(4, 4, 4);
            Destroy(Blood, 0.6f);

            string BloodPath2 = "Prefabs/Particles/SangreSalpicaduraHC";
            Vector3 InstPos2 = new Vector3(5, -1, 0);
            GameObject Blood2;
            Blood2 = Inst.GiveMeObject("SangreSalpicaduraHC");
            if (Blood2 == null)
            {
                Blood2 = (GameObject)Instantiate(Resources.Load(BloodPath2), InstPos2, transform.rotation);
            }
            Blood2.transform.position = InstPos;
            Blood2.GetComponent<ParticleSystem>().simulationSpace = ParticleSystemSimulationSpace.World;
            Blood2.transform.Rotate(0, 0, 160F);
            Blood2.SetActive(true);
            Blood2.transform.localScale = new Vector3(6, 6, 6);
            Destroy(Blood2, 0.6f);

        }

        if (EVENT == "ExplosionDeSangre2")
        {
            string BloodPath2 = "Prefabs/Particles/SangreSalpicaduraHC";
            Vector3 InstPos2 = new Vector3(5, -3, 0);
            GameObject Blood2;
            Blood2 = Inst.GiveMeObject("SangreSalpicaduraHC");
            if (Blood2 == null)
            {
                Blood2 = (GameObject)Instantiate(Resources.Load(BloodPath2), InstPos2, transform.rotation);
            }
            Blood2.transform.position = InstPos2;
            Blood2.GetComponent<ParticleSystem>().simulationSpace = ParticleSystemSimulationSpace.World;
            Blood2.transform.Rotate(0, 0, -120F);
            Blood2.SetActive(true);
            Blood2.transform.localScale = new Vector3(4, 4, 4);
            Destroy(Blood2, 0.6f);
        }

        if (EVENT == "ExplosionDeSangre3")
        {
            string BloodPath2 = "Prefabs/Particles/SangreSalpicaduraHC";
            Vector3 InstPos2 = new Vector3(5, -2, 0);
            GameObject Blood2;
            Blood2 = Inst.GiveMeObject("SangreSalpicaduraHC");
            if (Blood2 == null)
            {
                Blood2 = (GameObject)Instantiate(Resources.Load(BloodPath2), InstPos2, transform.rotation);
            }
            Blood2.transform.position = InstPos2;
            Blood2.GetComponent<ParticleSystem>().simulationSpace = ParticleSystemSimulationSpace.World;
            Blood2.transform.Rotate(0, 0, -56F);
            Blood2.SetActive(true);
            Blood2.transform.localScale = new Vector3(2, 2, 2);
            Destroy(Blood2, 0.6f);
        }

        if (EVENT == "ExplosionDeSangre4")
        {
            string BloodPath2 = "Prefabs/Particles/SangreSalpicaduraHC";
            Vector3 InstPos2 = new Vector3(5, 0, 0);
            GameObject Blood2;
            Blood2 = Inst.GiveMeObject("SangreSalpicaduraHC");
            if (Blood2 == null)
            {
                Blood2 = (GameObject)Instantiate(Resources.Load(BloodPath2), InstPos2, transform.rotation);
            }
            Blood2.transform.position = InstPos2;
            Blood2.GetComponent<ParticleSystem>().simulationSpace = ParticleSystemSimulationSpace.World;
            Blood2.transform.Rotate(0, 0, -270F);
            Blood2.SetActive(true);
            Blood2.transform.localScale = new Vector3(4, 4, 4);
            Destroy(Blood2, 0.6f);
        }
    }

    IEnumerator BloodEventDelayed(float Delay, string EVENT)
    {
        yield return new WaitForSeconds(Delay);
        BloodExplode(EVENT);
    }


    IEnumerator TERMINARLEVEL(float DelayFadeIn, float DelayChangeScene)
    {
        yield return new WaitForSeconds(DelayFadeIn);
        GameObject.Find("GameplayPrueba").GetComponentInChildren<FadeIn>().enabled = true; ;
        yield return new WaitForSeconds(DelayFadeIn);
        SceneManager.LoadScene("Credits");
    }
}