﻿using UnityEngine;
using System.Collections;

public class ChargeAttack : MonoBehaviour
{

    public float chargeSpeed;
    public float backSpeed;
    [Range(0.0f, 1.0f)]
    public float xMaxChargePosition;
    [Range(0.0f, 1.0f)]
    public float xComebackPosition; 

    private float maxDistance;
    private float distanceTraveled;
    private Vector3 lastPosition, initialPosition;
    private bool charging, turningBack;
    private GameObject player;
    private Vector3 chargingDirection;

    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Player");
        chargingDirection = new Vector3(-1, 0, 0);
        //doAttack(); 
    }

    // Update is called once per frame
    void Update()
    {
        if (charging)
        {
            transform.position += chargingDirection * Time.deltaTime * chargeSpeed;
            distanceTraveled += Vector3.Distance(transform.position, lastPosition);
            lastPosition = transform.position;
            if (distanceTraveled > maxDistance)
            {
                distanceTraveled = 0; 
                charging = false;
                turningBack = true;
            }                
        }

        if (turningBack)
        {
            transform.position -= chargingDirection * Time.deltaTime * backSpeed;
            if (transform.position.x > initialPosition.x)
            {
                turningBack = false; 
            }
        }
    }

    public void doAttack()
    {
        Vector2 maxChargePosition = GetWorldPositionFromScreenPercentage(xMaxChargePosition, 0.50f);
        Vector2 comebackPosition = GetWorldPositionFromScreenPercentage(xComebackPosition, 0.50f);
        distanceTraveled = 0;
        charging = true;
        turningBack = false;

        maxDistance = Vector3.Distance(transform.position, new Vector3(maxChargePosition.x, transform.position.y, Camera.main.nearClipPlane));

        initialPosition = comebackPosition;
        lastPosition = transform.position; 
    }

    private Vector3 GetWorldPositionFromScreenPercentage(float x, float y)
    {
        float xScreen = Screen.width * x;
        float yScreen = Screen.height * y;
        return Camera.main.ScreenToWorldPoint(new Vector2(xScreen, yScreen));
    }
}
