using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public class RandomItem: MonoBehaviour
{
    private int indexRandom;
    private float Randomfloat;
    private string ItemPath;

    private float posItemX;
    private float posItemY;
    private ObjectApear InstanceItem;
    [HideInInspector]
    public bool FuelEvent = false;
    private Instanciador instanciador;

    void Awake()
    {
        InstanceItem = GameObject.Find("Manager").GetComponentInChildren<ObjectApear>();
        if (SceneManager.GetActiveScene().name == "F1_Lvl2_RedSpace" )
        {
            FuelEvent = true;
        }

        instanciador = GameObject.FindGameObjectWithTag("Instanciador").GetComponent<Instanciador>();
    }

    private void addItem(string ItemPath)
    {

        string Path = "Prefabs/Gameplay/Items/";
        string Name = ItemPath;
        ItemPath = Path + ItemPath;
        InstanceItem.addobjetc(posItemX, posItemY, 1.2f, 0, ItemPath, Name);

    }

    private int GetIndexRandom(List<string> Lista)
    {
        int IndexRandom = Random.Range(0, Lista.Count);
        return IndexRandom;
    }

    public void DropItem(List<string> Lista, float ProbAppear, float positionX, float positionY)
    {
        posItemX = positionX;
        posItemY = positionY;

        indexRandom = GetIndexRandom(Lista);
        ItemPath = Lista[indexRandom];
        Randomfloat = Random.Range(0.0f, 1.0f);
        if(ProbAppear > Randomfloat)
        {
            addItem(ItemPath);
        }
    }

    public void Drop100Item(List<GameObject> Lista, float positionX, float positionY, float positionZ, float MaxVelX, float MaxVelY, string tagg)
    {

        if (FuelEvent && tagg== "Enemy") {
            Lista.Add(instanciador.GiveMeObject("FuelRestorer"));
        }
        for (int e = 0; e < Lista.Count; e++)
        {
            if (Lista[e] != null)
            {
               GameObject NewDrop = (GameObject)Instantiate(Lista[e], new Vector3(positionX + Random.Range(-0.15f, 0.15f), positionY + Random.Range(-0.15f, 0.15f), 0f), Quaternion.identity);
               NewDrop.SetActive(true);
               NewDrop.GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-0.0f, -MaxVelX), Random.Range(0.0f, MaxVelY));
            }
        }
    }

}
