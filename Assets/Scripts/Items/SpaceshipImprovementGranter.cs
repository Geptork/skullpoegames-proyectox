﻿using UnityEngine;
using System.Collections;

public class SpaceshipImprovementGranter : MonoBehaviour {

	public int strengthLevel;
	public AudioClip audioC;
	
	private AudioSource audioS;
	
	void Start()
	{
		audioS = GameObject.Find("SpaceGameplay").GetComponent<AudioSource>();
	}
	
	void OnTriggerEnter2D (Collider2D collider)
	{
		/*PlayerController pController = collider.gameObject.GetComponent<PlayerController>();
		GameObject container = transform.FindChild("Container").gameObject;
		
		if (pController != null)
		{
			//Give weapon to player
			pController.ChangeMainWeapon(transform.FindChild ("Weapon").gameObject);
			pController.SetStrengthLevel(strengthLevel);
			
			pController.GetComponent<SpriteRenderer>().sprite = container.GetComponent<SpriteRenderer>().sprite;
			*/
			/*
			SpriteAnimator spriteAnimator = pController.GetComponent<SpriteAnimator>();
			string name = spriteAnimator.currentAnimation.name;
			pController.GetComponent<SpriteAnimator>().animations = container.GetComponent<SpriteAnimator>().animations;
			*/
			
			//pController.GetComponent<SpriteAnimator>().ChangeAnimations( container.GetComponent<SpriteAnimator>().animations);
			
			audioS.clip = audioC;
			audioS.PlayOneShot(audioC);
			
			//DIE
			Destroy (gameObject);
		}
}
