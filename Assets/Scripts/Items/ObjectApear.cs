using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectApear : MonoBehaviour
{
    public Instanciador Instan;
    [HideInInspector]
    public enum item { None, SecondWeaponImprovement, RadioCollectable, FirstWeaponImprovement, ShieldRestorer, ShieldImprovement, EstelaCilezar, DiamanteAstergor, EscamaDelPasado, HealItem, HealthImprovement, HealthRestorer, PiedraMeteoro, LaserImprovement};
    [HideInInspector]
    public float frameOffset;
    [HideInInspector]
    public string path;
    [HideInInspector]
    public string name;
    [System.Serializable]
    public class ObjectController
    {
        public item WhatItem;
        public float positionX;
        public float positionY;
        public float StartFrame;
        public float velocity;
        public float gravity;
        private bool thisone;
        [HideInInspector]
        public bool Actived = false;
    }

    [HideInInspector]
    public float currentFrame;

    public List<ObjectController> ArrayObjetos;

    void Start()
    {
        if (Instan == null)
        {
            Instan = GameObject.FindGameObjectWithTag("Instanciador").GetComponent<Instanciador>();
        }
    }

    public void addobjetc(float positionX, float positionY, float velocity, float gravity, string ItemPath, string Name)
    {
        float posx = float.Parse(positionX.ToString());
        float posy = float.Parse(positionY.ToString());
        Vector3 newpos = new Vector3(posx, posy, 0f);
        GameObject newelement;
        newelement = Instan.GiveMeObject(Name); //#INSTANCIADOR  Probando de cambiar GiveMePreInstantiatedObject por GiveMeObject, FUNCIONA

        if (newelement == null)
        {
            newelement = (GameObject)Instantiate(Resources.Load(ItemPath), newpos, transform.rotation);
        }
        else
        {
            //print("hemos usado el instanciador para cargar el prefab:" + Name);
        }
        newelement.transform.position = newpos;
        newelement.SetActive(true);
        newelement.name = Name;
        Rigidbody2D rb;
        SpriteAnimator Animator = newelement.GetComponent<SpriteAnimator>();
        if (Animator != null)
        {
            Animator.enabled = true;
        }
        rb = newelement.GetComponent<Rigidbody2D>();
        if (rb == null)
        {
            rb = newelement.AddComponent<Rigidbody2D>();
        }
        if (rb.gravityScale != null)
        {
            rb.gravityScale = gravity;
        }
        if (velocity != null)
        {
            rb.velocity = new Vector3(-velocity, 0, 0);
        }
    }

    void selectitem(item nextitem)
    {
        switch (nextitem)
        {
            case item.SecondWeaponImprovement:
                name = "SecondWeaponImprovement";
                path = "Prefabs/Gameplay/Items/SecondWeaponImprovement";
                break;
            case item.RadioCollectable:
                name = "RadioCollectable";
                path = "Prefabs/Gameplay/Items/RadioCollectable";
                break;
            case item.FirstWeaponImprovement:
                name = "FirstWeaponImprovement";
                path = "Prefabs/Gameplay/Items/FirstWeaponImprovement";
                break;
            case item.ShieldRestorer:
                name = "ShieldRestorer";
                path = "Prefabs/Gameplay/Items/ShieldRestorer";
                break;
            case item.ShieldImprovement:
                name = "ShieldImprovement";
                path = "Prefabs/Gameplay/Items/ShieldImprovement";
                break;
            case item.EstelaCilezar:
                name = "EstelaCilezar";
                path = "Prefabs/Gameplay/Items/EstelaCilezar";
                break;
            case item.DiamanteAstergor:
                name = "DiamanteAstergor";
                path = "Prefabs/Gameplay/Items/DiamanteAstergor";
                break;
            case item.EscamaDelPasado:
                name = "EscamaDelPasado";
                path = "Prefabs/Gameplay/Items/EscamaDelPasado";
                break;
            case item.HealItem:
                name = "HealItem";
                path = "Prefabs/Gameplay/Items/HealItem";
                break;
            case item.HealthImprovement:
                name = "HealthImprovement";
                path = "Prefabs/Gameplay/Items/HealthImprovement";
                break;
            case item.HealthRestorer:
                name = "HealthRestorer";
                path = "Prefabs/Gameplay/Items/HealthRestorer";
                break;
            case item.PiedraMeteoro:
                name = "PiedraMeteoro";
                path = "Prefabs/Gameplay/Items/PiedraMeteoro";
                break;
            case item.LaserImprovement:
                name = "LaserImprovement";
                path = "Prefabs/Gameplay/Items/LaserImprovement";
                break;
        }
    }

    void Update()
    {
        currentFrame = Time.timeSinceLevelLoad - frameOffset;
        for (int ev = 0; ev < ArrayObjetos.Count; ev++)
        {
            if (currentFrame >= ArrayObjetos[ev].StartFrame && !ArrayObjetos[ev].Actived)
            {
                selectitem(ArrayObjetos[ev].WhatItem);
                addobjetc(ArrayObjetos[ev].positionX, ArrayObjetos[ev].positionY, ArrayObjetos[ev].velocity, ArrayObjetos[ev].gravity, path, name);
                ArrayObjetos[ev].Actived = true;
            }
        }
        comeback();
    }

    public void comeback()
    {
        for (int ev = 0; ev < ArrayObjetos.Count; ev++)
            if (currentFrame <= ArrayObjetos[ev].StartFrame && ArrayObjetos[ev].Actived)
            {
                ArrayObjetos[ev].Actived = false;
            }

    }

}
