﻿using UnityEngine;
using System.Collections;

public class PointsGranter : MonoBehaviour {

	public int pointsToGrant;
	private SpaceHUD PointsPrinter;
	
	void Start()
	{
        PointsPrinter = GameObject.Find ("GameplayPrueba").GetComponent<SpaceHUD>();
	}
	
	void OnTriggerEnter2D(Collider2D collider)
	{		
		if (collider.gameObject.tag == "Player")
		{
            //suma los puntos definidos por inspector a la Score general
            PointsPrinter.TotalScore += pointsToGrant;
			Destroy(gameObject);
		}
	}
}
