﻿using UnityEngine;
using System.Collections;

public class RadioCollider : MonoBehaviour {

	private GameObject spaceGameplayGO;

	void Start ()
    {
		spaceGameplayGO = GameObject.Find ("SpaceGameplay");
	}
	
	void OnTriggerEnter2D (Collider2D collider)
	{
		if (collider.gameObject.tag == "Player")
		{
            Estadisticas Estadis = GameObject.Find("EstadoJuego").GetComponent<Estadisticas>();
            Estadis.CurrentCollectablesTaken++;
            Estadis.CollectablesPercent = (Estadis.CurrentCollectablesTaken / Estadis.TotalCollectablesinGame) * 100f;
            //Activate radio
            spaceGameplayGO.GetComponent<TextManager>().Radio1isActive = true; ;
            //suma 1 al contador
			spaceGameplayGO.GetComponent<RadioContador>().IncreaseCounter();
            //DIE
            Destroy(gameObject);
		}
	}
}
