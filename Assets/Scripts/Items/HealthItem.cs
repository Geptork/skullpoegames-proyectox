﻿using UnityEngine;
using System.Collections;

public class HealthItem : MonoBehaviour {

    //points to restore
    public float HealPoints;

    //vida actual
    private float CurrentHealth;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            PlayerHealthController player = collider.gameObject.GetComponent<PlayerHealthController>();
            //GetHealth para que retorne la CurrentHealth del player
            CurrentHealth = player.GetHealth();
            //suma la vida actual del player con los puntos que deseamos incrementarla
            player.SetHealth(CurrentHealth + HealPoints);

            Destroy(gameObject);
        }
    }
}
