﻿using UnityEngine;
using System.Collections;

public class SateliteItem : MonoBehaviour
{
    Contador60 Cont60;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            Cont60 = GameObject.Find("GameplayPrueba").GetComponent<Contador60>();

            Cont60.SatItemGrab = true;
            Destroy(gameObject);
        }
    }
}
