﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDrop : MonoBehaviour
{
    public float chancesOfDroping;
    private float tdelay = 0.5f;
    [Header("100%")]
    public List<GameObject> Drop100;
    public float MaxVelocityY = 0f;
    public float MaxVelocityX = 1.5f;
    private string tagg;

    void Awake()
    {
        tagg = gameObject.tag;
    }

    public void Drop(List<string> bolsadeItems)
    {
        RandomItem RandomItem = GameObject.FindGameObjectWithTag("ItemPrefabBag").GetComponent<RandomItem>();
        //StartCoroutine(Drop100ButWait(tdelay, RandomItem)); Pa la prueba
        StartCoroutine(DropButWait(tdelay, RandomItem, bolsadeItems));
    }

   //PRUEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAS (si el GO tiene hijos, Dropa cada vez q muere un hijo)
    public void FijoDropeo()
    {
        RandomItem RandomItem = GameObject.FindGameObjectWithTag("ItemPrefabBag").GetComponent<RandomItem>();
        StartCoroutine(Drop100ButWait(tdelay, RandomItem));
    }
    //---------------------------------------------------

    IEnumerator DropButWait(float waitTime, RandomItem RandomItem, List<string> bolsadeItems)
    {
        yield return new WaitForSeconds(waitTime);
        RandomItem.DropItem(bolsadeItems, chancesOfDroping, transform.position.x, transform.position.y);
    }

    IEnumerator Drop100ButWait(float waitTime, RandomItem RandomItem)
    {
        yield return new WaitForSeconds(waitTime);
        RandomItem.Drop100Item(Drop100, transform.position.x, transform.position.y, transform.position.z, MaxVelocityX, MaxVelocityY, tagg);
    }
}