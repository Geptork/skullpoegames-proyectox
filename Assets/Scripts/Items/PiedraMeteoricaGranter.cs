﻿using UnityEngine;
using System.Collections;

public class PiedraMeteoricaGranter : MonoBehaviour
{

    public GameObject Bullet;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            collider.gameObject.GetComponent<changeWeapon>().ChangeSecondaryWeaponTemp(5f, Bullet, 0.1f, 0.8f, -0.1f);
        }
    }
}