﻿using UnityEngine;
using System.Collections;

public class FuelRestorer : MonoBehaviour {

    //points to restore
    public float FuelPoints;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            Fuel player = collider.gameObject.GetComponent<Fuel>();

            player.IncreaseFuel(FuelPoints);

            Destroy(gameObject);
        }
    }
}
