﻿using UnityEngine;
using System.Collections;

public class PruebaBORRARCAMBIOARMA : MonoBehaviour {

    public GameObject Bullet;

        void OnTriggerEnter2D(Collider2D collider)
        {
            if(collider.gameObject.tag == "Player")
            {
            // ----------------------------------------------TEST-----------------------------------------------------------
                //CAMBIA EL ARMA SECUNDARIA POR 10 SEGUNDOS
                //collider.gameObject.GetComponent<changeWeapon>().ChangeSecondaryWeaponTemp(10f,Bullet, 0.01f, 0.8f, -0.1f);

                //CAMBIA EL ARMA PRINCIPAL POR 10 SEGUNDOS
                collider.gameObject.GetComponent<changeWeapon>().ChangeMainWeaponTemp(10f, Bullet, 0.01f, 0.8f, -0.1f);

                //CAMBIA EL ARMA SECUNDARIA PARA SIEMPRE
                //collider.gameObject.GetComponent<changeWeapon>().ChangeSecondaryWeapon( Bullet, 0.01f, 0.8f, -0.1f);

                //CAMBIA EL ARMA PRINCIPAL PARA SIEMPRE
                //collider.gameObject.GetComponent<changeWeapon>().ChangeMainWeapon( Bullet, 0.01f, 0.8f, -0.1f);

                //CAMBIA EL FIRERATE DEL ARMA SECUNDARIA
                collider.gameObject.GetComponent<changeWeapon>().ChangeSecondaryWeapon(0.017F);

                //CAMBIA EL FIRERATE DEL ARMA PRINCIPAL
                //collider.gameObject.GetComponent<changeWeapon>().ChangeMainWeapon(0.017F);

                //CAMBIA EL FIRERATE DEL ARMA SECUNDARIA POR 5 SEGUNDOS
                //collider.gameObject.GetComponent<changeWeapon>().ChangeSecondaryWeaponTemp(5.0F, 0.01F);

                //CAMBIA EL FIRERATE DEL ARMA PRINCIPAL POR 5 SEGUNDOS
                //collider.gameObject.GetComponent<changeWeapon>().ChangeMainWeaponTemp(5F, 0.01F);

                //CAMBIAR EL DAÑO TEMPORAL
                collider.gameObject.GetComponent<changeWeapon>().ChangeMainDamageTemp(10f, 0f);
                collider.gameObject.GetComponent<changeWeapon>().ChangeSecondaryDamageTemp(10f, 0f);
                collider.gameObject.GetComponent<changeWeapon>().ChangeSpecialDamageTemp(10f, 0f);

                //CAMBIAR EL DAÑO PERMANENTE
                //collider.gameObject.GetComponent<changeWeapon>().ChangeMainDamage(10f);
                //collider.gameObject.GetComponent<changeWeapon>().ChangeSecondaryDamage(10f);
                //collider.gameObject.GetComponent<changeWeapon>().ChangeSpecialDamage(10F);

                //RECARGAR ARMA ESPECIAL
                //SETEA A 10 EL VALOR MAXIMO DE BALAS ESPECIALES Y RECARGA 10 BALAS
                collider.gameObject.GetComponent<changeWeapon>().ChangeSpeacialWeapon(10, 10);

                //RECARGA 10 BALAS, SI TU MUNICION MAXIMA ES X Y ES MENOR QUE 10, SETEA X
                //collider.gameObject.GetComponent<changeWeapon>().ChangeSpeacialWeapon(10);

            Destroy(gameObject);
            }
        }

    }
