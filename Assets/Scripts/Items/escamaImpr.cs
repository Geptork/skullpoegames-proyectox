﻿using UnityEngine;
using System.Collections;

public class escamaImpr : MonoBehaviour {
    public GameObject Bullet;

    // Update is called once per frame
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        { 
            //Disparo triple durante 3 segundos
            collider.gameObject.GetComponent<changeWeapon>().ChangeMainWeaponTemp(3f, Bullet, 0.2f, 0.8f, -0.1f);
            Destroy(gameObject);
        }
    }
}
