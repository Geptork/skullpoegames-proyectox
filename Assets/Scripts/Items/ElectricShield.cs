﻿using UnityEngine;
using System.Collections;

public class ElectricShield : MonoBehaviour
{

    public float AbsorbPercent;

    public GameObject Shieldimp;
    public Light ShieldLightimp;
    public float NewShieldHealth = 300f;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "Player")
        { 

        //PRUEBA                                     //NUEVO ESCUDO, NUEVA LUZ, NUEVA ABSORCION, NUEVA VIDA DE ESCUDO
        collider.GetComponent<ChangeShield>().ChangeIt(Shieldimp, ShieldLightimp, AbsorbPercent, NewShieldHealth, this.gameObject);

            //añadir vida al escudo a partir de la que ya tiene (otro tipo de IMPR?)
            //collider.GetComponent<ChangeShield>().ChangeIt(ShieldToIncrease, AbsorbPercent);
           Destroy(gameObject);
        }
    }
}
