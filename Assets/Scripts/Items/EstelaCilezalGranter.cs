﻿using UnityEngine;
using System.Collections;

public class EstelaCilezalGranter : MonoBehaviour {

    public GameObject Bullet;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            collider.gameObject.GetComponent<changeWeapon>().ChangeSecondaryWeapon(Bullet, 0.1f, 0.8f, -0.1f, true);
        }
    }
}
