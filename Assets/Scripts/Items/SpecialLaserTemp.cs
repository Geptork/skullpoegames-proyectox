﻿using UnityEngine;
using System.Collections;

public class SpecialLaserTemp : MonoBehaviour
{

    public GameObject Bullet;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            //Disparo special Laser durante 5 segundos PROBANDO
            collider.gameObject.GetComponent<changeWeapon>().ChangeSpecialWeaponTemp(5f, Bullet, 1, 1, 0.8f, -0.1f);
            Destroy(gameObject);
        }
    }
}
