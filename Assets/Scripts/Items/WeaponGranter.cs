﻿using UnityEngine;
using System.Collections;

public class WeaponGranter : MonoBehaviour {

    //New Bullet
    public GameObject Bullet;
    //new Fire Rate
    public float FireRate;
    //new Damage
    public float Damage;

    public AudioClip audioC;
	private AudioSource audioS;
	
	void Start()
	{
		audioS = GameObject.Find("SpaceGameplay").GetComponent<AudioSource>();
	}
	
	void OnTriggerEnter2D (Collider2D collider)
	{	
		if (collider.gameObject.tag == "Player")
		{
            changeWeapon Weapon = collider.GetComponent<changeWeapon>();

            //Change the MainWeapon
            Weapon.ChangeMainWeapon(Bullet, FireRate, 0.8f, -0.1f, true);
            Weapon.ChangeMainDamage(Damage);

            //Audio
            audioS.clip = audioC;
			audioS.PlayOneShot(audioC);
			
			//DIE
			Destroy (gameObject);
		}
	}
}
