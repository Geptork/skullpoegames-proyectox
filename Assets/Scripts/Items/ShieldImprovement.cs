using UnityEngine;
using System.Collections;

public class ShieldImprovement: MonoBehaviour {

    //lo que aumentara el escudo maximo
    public float ShieldToIncrease;

    //Reduccion de da�o (def = 0.5) 0-1
    public float AbsorbPercent;
    //sonido
    public AudioClip sound;
    private AudioSource Audio;


    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            PlayerHealthController player = collider.gameObject.GetComponent<PlayerHealthController>();
            player.UpgradeShield(ShieldToIncrease, AbsorbPercent);

            //capturar el AudioSource de SpaceGameplay y reproducir el sonido asignado por inspector
            Audio = GameObject.Find("SpaceGameplay").GetComponent<AudioSource>();
            Audio.PlayOneShot(sound);

            Destroy(gameObject);
        }
    }
}