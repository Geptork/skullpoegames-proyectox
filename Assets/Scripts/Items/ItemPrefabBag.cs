﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ItemPrefavBag_small
{
	public string name;
	public GameObject[] itemPrefabs_small;
}

public class ItemPrefabBag : Singleton<ItemPrefabBag> {

    string itemName;

	public ItemPrefavBag_small[] itemPrefabs;

	public static GameObject GetRandomPrefabFromBag(int bagIndex)
	{
		GameObject result = null;

        if (bagIndex >= 0 && bagIndex < Instance.itemPrefabs.Length)
            result = Instance.itemPrefabs[bagIndex].itemPrefabs_small[Random.Range (0, Instance.itemPrefabs[bagIndex].itemPrefabs_small.Length)];
			
		return result;
	}

    private void addItem (string PrefabPath, string name, float positionX, float positionY, float velocity)
    {
        float posx = positionX;
        float posy = positionY;
        Vector3 newpos = new Vector3(posx, posy, 10f);
        GameObject newelement = (GameObject)Instantiate(Resources.Load(PrefabPath), newpos, transform.rotation);
        newelement.SetActive(true);
        newelement.name = name;
        Rigidbody2D rb;
        rb = newelement.GetComponent<Rigidbody2D>();
        if (rb == null)
        {
            rb = newelement.AddComponent<Rigidbody2D>();
        }
        rb.velocity = new Vector2(velocity, 0);
    }
}
