﻿using UnityEngine;
using System.Collections;

public class ShieldRestorer : MonoBehaviour {

    //points to restore
    public float ShieldPoints;


    //escudo actual
    private float CurrentShield;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            PlayerHealthController player = collider.gameObject.GetComponent<PlayerHealthController>();
            //GetShield para que retorne el CurrentShield del player
            CurrentShield = player.GetShield();
            //suma el escudo actual del player con los puntos que deseamos incrementarla
            player.SetShield(CurrentShield + ShieldPoints);

            Destroy(gameObject);
        }
    }
}
