﻿using UnityEngine;
using System.Collections;

public class HealthImprovement : MonoBehaviour {

    //lo que aumentara la vida maxima
    public float HealthToIncrease;
    //sonido
    public AudioClip sound;
    private AudioSource Audio;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            PlayerHealthController player = collider.gameObject.GetComponent<PlayerHealthController>();
            //capturar el AudioSource de SpaceGameplay y reproducir el sonido asignado por inspector
            Audio = GameObject.Find("SpaceGameplay").GetComponent<AudioSource>();
            Audio.PlayOneShot(sound);

            player.UpgradeHealth(HealthToIncrease);

            Destroy(gameObject);
        }
    }
}
