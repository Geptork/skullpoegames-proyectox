﻿using UnityEngine;
using System.Collections;

public class RadioContador : MonoBehaviour
{
    public int counter = 0;

    //public function to increase in 1 the counter value
    public void IncreaseCounter()
    {
        counter ++;
    }

    //public function to get the counter value
    public int GetCounter()
    {
        return counter;
    }
}
