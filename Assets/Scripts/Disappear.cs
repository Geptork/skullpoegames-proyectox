﻿using UnityEngine;
using System.Collections;

public class Disappear : MonoBehaviour {

    public SpriteRenderer Sprite;
    public bool DisappearOnStart = false;
    [Range(0.1f, 100f)]
    public float ActiveTime = 2f;
    [Range(10, 10000)]
    public float CoefSoft = 10;
    private float RecursFrecuency;
    private float InitTime;

    void Start()
    {

        RecursFrecuency = (1 / CoefSoft);

        if (Sprite == null)
        {
            Sprite = gameObject.GetComponent<SpriteRenderer>();
        }


        if (DisappearOnStart)
        {
            startDisappear();
        }
    }

    public void startDisappear()
    {
        InitTime = Time.time;
        StartCoroutine(StartDisappear());
    }

    IEnumerator StartDisappear()
    {

        float Trans = 1 - ((Time.time - InitTime) / ActiveTime);
        Sprite.color = new Color(1, 1, 1, Trans);

        if (Trans <= 0)
        {
            Destroy(gameObject);
        }

        yield return new WaitForSeconds(RecursFrecuency);
        StartCoroutine(StartDisappear());
    }

}
