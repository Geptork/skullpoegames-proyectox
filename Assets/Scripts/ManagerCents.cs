﻿using UnityEngine;
using System.Collections;

public class ManagerCents : MonoBehaviour
{

    public GameObject Cent1;
    public GameObject Cent2;
    public GameObject Cent3;
    public GameObject Cent4;
    public GameObject Cent5;

    void Start()
    {
        Cent1.SetActive(false);
        Cent2.SetActive(false);
        Cent3.SetActive(false);
        Cent4.SetActive(false);
        Cent5.SetActive(false);

        ActivateCents();
    }
    void ActivateCents()
    {
        Invoke("ActivateCent1",0f);
        Invoke("ActivateCent2", 0.5f);
        Invoke("ActivateCent3", 1.8f);
        Invoke("ActivateCent4", 2.2f);
        Invoke("ActivateCent5", 3f);

    }

    void ActivateCent1()
    {
        Cent1.SetActive(true);
    }
    void ActivateCent2()
    {
        Cent2.SetActive(true);
    }
    void ActivateCent3()
    {
        Cent3.SetActive(true);
    }
    void ActivateCent4()
    {
        Cent4.SetActive(true);
    }
    void ActivateCent5()
    {
        Cent5.SetActive(true);
    }

}
