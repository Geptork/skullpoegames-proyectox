﻿using UnityEngine;
using System.Collections;

public class CinematicaLevel0 : MonoBehaviour {

    private GameObject Player;
    public GameObject Stalker1;
    public GameObject Stalker2;
    public GameObject StalkersBullets;

    private Camera cam;
    private float camHeight;
    private float camWidth;
    private  ShakeObject PlayerShakeObject;

    private ShakeObject Nepto1Shaker;
    private ShakeObject Nepto2Shaker;
    private bool Bullet1Shooted = false;
    private bool Bullet2Shooted = false;


    // Use this for initialization

    void Awake()
    {
        Player = this.gameObject;
        PlayerShakeObject = Player.GetComponent<ShakeObject>();
        PlayerShakeObject.ShakeOfStartTime = 900;
        PlayerShakeObject.offsetx = 0;
        PlayerShakeObject.offsety = 0.01f;
        PlayerShakeObject.TranslateX = 0.07f;
        PlayerShakeObject.TranslateY = 0f;

        PlayerShakeObject.enabled = true;
        cam = Camera.main;
        camHeight = 2f * cam.orthographicSize;
        camWidth = camHeight * cam.aspect;

        Stalker1 = (GameObject)Instantiate(Stalker1, new Vector3(Player.transform.position.x - 5, 2, 0), transform.rotation);
        Stalker2 = (GameObject)Instantiate(Stalker2, new Vector3(Player.transform.position.x - 5, -2, 0), transform.rotation);
        Stalker1.transform.localScale = new Vector3(0.7f, 0.7f, 1f);
        Stalker2.transform.localScale = new Vector3(0.7f, 0.7f, 1f);
        Stalker1.GetComponentInChildren<WeaponTarget>().ShootOnStart = false;
        Stalker2.GetComponentInChildren<WeaponTarget>().ShootOnStart = false;

        Stalker1.AddComponent<ShakeObject>().enabled = false;
        Stalker2.AddComponent<ShakeObject>().enabled = false;

        Nepto1Shaker = Stalker1.GetComponent<ShakeObject>();
        Nepto2Shaker = Stalker2.GetComponent<ShakeObject>();

        Nepto1Shaker.enabled = true;
        Nepto1Shaker.ShakeOnStart = true;
        Nepto1Shaker.ShakeOfStartTime = 900;
        Nepto1Shaker.TranslateX = 0.07f;
        Nepto1Shaker.TranslateY = 0.00f;
        Nepto1Shaker.offsetx = 0.0f;
        Nepto1Shaker.offsety = 0.01f;

        Nepto2Shaker.enabled = true;
        Nepto2Shaker.ShakeOnStart = true;
        Nepto2Shaker.ShakeOfStartTime = 900;
        Nepto2Shaker.TranslateX = 0.07f;
        Nepto2Shaker.TranslateY = 0.00f;
        Nepto2Shaker.offsetx = 0.0f;
        Nepto2Shaker.offsety = 0.01f;

    }
	
	// Update is called once per frame
	void Update () {

        if (Stalker1.transform.position.x > -camWidth / 4 - 2)
        {
            PlayerShakeObject.TranslateX = 0.04f;
            Nepto1Shaker.TranslateX = 0.07f;
            Nepto2Shaker.TranslateX = 0.06f;
        }

        if (Player.transform.position.x > -camWidth/4 -1)
        {
            PlayerShakeObject.TranslateX = 0.03f;
            Nepto1Shaker.TranslateX = 0.04f;
            Nepto2Shaker.TranslateX = 0.06f;
        }
        if (Player.transform.position.x > -camWidth / 4 + 1)
        {
            PlayerShakeObject.TranslateX = 0.02f;
            Nepto1Shaker.TranslateX = 0.05f;
            Nepto2Shaker.TranslateX = 0.07f;
        }

        if (Player.transform.position.x > -camWidth/4 +3)
        {
            PlayerShakeObject.TranslateX = 0.01f;
            Nepto1Shaker.TranslateX = 0.045f;
            Nepto2Shaker.TranslateX = 0.025f;
        }

        if (Player.transform.position.x > -camWidth / 4 + 7)
        {
            PlayerShakeObject.TranslateX = 0.007f;
            Nepto1Shaker.TranslateX = 0.01f;
            Nepto2Shaker.TranslateX = 0.01f;
        }

        if (Player.transform.position.x > -camWidth / 4 + 7)
        {
            PlayerShakeObject.TranslateX = 0.007f;

            if (Stalker2.transform.position.y > Player.transform.position.y)
            {
                Nepto2Shaker.TranslateY = -0.01f;
            }

            if (Stalker2.transform.position.y < Player.transform.position.y)
            {
                Nepto2Shaker.TranslateY = +0.01f;
            }

            if (Stalker1.transform.position.y > Player.transform.position.y + 0.7f)
            {
                Nepto1Shaker.TranslateY = -0.007f;
            }

            if (Stalker1.transform.position.y < Player.transform.position.y -0.7f)
            {
                Nepto1Shaker.TranslateY = +0.007f;
            }

            if (Stalker1.transform.position.y > Player.transform.position.y -0.4f && Stalker1.transform.position.y < Player.transform.position.y + 0.4f)
            {
                if (!Bullet1Shooted)
                {
                    Bullet1Shooted = true;
                    GameObject Bullet = (GameObject)Instantiate(StalkersBullets, Stalker1.transform.position, transform.rotation);
                    StartCoroutine(TimeBBullets1(UnityEngine.Random.Range(1f, 3f)));
                }
            }

            if (Stalker2.transform.position.y > Player.transform.position.y - 0.2f && Stalker2.transform.position.y < Player.transform.position.y + 0.2f)
            {
                if (!Bullet2Shooted)
                {
                    Bullet2Shooted = true;
                    GameObject Bullet = (GameObject)Instantiate(StalkersBullets, Stalker2.transform.position, transform.rotation);
                    StartCoroutine(TimeBBullets2(UnityEngine.Random.Range(1f, 3f)));
                    Bullet.GetComponent<SpriteRenderer>().sortingOrder = 1;
                    Bullet.GetComponent<SpriteRenderer>().sortingLayerName = "Characters";

                }
            }


        }



    }

    IEnumerator TimeBBullets1(float tdelay)
    {
        yield return new WaitForSeconds(tdelay);
        Bullet1Shooted = false;
    }

    IEnumerator TimeBBullets2(float tdelay)
    {
        yield return new WaitForSeconds(tdelay);
        Bullet2Shooted = false;
    }
}
