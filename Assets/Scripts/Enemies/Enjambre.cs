﻿using UnityEngine;
using System.Collections;

public class Enjambre : MonoBehaviour {

    public bool RunOnStart = false;
    public float TimeToDeploy;
    private bool Deployed;
    private float counter;
    public GameObject ElementToInstance;
    [Range(1,5000)]
    public int NumMaxElements = 10;
    [Range(0.00000000001f, 10f)]
    public float timeBwtElements = 0.2f;
    [Range(-20f, 20f)]
    public float velocityXDown = 3f;
    [Range(-20f, 20f)]
    public float velocityXTop = 10f;
    [Range(-20f, 20f)]
    public float velocityYDown = -1f;
    [Range(-20f, 20f)]
    public float velocityYTop = 1f;
    private int ItemDropped = 0;
    private Rigidbody2D rb;

    private float PrevVeloYDown;
    private float PrevVeloYTop;
    void Start()
    {
        if (RunOnStart)
        {
            StartCoroutine(InstanceNewItem());
        }
    }
    void Update()
    {
        counter += Time.deltaTime;
        if(counter >= TimeToDeploy && Deployed == false)
        {
            StartDroping();
            Deployed = true;
        }
    }
    public void StartDroping()
    {
        StartCoroutine(InstanceNewItem());
    }

    private void addItem(float velocityX, float velocityY)
    {

        Vector3 newpos = gameObject.transform.position;
        GameObject newelement;

        newelement = (GameObject)Instantiate(ElementToInstance, newpos, transform.rotation);
        newelement.SetActive(true);
        rb = newelement.GetComponent<Rigidbody2D>();
        if (rb == null)
        {
            rb = newelement.AddComponent<Rigidbody2D>();
        }
        rb.velocity = new Vector3(velocityX, velocityY, 0);
    }
    IEnumerator InstanceNewItem()
    {
        ItemDropped++;
        addItem(UnityEngine.Random.Range(velocityXDown, velocityXTop), UnityEngine.Random.Range(velocityYDown, velocityYTop));
        yield return new WaitForSeconds(timeBwtElements);
        if (ItemDropped < NumMaxElements) {
            StartCoroutine(InstanceNewItem());
        }
            else
        {
            StopAllCoroutines();
        }
    }
}
