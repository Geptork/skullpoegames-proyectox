﻿using UnityEngine;
using System.Collections;

public class ChargeSpecialLaser : MonoBehaviour
{
    GameObject Player;
    changeWeapon Weapon;

    public void ChargeSpecial()
    {
        Player = GameObject.FindGameObjectWithTag("Player");

        changeWeapon Weapon = Player.GetComponent<changeWeapon>();
        Weapon.ChangeSpeacialWeapon(1);
    }
}
