﻿using UnityEngine;
using System.Collections;

public class ShakeObject : MonoBehaviour
{
    public float offsetx = 0.1f;
    public float offsety = 0.1f;
    private float CurrentTime;
    private float StopTime;
    private Vector3 Posicion;
    private bool shaking = false;
    public bool ShakeOnStart = false;
    public float ShakeOfStartTime = 0.0f;
    public float TranslateX = 0.0f;
    public float TranslateY = 0.0f;

    void Start()
    {
        Posicion = gameObject.transform.position;
        if (ShakeOnStart)
        {
            ShakeIt(ShakeOfStartTime, TranslateX, TranslateY);
        }
    }

    private void UpdatePos()
    {
        Posicion = gameObject.transform.position;

        if (0.5 < UnityEngine.Random.RandomRange(0.0f, 1.0f))
        {
            offsetx *= -1;
        }
        if (0.5 < UnityEngine.Random.RandomRange(0.0f, 1.0f))
        {
            offsety *= -1;
        }
        Vector3 newpos = new Vector3(Posicion.x + offsetx + TranslateX, Posicion.y + offsety + TranslateY, Posicion.z);
        gameObject.transform.position = newpos;
        CurrentTime = Time.time;
        if (CurrentTime > StopTime)
        {
            CancelInvoke();
            shaking = false;
            TranslateX = 0.0f;
            TranslateY = 0.0f;
        }
    }

    public void ShakeIt(float time, float velocityx, float velocityy)
    {
        TranslateX = velocityx;
        TranslateY = velocityy;
        StopTime = Time.time + time;
        if (!shaking)
        {
            InvokeRepeating("UpdatePos", 0.01f, 0.02f);
            shaking = true;
        }
    }

    public void StopShakeIt()
    {
        StopTime = Time.time;
        TranslateX = 0.0f;
        TranslateY = 0.0f;
    }
}
