﻿using UnityEngine;
using System.Collections;

public class PointsOnKill : MonoBehaviour
{
    public int pointsToGrant;
    private SpaceHUD PointsPrinter;
    private Estadisticas GlobalStatistics;

    void Start()
    {
        PointsPrinter = GameObject.Find("GameplayPrueba").GetComponent<SpaceHUD>();
        GlobalStatistics = GameObject.Find("EstadoJuego").GetComponent<Estadisticas>();
    }

    public void AddPoints()
    {
        //suma los puntos definidos por inspector a la Score general
        PointsPrinter.TotalScore += pointsToGrant;
        GlobalStatistics.Score += pointsToGrant;
    }
}
