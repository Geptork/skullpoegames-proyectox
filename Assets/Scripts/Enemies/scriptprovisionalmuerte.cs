﻿using UnityEngine;
using System.Collections;

public class scriptprovisionalmuerte : MonoBehaviour {
    public EnemyHealthControl EHC;
    public Animator Anim;

    [Header("Only for Sprite Renderer")]
    public  float DyingTime;
    public string SpriteAnimationDeathName = "Dead";
    // Use this for initialization
    private bool Dead = false;
    // Update is called once per frame

    public bool MuerteJefeDeOleada = false;

	void Update () {
	    if(EHC.GetHealth() <= 0.0f && !Dead)
        {
            Dead = true;

            if(Anim != null) //si lo animamos por animator
            {
                Anim.SetBool("isDead", Dead);
                Destroy(gameObject, Anim.GetCurrentAnimatorStateInfo(0).length);
            }
            else //si lo animamos por sprite renderer
            {
                gameObject.GetComponent<SpriteAnimator>().ForcePlay(SpriteAnimationDeathName, false, 0);
                gameObject.tag = "Untagged";
                if (!MuerteJefeDeOleada)
                {
                    Destroy(gameObject, DyingTime);
                }

                if (MuerteJefeDeOleada)
                {
                    GameObject.Find("Main Camera").GetComponent<ShakeCam>().StopShakeScreen();
                    gameObject.GetComponent<ShakeObject>().enabled = true;
                    gameObject.GetComponent<EnemyBehaviourV2>().enabled = false;
                    Destroy(gameObject, 20);
                }
            }

        }
	}
}
