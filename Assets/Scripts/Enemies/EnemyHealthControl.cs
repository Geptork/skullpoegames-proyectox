﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class EnemyHealthControl : MonoBehaviour {
    //private vars
    [Header("Estado juego")]
    public Estadisticas estadisticas;
    private bool ChargeAttack = false;
    private float BlinzChargeTime = 3.0F;
    private float CurrentHealth;
    private float CurrentShield;
    public bool Died = false;
    public bool isABoss = false;
    Color MainColor = new Color(1f, 1f, 1f, 1f);
    Color IluColor = new Color(3f, 3f, 3f, 1f);
    SpaceHUD HUD;

    //public vars
    public enum EnemyTipe { None, BlinzFase0, BlinzFase1, BlinzFase2, SateliteEventoFuel};
    private float TotalBossDamaged = 0;
    public EnemyTipe _EnemyTipe;
    public float InitHealth = 100.0f;
    public float InitShield = 0.0f;
    //0 - 1 % shield absorbed damage (1 -> full protected)
    public float ShieldAbsorb = 0.5f;



    //set init values
	void Awake () {
        CurrentHealth = InitHealth;
        CurrentShield = InitShield;

        HUD = GameObject.Find("GameplayPrueba").GetComponent<SpaceHUD>();

    }

    private void Kill()
    {
        if (_EnemyTipe == EnemyTipe.SateliteEventoFuel)
        {
            GameObject.FindGameObjectWithTag("ItemPrefabBag").GetComponent<RandomItem>().FuelEvent = true;
            gameObject.GetComponent<ItemDrop>().FijoDropeo();

        }
        List<string> bolsadeItems = new List<string>();

        if (this.gameObject.tag == "Enemy" && !Died)
        {
            gameObject.GetComponent<PointsOnKill>().AddPoints();
            GameObject.Find("EstadoJuego").GetComponent<Estadisticas>().Kills++;
            bolsadeItems.Add("EscamaDelPasado");
            bolsadeItems.Add("HealthRestorer");

        }

        if (this.gameObject.tag == "Meteorite")
        {
            bolsadeItems.Add("ShieldRestorer");
            bolsadeItems.Add("PiedraMeteoro");
        }

        if (!Died && this.gameObject.tag != "Enemy-C" && this.gameObject.tag != "Nave-C")
        {
            gameObject.GetComponent<ItemDrop>().Drop(bolsadeItems);
        }
        Died = true;
        //set animator death boolean true and destroy this object a few secods after. 

        if(_EnemyTipe == EnemyTipe.BlinzFase2)
        {
            HUD.UpdateForBoss(999999999999999999); //Arreglo provisional
        }
    }

    //public functions to get shield/health parameters
    public float GetShield () {
        return CurrentShield;
	}

    //public functions to get shield/health parameters
    public float GetHealth() { 
        return CurrentHealth;
    }

    //public subprograms to set shield/health parameters
    public void SetShield(float ShieldToSet)
    {
            //seting value
            CurrentShield = ShieldToSet;
    }

    //public subprograms to set shield/health parameters
    public void SetHealth(float HealthToSet)
    {
        
            //seting value
            CurrentHealth = HealthToSet;
    }

    //subprogram to damage enemy shield
    private void TakeShield(float damage)
    {
        //Recalculate damaged absorved by our shield
        float DamageRecalculated = damage*(1 - ShieldAbsorb);
        if(DamageRecalculated > CurrentShield)
        {
            CurrentShield = 0.0f;
        }
        else
        {
            CurrentShield -= DamageRecalculated; 
        }
    }

    //subprogram to damage enemy health
    private void TakeHealth(float damage)
    {
        if (damage> CurrentHealth)
        {
            CurrentHealth = 0.0f;
        }
        else
        {
            CurrentHealth -= damage;
        }


    }


    /*
        Use DoDamage to damage enemy from your external Script
        Usage: EnemyHealthControl.DoDamage(20.0);
    */
    public void DoDamage(float IncomingDamage)
    {
        if (isABoss) {TotalBossDamaged += IncomingDamage;}

        //if enemy have shield damaged it
        if (CurrentShield > 0.0f)
        {
            TakeShield(IncomingDamage);
        }
        else
        {
            TakeHealth(IncomingDamage);
        }

        if (estadisticas != null)
        {
            estadisticas.DamageDone += IncomingDamage;
        }
        else
        {
            estadisticas = GameObject.FindGameObjectWithTag("EstadoJuego").GetComponent<Estadisticas>();
            if (estadisticas == null)
            {
                estadisticas = GameObject.Find("EstadoJuego").GetComponent<Estadisticas>();
            }
            estadisticas.DamageDone += IncomingDamage;
        }

        if (CurrentHealth <= 0)
        {


            Kill();
        }
        else
        {
            switch (_EnemyTipe)
            {
                case EnemyTipe.None:
                    //NO ILUMINAR NADA, ENEMIGO COMUN;
                    Invoke("RestoreColor", 0.5f);
                    break;
                case EnemyTipe.BlinzFase0:
                    HUD.UpdateForBoss(TotalBossDamaged);

                    //lo enfadamos
                    GameObject.Find("Managers").GetComponent<BlinzIA>().ProbShootFase1 += 0.01f;

                    //cargar
                    //GameObject Player = GameObject.Find("Player");

                    /*if((Player.transform.position.y > 3 || Player.transform.position.y < -3) && !ChargeAttack)
                    {
                        gameObject.GetComponent<ChargeAttack>().doAttack();
                        ChargeAttack = true;
                        StartCoroutine(BlinzCharge(BlinzChargeTime));
                    }*/

                    //ILLUMINAR BRAZO DEL BLINZ
                    Renderer Weapon = GameObject.Find("ARMA F0").GetComponent<Renderer>();
                    Weapon.material.color = IluColor;

                    Renderer BrazoIzq = GameObject.Find("BRAZO IZQ F0").GetComponent<Renderer>();
                    BrazoIzq.material.color = IluColor;

                    Renderer AnteIzq = GameObject.Find("ANTEBRAZO IZQ F0").GetComponent<Renderer>();
                    AnteIzq.material.color = IluColor;

                    Renderer HomIzq = GameObject.Find("HOMBRO IZQ F0").GetComponent<Renderer>();
                    HomIzq.material.color = IluColor;

                    Renderer ManoIzq = GameObject.Find("MANO IZQ F0").GetComponent<Renderer>();
                    ManoIzq.material.color = IluColor;

                    Invoke("RestoreColor", 0.2f);
                    break;
                case EnemyTipe.BlinzFase1:
                    float vidaBossFase0 = GameObject.Find("Managers").GetComponent<BlinzIA>().VidaFase1;
                    HUD.UpdateForBoss(TotalBossDamaged + vidaBossFase0);
                    //lo enfadamos
                    GameObject.Find("Managers").GetComponent<BlinzIA>().ProbHeadRayFase2 += 0.01f;

                    //ILLUMINAR ESTOMAGO DEL BLINZ
                    Renderer Abdom = GameObject.Find("ABDOMEN F1").GetComponent<Renderer>();
                    Abdom.material.color = IluColor;

                    //Renderer base2 = GameObject.Find("base2 F2").GetComponent<Renderer>();
                    //base2.material.color = IluColor;

                    Renderer Motor3 = GameObject.Find("MOTOR 3 F0").GetComponent<Renderer>();
                    Motor3.material.color = IluColor;

                    Invoke("RestoreColor", 0.2f);
                    break;
                case EnemyTipe.BlinzFase2:
                    float vFase0 = GameObject.Find("Managers").GetComponent<BlinzIA>().VidaFase1;
                    float vidaBossFase1 = GameObject.Find("Managers").GetComponent<BlinzIA>().VidaFase2;

                    HUD.UpdateForBoss(TotalBossDamaged + vidaBossFase1 + vFase0);
                    //lo enfadamos
                    GameObject.Find("Managers").GetComponent<BlinzIA>().ProbHeadRayFase3 += 0.01f;

                    //ILUMINAR CABEZA DEL BLINZ
                    Renderer Cabeza = GameObject.Find("CABEZA F2").GetComponent<Renderer>();
                    Cabeza.material.color = IluColor;

                    Renderer Cuello = GameObject.Find("CUELLO F0").GetComponent<Renderer>();
                    Cuello.material.color = IluColor;

                    Renderer Mandi = GameObject.Find("MANDIBULA F1").GetComponent<Renderer>();
                    Mandi.material.color = IluColor;

                    Renderer Ojo = GameObject.Find("ojo F2").GetComponent<Renderer>();
                    Ojo.material.color = IluColor;

                    Invoke("RestoreColor", 0.2f);
                    break;
            }
        }
    }

    private void RestoreColor()
    {
        //VOLVER A SU ESTADO DE COLOR ANTERIOR
        switch (_EnemyTipe)
        {
            case EnemyTipe.None:
                //NO HACER NADA, ENEMIGO COMUN;
                break;
            case EnemyTipe.BlinzFase0:
                //COLOREAR BRAZO DEL BLINZ CON COLOR REAL
                Renderer Weapon = GameObject.Find("ARMA F0").GetComponent<Renderer>();
                Weapon.material.color = MainColor;

                Renderer BrazoIzq = GameObject.Find("BRAZO IZQ F0").GetComponent<Renderer>();
                BrazoIzq.material.color = MainColor;

                Renderer AnteIzq = GameObject.Find("ANTEBRAZO IZQ F0").GetComponent<Renderer>();
                AnteIzq.material.color = MainColor;

                Renderer HomIzq = GameObject.Find("HOMBRO IZQ F0").GetComponent<Renderer>();
                HomIzq.material.color = MainColor;

                Renderer ManoIzq = GameObject.Find("MANO IZQ F0").GetComponent<Renderer>();
                ManoIzq.material.color = MainColor;
                break;
            case EnemyTipe.BlinzFase1:
                //COLOREAR ESTOMAGO DEL BLINZ CON COLOR REAL
                Renderer Abdom = GameObject.Find("ABDOMEN F1").GetComponent<Renderer>();
                Abdom.material.color = MainColor;

                //Renderer base2 = GameObject.Find("base2 F2").GetComponent<Renderer>();
                //base2.material.color = MainColor;

                Renderer Motor3 = GameObject.Find("MOTOR 3 F0").GetComponent<Renderer>();
                Motor3.material.color = MainColor;
                break;
            case EnemyTipe.BlinzFase2:
                //COLOREAR CABEZA DEL BLINZ CON COLOR REAL
                Renderer Cabeza = GameObject.Find("CABEZA F2").GetComponent<Renderer>();
                Cabeza.material.color = MainColor;

                Renderer Cuello = GameObject.Find("CUELLO F0").GetComponent<Renderer>();
                Cuello.material.color = MainColor;

                Renderer Mandi = GameObject.Find("MANDIBULA F1").GetComponent<Renderer>();
                Mandi.material.color = MainColor;

                Renderer Ojo = GameObject.Find("ojo F2").GetComponent<Renderer>();
                Ojo.material.color = MainColor;
                break;
        }
    }

    IEnumerator BlinzCharge(float timeBcharge)
    {

        yield return new WaitForSeconds(timeBcharge);
        ChargeAttack = false;
    }

}
