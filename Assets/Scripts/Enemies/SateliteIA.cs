﻿using UnityEngine;
using System.Collections;

public class SateliteIA : MonoBehaviour {

    public float velocity; 
    public GameObject PArriba;
    public GameObject PAbajo;

    [Header ("Diamantes")]
    public GameObject Diam1;
    public GameObject Diam2;
    public GameObject Diam3;
    public GameObject Diam4;
    public GameObject Diam5;

    private bool Fase2;
    [HideInInspector]
    public bool Died;
    void Start ()
    {
        PArriba.tag = "Armored";
        PAbajo.tag = "Armored";
        GetComponent<Rigidbody2D>().velocity = new Vector2(velocity,0);
        Fase2 = false;
        Died = false;
    }

    void Update ()
    {
        if (Diam1 == null && Diam2 == null && Diam3 == null && Diam4 == null && Diam5 == null && Fase2 == false)
        {
            PArriba.tag = "Enemy";
            PAbajo.tag = "Enemy";
            Fase2 = true;
        }

        if (PAbajo == null && PArriba == null && Died == false)
        {
            Died = true;
            //Destroy(gameObject);
            GetComponent<EnemyHealthControl>().DoDamage(500);
        }
	}
}
