﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviourV2 : MonoBehaviour
{
    private Instanciador Instan;
    //////Movement vars//////
    public float speedXaxis = 2.5f;
    //KAMIKAZE
    [Header("Kamikaze")]
    public float speedForKamikazeBehaviour = 3f;
    private Vector2 posTargetKamikaze = Vector2.zero;
    [Range(0, 1)]
    public float posScreenToKamikazeGO = 0.7f; //Between 0 and 1
    public float distanceDetectPlayer = 4f;
    public float radExplosion = 2f;
    private bool kamikazeStarted = false;
    //SINUS
    [Header("Sinus")]
    public float amplitudeSinus = 4;
    public float freqSinus = 3f;
    public float phaseAngleSinus = 0;
    private float offsetYposition = 0;
    //INVISIBLE
    [Header("Invisible")]
    private bool invisible = true;
    public float timeToWait = 1f;
    private SpriteRenderer enemyRender;
    private BoxCollider2D enemyCollider;
    //MINE
    [Header("Mine")]
    Color Red = new Color(1, 0, 0);
    SpriteRenderer SP;
    public float distanceToExplosionMine = 1.5f;
    public float MineDamage = 1000f;
    private float StartDelayMine = 0.5f;
    private bool GoingOff = false;
    ////////////

    public enum BehaviourAI { None, Standard, Kamikaze, Sinus, Invisible, Mine };
    public BehaviourAI behaviour;
    protected float counterForInvisibleBehaviour = 4;

    [Header("ShakeCam")]
    public bool ShakeOnStart = false;
    public float ShakeDelay = 0.2f;
    public float ShakeOfStartTime = 0.0f;

    GameObject player;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        enemyRender = GetComponent<SpriteRenderer>();
        enemyCollider = GetComponent<BoxCollider2D>();
        offsetYposition = transform.position.y;
        if (behaviour == BehaviourAI.Invisible) StartCoroutine(ChangeVisibilityEnemy());

    }
    void Awake()
    {
        Instan = GameObject.FindGameObjectWithTag("Instanciador").GetComponent<Instanciador>();
        WeaponTarget WTInstan = gameObject.GetComponentInChildren<WeaponTarget>();
        if(WTInstan != null)
        {
            WTInstan.Instan = Instan;
        }
    }


    void Update()
    {
        if (ShakeOnStart)
        {
            if (ShakeDelay == 0.0f) //La razon de esto es rendimiento, unicamente
            {
                ShakeDelay += 0.01f;
            }
            StartCoroutine(ShakeCam(ShakeDelay));
            ShakeOnStart = false;
        }

        switch (behaviour)
        {
            //NONE
            case BehaviourAI.None:
                break;

            //STANDARD
            case BehaviourAI.Standard:
                //normal movement
                transform.position = new Vector2(transform.position.x + (-speedXaxis * Time.deltaTime), transform.position.y);
                break;

            //KAMIKAZE
            case BehaviourAI.Kamikaze:
                Vector3 screenPosEnemyKamikaze = Camera.main.WorldToViewportPoint(transform.position);

                //When kamikaze behaviour must be enabled else normal movement
                if (screenPosEnemyKamikaze.x <= posScreenToKamikazeGO || Vector2.Distance(transform.position, player.transform.position) < distanceDetectPlayer && screenPosEnemyKamikaze.x <= 0.9f || kamikazeStarted)
                {
                    kamikazeStarted = true;
                    //Initialitation Kamikaze behaviour: Get current Player position
                    if (posTargetKamikaze == Vector2.zero) posTargetKamikaze = player.transform.position;

                    if (Vector2.Distance(transform.position, posTargetKamikaze) > 1 && Vector2.Distance(transform.position, player.transform.position) > 1)
                    {
                        transform.position = Vector2.MoveTowards(transform.position, posTargetKamikaze, speedForKamikazeBehaviour * Time.deltaTime);
                    }
                    else
                    {
                        KamikazeExplosion(transform.position, radExplosion);
                    }
                }
                else transform.position = new Vector2(transform.position.x + (-speedXaxis * Time.deltaTime), transform.position.y);

                break;

            //INVISIBLE behaviour is enabled on Start();
            //case BehaviourAI.Invisible:
            //    break;

            //SINUS
            case BehaviourAI.Sinus:
                //Enemy screen position 
                Vector3 screenPosEnemy = Camera.main.WorldToViewportPoint(transform.position);

                //Sinus Behaviour enabled if the enemy is visible on the screen if not normal movement
                if (screenPosEnemy.x <= 1.5)
                {
                    transform.position = new Vector2(transform.position.x + (-speedXaxis * Time.deltaTime),
                    offsetYposition + Mathf.Sin(phaseAngleSinus) * amplitudeSinus);
                    phaseAngleSinus += freqSinus * Time.deltaTime;

                }
                else transform.position = new Vector2(transform.position.x + (-speedXaxis * Time.deltaTime), transform.position.y);
                break;

            //MINE
            case BehaviourAI.Mine:
                //Mine movement
                transform.position = new Vector2(transform.position.x + (-speedXaxis * Time.deltaTime), transform.position.y);
                //If player is very close to mine

                {                if (Vector2.Distance(transform.position, player.transform.position) < distanceToExplosionMine)
                    //Decrease health to player, instantiate explosion and destroy particle and enemy
                    //AQUI SE DEBE LLAMAR A LA FUNCION PARA DECREMENTAR VIDA DEL PLAYER
                    //
                    if (!GoingOff)
                    {
                        GoingOff = true;
                        StartCoroutine(MineRed(StartDelayMine, Red));
                    }
                }
                break;


        }
    }


    //Kamikaze explosion
    void KamikazeExplosion(Vector2 center, float radius)
    {
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(center, radius);
        int i = 0;
        while (i < hitColliders.Length)
        {
            if (hitColliders[i].tag == player.transform.tag)
            {
                Debug.Log("Player alcanzado por la explosión");
                //RESTAR VIDA A PLAYER
                //ANIMACIÓN DE EXPLOSIÓN DEL KAMIKAZE
            }
            i++;
        }
        Destroy(this.gameObject);
    }

    //////To change visibility//////
    IEnumerator ChangeVisibilityEnemy()
    {
        yield return new WaitForSeconds(timeToWait);
        invisible = !invisible;
        InvisibleBehaviour();
        StartCoroutine(ChangeVisibilityEnemy());
    }

    void InvisibleBehaviour()
    {
        enemyRender.enabled = !invisible;
        enemyCollider.enabled = !invisible;
    }
    ////////////////////////////////

    IEnumerator MineExplode(float Delay)
    {
        yield return new WaitForSeconds(Delay);
        GameObject explosionMineParticle;
        explosionMineParticle = GameObject.FindGameObjectWithTag("Instanciador").GetComponent<Instanciador>().GiveMeObject("Explosion");
        if (explosionMineParticle == null)
        {
            explosionMineParticle = Resources.Load("Prefabs/Particles/Explosion", typeof(GameObject)) as GameObject;
        }else
        {
            //print("hemos usado el instanciadro para cargar el prefab:" + "Explosion");
        }
        //explosionMineParticle.SetActive(true);
        explosionMineParticle.transform.position = transform.position;
        GameObject explosionMine = Instantiate(explosionMineParticle, transform.position, Quaternion.identity) as GameObject;
        explosionMine.transform.localScale = new Vector3(5, 5, 5);
        Destroy(explosionMine, 3f);
        if (Vector2.Distance(transform.position, player.transform.position) < distanceToExplosionMine)
        {
            player.GetComponent<PlayerHealthController>().DoDamage(MineDamage);
        }
        Destroy(this.gameObject);
    }

    IEnumerator MineRed(float TimeBetwenColors, Color color)
    {
        yield return new WaitForSeconds(TimeBetwenColors);
        SP = gameObject.GetComponent<SpriteRenderer>();
        if (SP.color == color)
        {
            SP.color = new Color(1, 1, 1);
        }
        else
        {
            SP.color = color;
        }

        if (TimeBetwenColors < 0.05f)
        {
            StartCoroutine(MineExplode(StartDelayMine));
            TimeBetwenColors += 0.09f;
            StartCoroutine(MineRed(TimeBetwenColors, color));
        }
        else
        {
            TimeBetwenColors -= 0.1f;
            StartCoroutine(MineRed(TimeBetwenColors, color));
        }

    }

    IEnumerator ShakeCam(float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject.Find("Main Camera").GetComponent<ShakeCam>().ShakeScreen(ShakeOfStartTime);
    }
}