﻿using UnityEngine;
using System.Collections;

public class OnEndAnimation : MonoBehaviour {

    public bool animationEnded; 

    public void Start()
    {
        animationEnded = false; 
    }


    public void EndAnimation()
    {
        animationEnded = true; 
    }
}
