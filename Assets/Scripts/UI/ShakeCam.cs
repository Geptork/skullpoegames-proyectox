﻿using UnityEngine;
using System.Collections;

public class ShakeCam : MonoBehaviour {
    public float offsetx = 0.1f;
    public float offsety = 0.1f;
    private float CurrentTime;
    private float StopTime;
    private Vector3 Posicion;
    private bool shaking = false;
    public bool ShakeOnStart = false;
    public float ShakeOfStartTime = 0.0f;

    void Start()
    {
         Posicion = gameObject.transform.position;
        if (ShakeOnStart)
        {
            ShakeScreen(ShakeOfStartTime);
        }
    }

    private void UpdateScreen () {
         if(0.5 < UnityEngine.Random.RandomRange(0.0f, 1.0f))
        {
            offsetx *= -1;
        }
        if (0.5 < UnityEngine.Random.RandomRange(0.0f, 1.0f))
        {
            offsety *= -1;
        }
        Vector3 newpos = new Vector3(Posicion.x+offsetx, Posicion.y+offsety, Posicion.z);
        transform.position = newpos;

        if (Time.time > StopTime)
        {
            CancelInvoke();
            shaking = false;
        }
    }
    
    public void ShakeScreen (float time)
    {
        StopTime = Time.time + time;
        if (!shaking)
        {
            InvokeRepeating("UpdateScreen", 0.01f, 0.02f);
            shaking = true;
        }
    }

    public void StopShakeScreen()
    {
        StopTime = Time.time;
    }
}
