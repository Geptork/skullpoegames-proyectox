using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LanguageDictonary  {

	//Clave y valor
	public static Dictionary<string, string > stringList = new Dictionary<string, string>();

	public static void SetLanguage (SystemLanguage lang){

		if(lang == SystemLanguage.English || lang == SystemLanguage.Unknown){

			//Acciones

			stringList.Add("New Game","New Game");
			stringList.Add("Load Game","Load Game");
			stringList.Add("Save Game","Save Game");
			stringList.Add("Options","Options");
			stringList.Add("Tutorial","Tutorial");
			stringList.Add("Exit","Exit");
			stringList.Add("Effects Level","Effects Level");
			stringList.Add("Music Level","Music Level");
			stringList.Add("Controls","Controls");
			stringList.Add("Language","Language");
			stringList.Add("Back","Back");
			stringList.Add("Ok","Ok");
			stringList.Add("Cancel","Cancel");
			stringList.Add("Credits","Credits");
			stringList.Add("Up","Up");
			stringList.Add("Down","Down");
			stringList.Add("Left","Left");
			stringList.Add("Right","Right");
			stringList.Add("Main fire","Main fire");
			stringList.Add("Secondary fire","Secondary fire");


			//UI-CONTROLES
			
			stringList.Add("Shield", "Shield");
			stringList.Add("Life", "Life");
			stringList.Add("Score", "Score");

			stringList.Add("dirControls", "Move - Left/Right/Top/Bottom arrows");
			stringList.Add("mainAttack", "Main Attack - Left Click");
			stringList.Add("secAttack", "Secondary Attack - Right Click");


			// Dialogos

			stringList.Add("Do you want to overwrite this saved game?","Do you want to overwrite this saved game?");

            //Radios

            stringList.Add("Alert! We have detected unauthorized Kruxon on the battlefield.", "Alert! We have detected unauthorized Kruxon on the battlefield.");
            stringList.Add("The pilot's indentify is unknown.", "The pilot's indentify is unknown.");
            stringList.Add("Stay alert and report any new.", "Stay alert and report any new.");

            //Intro

            stringList.Add("intro1","An ancient race in danger of extinction was forced to leave their planet and seek new places to rebuild their civilization.");
			stringList.Add("intro2","In order to preserve lives they used Gexotark, a world engine  which replace planet's mass and atmosphere by their planetary conditions.");
			stringList.Add("intro3","On their interstellar travels found Arkelian's worlds, where launched a full scale invasion. Immediately war began between Arkelians and them.");
			stringList.Add("intro4","In the middle of war chaos, Kruxon ultimate spaceship was stolen.");
			stringList.Add("intro5","Now arkelians should take back his spaceship and struggle to survive...");
		}


		if(lang == SystemLanguage.Spanish){

			//Acciones

			stringList.Add("New Game","Nueva Partida");
			stringList.Add("Load Game","Cargar Partida");
			stringList.Add("Save Game","Guardar Partida");
			stringList.Add("Options","Opciones");
			stringList.Add("Tutorial","Tutorial");
			stringList.Add("Exit","Salir");
			stringList.Add("Effects Level","Volumen de efectos");
			stringList.Add("Music Level","Volumen de musica");
			stringList.Add("Controls","Controles");
			stringList.Add("Language","Idioma");
			stringList.Add("Back","Volver");
			stringList.Add("Ok","Aceptar");
			stringList.Add("Cancel","Cancelar");
			stringList.Add("Credits","Créditos");
			stringList.Add("Up","Arriba");
			stringList.Add("Down","Abajo");
			stringList.Add("Left","Izquierda");
			stringList.Add("Right","Derecha");
			stringList.Add("Main fire","Disparo");
			stringList.Add("Secondary fire","Disparo secundario");


			//UI-CONTROLES

			stringList.Add("Shield", "Escudo");
			stringList.Add("Health", "Vida");
			stringList.Add("Score", "Puntuacion");

			stringList.Add("dirControls", "Moverse - Flechas de izquierda/derecha/arriba/abajo");
			stringList.Add("mainAttack", "Ataque principal - click izquierdo");
			stringList.Add("secAttack", "Ataque secundario - click derecho");


			//Dialogos

			stringList.Add("Do you want to overwrite this saved game?","Quieres sobreescribir partida?");

            //Radio

            stringList.Add("Alert! We have detected unauthorized Kruxon on the battlefield.", "Alerta! Hemos detectado el uso no autorizado de la Kruxon en el campo de batalla.");
            stringList.Add("The pilot's indentify is unknown.", "La identidad del piloto es desconocida.");
            stringList.Add("Stay alert and report any new.", "Estén alerta e informen de cualquier anomalia.");

            //Intro 

            stringList.Add("intro1","Una antigua raza en peligro de extinción se ve obligada a abandonar su planeta y colonizar nuevos sistemas.");
			stringList.Add("intro2","Su única posibilidad de supervivencia es usar el Gexotark, un artefacto que reconvierte el entorno en un lugar habitable para su forma de vida.");
			stringList.Add("intro3","En sus viajes encontraron los mundos Arkelianos, donde empezó una invasión en forma de guerra entres los Arkelianos y ellos.");
			stringList.Add("intro4","En mitad del caos y la confusión, la nave Kruxon fue robada.");
			stringList.Add("intro5","Ahora los akerlianos deberán recuperar la nave y luchar por la supervivencia...");



            //Dialogos
            //Cinematica 1
            stringList.Add ("I just have to scape, left out of here! I just have to find a way to let behind, or I will die!", "Tengo que desaparecer, salir de aquí! Tengo que encontrar la manera de despistarlos o moriré!");
            stringList.Add ("Caution! Biosystem out. Scans out. Sixty seconds left to shotdown.", "Alerta! Biosistema actual incompatible. Apagado del sistema en 60 segundos.");
            stringList.Add ("Whats going on?  I must hurry to find a secure place... ", "Que le pasa a la nave? Tengo que darme prisa y encontrar un lugar seguro...");

            //Lvl 1: Espacio azul

            stringList.Add ("Caution! Biosystem searching enable. Searching biosystems...", "Alerta! Sistema de búsqueda de biosistemas habilitado. Localizando biosistemas compatibles.");
            stringList.Add ("What is this...? Radar show an automatic route. I am going it", "Que le pasa a esta chatarra? Parece haber marcado una ruta en el radar. La seguiré haber a donde me lleva.");
            stringList.Add ("Target: Find Biosystem", "Objetivo: Localizar el biosistema compatible");
            //Matas al Satelite
            stringList.Add ("Target completed", "Objetivo completado");
            stringList.Add ("Caution! Arkelian biosystem found. Kruxon's systems normalized. Shotdown canceled.", "Alerta! Biosistema Arkeliano localizado. Sistemas Kruxon adaptados y normalizados. Apagado cancelado.");
            stringList.Add("That was close", "Eso estuvo cerca");
            stringList.Add ("Caution! System scan completed. Fly system damages. Secondary systems disable.  External energy pick ups are required.", "Alerta! Escaner completado. Daños en el sistema de propulsión. Inhabilitado sistemas secundarios. Se requieren acoplamientos periódicos de energía externa.");
            stringList.Add ("I knew it, too much to be real...", "Lo sabía, demasiado bonito para ser verdad...");

            stringList.Add ("Target: Find energy pick ups", "Objetivo: Encontrar recargas energeticas");
            stringList.Add ("Target: completed", "Objetivo completado");

            stringList.Add ("Caution!  Radio messages incoming. This is Arkalio-Vega II Fleet. We need back up! We are under attack by unknown creature. Permit to active protocol…", "Alerta! Mensaje de radio entrante. Aquí la flota Arkalio-Vega II. Solicitamos refuerzos! Estamos siendo atacados por una criatura no identificada. Permiso para ejecutar el protocolo de… ");
            stringList.Add ("Present route canceled. Redefine default route. Emergency propulsion system activated.", "Cancelando ruta actual. Restableciendo ruta por defecto. Sistema de propulsión de emergencia activado.");
            stringList.Add ("What? Why did you change that? I dont want to go! How I can chance it stupid machine?!", "Pero que? Porque has cambidado? No quiero ir! Como puedo cambiarla estupida maquina?!");


            stringList.Add ("Target: Find and destroy the creature", "Objetivo: Encontrar y destruir la criatura");
            stringList.Add ("Target: completed", "Objetivo completado");


            //Lvl 2: Espacio rojo


            stringList.Add ("Caution! System scan completed! Target system damaged. Fix function is requiered. Initialing basic target system.", "Alerta! Diagnóstico de la nave completado! Daños en el sistema de disparo. Reparación necesaria. Activando mecanismo de disparo bàsico.");

            stringList.Add ("Target: Find and fix the target sytem", "Objetivo: Encontrar y reparar el sistema de punteria");

            stringList.Add ("Planet route found. Checking outer Delxiko system", "Ruta planetaria encontrada. Analizando exteriores del sistema Delxiko");
            stringList.Add ("Seems I am alone again, but.. why are they so interested? Why they follow me?", "Parece que ya no me siguen... pero... porque se ponen tan pesados? Por que no paran de perseguirme?");
            stringList.Add ("Caution! Old warzone detected. Trash and dead is on the road. Avalible secondary routes.", "Alerta!, Detectada zona de guerra abandonada. Ruta interrumpida por escombros.  Disponible rutas alternativas.");
            stringList.Add ("I prefer the present route. Trashes can hide me", "Prefiero seguir la ruta en curso. Los escombros pueden ocultarme.");
            stringList.Add ("Redefining default route.", "Recalculando la ruta por defecto.");
            stringList.Add ("Are you get it?!", "Lo has captado?!");
            stringList.Add ("Caution! Strange criature incoming", "Alerta! Se aproxima criatura no identificada");

            stringList.Add ("Target: Fight and destroy the creature", "Objetivo: Combatir y eliminar a la criatura");
            stringList.Add ("Target: completed", "Objetivo completado");

            stringList.Add ("Caution!  Weapon system  available detected", "Alerta! Sistema de armamento compatible detectado");

            stringList.Add ("Target: completed", "Objetivo completado");

            stringList.Add ("Can you stop to say caution?", "Puedes dejar de decir alerta?");
            stringList.Add ("Can let me make orders? Caution! Caution! Caution!", "Puedes dejar de mandar? Alerta! alerta! alerta!");
            stringList.Add ("Alert, we made contact with the target. Change to reaper mode", "Alerta, tenemos contacto visual con el objetivo. Pasamos a modo furtivo");
            stringList.Add ("Find me again?!  If I could arrive the planet. I must try. My only choice to escape.", "Me han pillado?!  Si pudiera llegar a ese planeta. Debo intentarlo. Es mi única opción de escapar.");
            stringList.Add ("Caution! Planet detected. Name Niemis. Checking status. Population off. 70% desert. 80% Tronás atmosphere. Last planet under Arkelian planetary system", "Alerta! Planeta detectado. Nombre Niemis. Analizando condiciones. Planeta deshabitado; 70% desertico; 80% de gas Tronás. Último planeta anexado al tratado de territorios planetarios del sistema Arkeliano.");
            stringList.Add ("A desert with Tronás atmosphere? These is unbreathable! What can I do?!", "Un desierto con gas Tronás? Ese gas es irrespirable! Que hago?!");
            stringList.Add ("Alert! Space suit have a basic air generator", "Alerta! El traje posee un generador basico de aire.");
            stringList.Add ("Are you saying that I can hold the air on the planet for a while?",  "Estas diciendo que tendré que aguantar la respiración?");
            stringList.Add ("Generator has limited work. Require continue pick ups to complete clean toxic sequence", "El generador tiene un limite de uso. Requiere de recarga constante para completar la secuencia de limpiado");
            stringList.Add ("Target engaged. Repeat, target engaged. Waiting orders", "Objetivo fijado. Repito, objetivo fijado. Esperando ordenes.");
            stringList.Add ("Die here or there...", "Morir aquí o abajo...");
            stringList.Add ("Thermal shields activated. Descending to the planet now", "Escudos termicos activados. Descendiendo al planeta ahora");



            //Lvl 3: Planeta desertico


            stringList.Add ("Target I: Find air pickups", "Objetivo I: Encontrar recargas de aire");
            stringList.Add ("We have the position of the target. Ready to fire", "Tenemos la posición del objetivo. Preparados para disparar");
            stringList.Add ("This shit hasnt more speed? ","Este cacharro no alcanza mas velocidad?");
            stringList.Add ("Caution! The system has converse engine on emergencies.  Extra limit speed. Need elements support. Element detected.", "Alerta!  El sistema tiene un conversor de energía para casos de emergencia. Posee capacidad limitada extra. Se requieren elementos compatibles. Elemento compatible detectado.");
            stringList.Add ("Where?  I cant see it", "Donde?  No puedo verlo");
            stringList.Add ("Tronás gas is element supported by the system. Use Tronás to increase the power.", "El gas Tronás es un elemento de conversión compatible con el sistema. Es posible incrementar la potencia mientras existan suministros de gas Tronás.");
            stringList.Add ("Planet has 80% Tronás atmosphere, isnt? Well... isnt so bad", "No dijistes que el planeta poseía un 80% de atmósfera Tronás? Bueno... no es tan malo.");

            stringList.Add ("Target going faster! He is leaving our systems!  Change actions requiered!", "El objetivo aumenta su velocidad! Se escapa de nuestros sistemas! Se requiere procedimiento!");

            stringList.Add ("Caution!, strange creature incoming.", "Alerta, criatura desconocida aproximándose.");
            stringList.Add ("Other? Where is it?", "Otra? Donde esta?");
            stringList.Add ("Caution!, strange element detected. Redirecting energy system.", "Alerta, elemento desconocido detectado. Redirigiendo energía de propulsión.");
            stringList.Add ("No! We are lost without it!", "No! estaremos perdidos si lo haces!");
            stringList.Add ("Caution! Strange element inside attack range. I defined shape", "Alerta! Elemento desconocido dentro del rango de ataque. Se observa una forma definida");

            stringList.Add ("Target II: Destroy the shape", "Objetivo II: Destruir la forma");
            stringList.Add ("Target II: completed", "Objetivo II completado");
            stringList.Add ("Target I: completed", "Objetivo I completado");


            stringList.Add ("We engaged him, ready to fire", "Le tenemos fijado, preparados para disparar");
            stringList.Add ("Caution! Converse engine malfunction. Decreasing power.", "Alerta! Mal funcionamiento del conversor de elementos, reduciendo potencia.");
            stringList.Add ("No!", "No!");
            stringList.Add ("Warning. Drive system fail. Hold system damaged. Emergency system is activated.", "Atención. Fallo en el sistema de dirección. Daños en el sistema de apuntado. Sistema de emergencia activado.");
            stringList.Add ("Target down, extraction process on", "Objetivo derribado, Modo extracción activado");

        }

		if(lang == SystemLanguage.Polish){

			//Acciones 

			stringList.Add("New Game","Nowa gra");
			stringList.Add("Load Game","Wczytaj grę");
			stringList.Add("Save Game","Zapisz  grę");
			stringList.Add("Options","Opcje");
			stringList.Add("Tutorial","Samouczek");
			stringList.Add("Exit","Opuść grę");
			stringList.Add("Effects Level","Głośność efektów");
			stringList.Add("Music Level"," Głośność Muzyki");
			stringList.Add("Controls","Sterowanie");
			stringList.Add("Language","Język");
			stringList.Add("Back","Wstecz");
			stringList.Add("Ok","Akceptuj");
			stringList.Add("Cancel","Anuluj");
			stringList.Add("Credits","Twórcy");
			stringList.Add("Up","Góra");
			stringList.Add("Down","Dół");
			stringList.Add("Left","Lewo");
			stringList.Add("Right","Prawo");
			stringList.Add("Main fire","Strzelać");
			stringList.Add("Secondary fire","Strzelać Drugorzędna");


			//UI-CONTROLES
			
			stringList.Add("Shield", "Tarcza");
			stringList.Add("Health", "życie");
			stringList.Add("Score", "Wynik");

			stringList.Add("dirControls", "Moverse - Kierunek lewo/prawo/góra/dół");
			stringList.Add("mainAttack", "AStrzelać - Lewo klawisza myszy");
			stringList.Add("secAttack", "Strzelać Drugorzędna - Prawego klawisza myszy");


			//Dialogos

			stringList.Add("Do you want to overwrite this saved game?","Czy chcesz zamienić zapis gry?");

            //Radio

            stringList.Add("Alert! We have detected unauthorized Kruxon on the battlefield.", "Uwaga! Wykryliśmy nieautoryzowany Kruxon na polu bitwy.");
            stringList.Add("The pilot's indentify is unknown.", "Tożsamość pilota jest nieznana.");
            stringList.Add("Stay alert and report any new.", "Bądźcie czujni i informujcie na bieżąco.");

            //Intro

            stringList.Add("intro1","Zagrożona wyginięciem rasa pradawnych istot została zmuszona do odnalezienia planety, na której będzie mogła odbudować swoją cywilizację.");
			stringList.Add("intro2","Ma im w tym pomóc urządzenie zwane Gexotark, za pomocą którego będą mogli dowolnie zmienić ekosystem całego globu.");
			stringList.Add("intro3","Za nowy dom wybrali najbliższą bogatą w wartościowe surowce planetę świat Arkelian, między dwoma gatunkami wybuchła walka o przetrwanie.");
			stringList.Add("intro4","Po środku wojennego chaosu niespodziewanie znalazł się Kruxon, największy statek bitewny Arkelian.");
			stringList.Add("intro5","Teraz muszą go nie tylko odzyskać, ale i jak najszybciej ustalić tożsamość pilota oraz jego zamiary...");
		}
	
		if(lang == SystemLanguage.Italian){

			//Acciones 

			stringList.Add("New Game","Nuova Partita");
			stringList.Add("Load Game","Carica Partita");
			stringList.Add("Save Game","Salva Partita");
			stringList.Add("Options","Opzioni");
			stringList.Add("Tutorial","Tutorial");
			stringList.Add("Exit","Esci dal Gioco");
			stringList.Add("Effects Level","Volume effeti");
			stringList.Add("Music Level"," Volume Musica");
			stringList.Add("Controls","Comandi");
			stringList.Add("Language","Lingua");
			stringList.Add("Back","Indietro");
			stringList.Add("Ok","Acordo");
			stringList.Add("Cancel","Anulla");
			stringList.Add("Credits","Riconoscimenti");
			stringList.Add("Up","Su");
			stringList.Add("Down","Giù");
			stringList.Add("Left","Sinistra");
			stringList.Add("Right","Destra");
			stringList.Add("Main fire","Sparo principale");
			stringList.Add("Secondary fire","Sparo secondario");


			//UI-CONTROLES
			
			stringList.Add("Shield", "Scudo");
			stringList.Add("Health", "Vita");
			stringList.Add("Score", "Punteggio");
			stringList.Add("dirControls", "Movimento - Freccia di sinistra/destra/su/giù");
			stringList.Add("mainAttack", "Sparo principale - Cliccando il  destro del mouse ");
			stringList.Add("secAttack", "Sparo secondario - Cliccando il  sinistro del mouse ");


			//Dialogos

			stringList.Add("Do you want to overwrite this saved game?","Vuoi Sovrascrivere questa partita?");

            //Radio

            stringList.Add("Alert! We have detected unauthorized Kruxon on the battlefield.", "All'erta! Abbiamo individuato di utilizzo non autorizzato di Kruxon su campo di battaglia.");
            stringList.Add("The pilot's indentify is unknown.", "L'identità del piloto è sconosciuto.");
            stringList.Add("Stay alert and report any new.", "Essere preparati e rapporto di qualsiasi anomalìa.");

            //Intro

            stringList.Add("intro1","Un'antica razza  in via di estinzione costretto a lasciare di pianeta e colonizzare nuovo mondi.");
			stringList.Add("intro2","l’Unica soluzione per sopravvivere sono utilizzare il motore Gexotark, una tecnologia fondamentale per modificare la massa e l'atmosfera di qualsiasi pianeta per loro condizioni planetarie.");
			stringList.Add("intro3","Avevano trovato l'Arkelian mondi, dove cominciarono un invasione sotto forma di conflitti armati.");
			stringList.Add("intro4","Tra caos e confusione, il veicolo spaziale Kruxon fu rubó.");
			stringList.Add("intro5","Arkelians essere recuperare il veicolo spaziale e combattere per la sua sopravvivenza...");
		}


		if(lang == SystemLanguage.Catalan){

			//Acciones

			stringList.Add("New Game","Nova Partida");
			stringList.Add("Load Game","Carregar Partida");
			stringList.Add("Save Game","Desa Partida");
			stringList.Add("Options","Opcions");
			stringList.Add("Tutorial","Tutorial");
			stringList.Add("Exit","Sortir");
			stringList.Add("Effects Level","Volum d'Efectes");
			stringList.Add("Music Level","Volum de Musica");
			stringList.Add("Controls","Controls");
			stringList.Add("Language","Llenguatge");
			stringList.Add("Back","Tornar");
			stringList.Add("Ok","Acceptar");
			stringList.Add("Cancel","Cancel·la");
			stringList.Add("Credits","Crèdits");
			stringList.Add("Up","Amunt");
			stringList.Add("Down","A baix");
			stringList.Add("Left","Esquerra");
			stringList.Add("Right","Dreta");
			stringList.Add("Main fire","Foc primari");
			stringList.Add("Secondary fire","Foc secundari");


			//UI-CONTROLES
			
			stringList.Add("Shield", "Escut");
			stringList.Add("Health", "Vida");
			stringList.Add("Score", "Puntuació");
			stringList.Add("dirControls", "Moure's - fletxes esquerra/dreta/adalt/abaix");
			stringList.Add("mainAttack", "Atac principal - click esquerra");
			stringList.Add("secAttack", "Atac secondari - click dret");


			//Dialogos

			stringList.Add("Do you want to overwrite this saved game?","Vols sobreescriure la partida?");

            //Radio

            stringList.Add("Alert! We have detected unauthorized Kruxon on the battlefield.", "Alerta! Hem detectat l'ús no autoritzat de la Kruxon en el camp de batalla.");
            stringList.Add("The pilot's indentify is unknown.", "l' Identitat del pilot es desconeguda.");
            stringList.Add("Stay alert and report any new.", "Estigueu alerta i informeu de qualsevol anomalia.");

            //Intro

            stringList.Add("intro1","Una antiga rasa en perill d'extinció es veu obligada a deixar el seu món i colonitzar nous planetes.");
			stringList.Add("intro2","l'Única possibilitat de sobreviure es utilitzant el Gexotark, un eina capaç de modifica l'entorn per adaptar la seva forma de vida.");
			stringList.Add("intro3","En el seu viatge van trobar el mons Arkelians, on va començar una invasió en forma de guerra entre Arkelians i ells.");
			stringList.Add("intro4","En mig del conflicte, la nau Kruxon va ser robada.");
			stringList.Add("intro5","Ara, els Arkelians hauran de recuperar la nau i lluita per la seva supervivència...");
		}


		if(lang == SystemLanguage.Romanian){
			
			//Acciones
			
			stringList.Add("New Game","Joc Nou");
			stringList.Add("Load Game","Încarcă joc");
			stringList.Add("Save Game","Salvează jocul");
			stringList.Add("Options","Opțiuni");
			stringList.Add("Tutorial","Tutorial");
			stringList.Add("Exit","Ieșire");
			stringList.Add("Effects Level","Volumul efectelor");
			stringList.Add("Music Level","Volum muzică");
			stringList.Add("Controls","Controale");
			stringList.Add("Language","Limba");
			stringList.Add("Back","Înapoi");
			stringList.Add("Ok","Acceptă");
			stringList.Add("Cancel","Anulare");
			stringList.Add("Credits","Acreditare");
			stringList.Add("Up","Sus");
			stringList.Add("Down","Jos");
			stringList.Add("Left","Stânga");
			stringList.Add("Right","Dreapta");
			stringList.Add("Main fire","Foc");
			stringList.Add("Secondary fire","Foc secundar");
			
			
			//UI-CONTROLES
			
			stringList.Add("Shield", "Scut");
			stringList.Add("Health", "Viață");
			stringList.Add("Score", "Scor");
			stringList.Add("dirControls", "Mișcare - Săgeți din stânga/dreapta/sus/jos");
			stringList.Add("mainAttack", "Atac principal - click stânga");
			stringList.Add("secAttack", "Atac secundar - click dreapta");
			
			
			//Dialogos
			
			stringList.Add("Do you want to overwrite this saved game?","Vreți să suprascrieți?");
			
			//Radio
			
            stringList.Add("Alert! We have detected unauthorized Kruxon on the battlefield.", "Alerta! Am detectat uzul neautorizat al Kruxonului în câmpul de luptă.");
            stringList.Add("The pilot's indentify is unknown.", "Identitatea pilotului nu este cunoscută.");
            stringList.Add("Stay alert and report any new.", "Fiți alertă și raportați orice anomalie.");
            //Intro

            stringList.Add("intro1","O rasă străveche pe cale de dispariție se vede obligată să abandoneze planeta și să colonizeze noi sisteme.");
			stringList.Add("intro2","Unica lor posibilitate de supraviețuire este folosind Gexotarkul, un obiect ce reconvertește mediul intr-un ambit locuibi pentru forma lor de vița.");
			stringList.Add("intro3","În călătoriile lor vor găsi lumiile Arkeliane, unde încep o invaziune în formă de război între ei și arkeliani.");
			stringList.Add("intro4","În mijlocul caosului și a confuziei, nava Kruxon a fost furată.");
			stringList.Add("intro5","Acum akerliani vor trebui să recupereze nava și să lupte pentru supraviețuire...");
		}

	}
}
