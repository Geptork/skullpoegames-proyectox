using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FPSDisplay : MonoBehaviour
{
	float deltaTime = 0.0f;
    public Text FPS;
    public bool HideFPS = false;

    void Start()
    {
        if (HideFPS)
        {
            FPS.enabled = false ;
        }
    }
    void Update()
	{
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        if (Time.timeSinceLevelLoad > 1)
        {
            UpdateFPSInfo();
        }
	}

	void UpdateFPSInfo()
	{
		float fps = 1.0f / deltaTime;
        //Debug.LogWarning("<color=blue>Fps:</color>" + UnityEngine.Mathf.RoundToInt(fps));        
        if(fps < 35.0f && !HideFPS)
        {
            Debug.LogError("Huge FPS drop");
        }
        FPS.text = UnityEngine.Mathf.RoundToInt(fps).ToString();
    }
}
