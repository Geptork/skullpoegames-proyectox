﻿using UnityEngine;
using System.Collections;

public class FramesController : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {

        bool destroy = true;
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            if (!this.transform.GetChild(i).GetComponent<OnEndAnimation>().animationEnded)
            {
                destroy = false;
            } 
        }

        if (destroy)
        {
            Destroy(this.gameObject); 
        }

	}
}
