﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour {

	public float timespan;
	//public Component image;
	private float initialTimespan;
	private Color initialColor;
	private float initialDifferenceToTheMax_alpha;
	public Image PanelFade;
	// Use this for initialization
	void Start () {
		initialTimespan = timespan;
        PanelFade = gameObject.GetComponent<Image>();
		initialColor = PanelFade.color;
		initialDifferenceToTheMax_alpha = 1 - initialColor.a;
		//initialAlpha = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		timespan -= Time.deltaTime;
		Color newColor = new Color(initialColor.r, initialColor.g, initialColor.b, initialColor.a + (1f - (timespan / initialTimespan)) * initialDifferenceToTheMax_alpha);
        PanelFade.color = newColor;
		
		if (timespan < 0) Destroy (this);
	}
}
