﻿using UnityEngine;
using System.Collections;

public class AspectRelation : MonoBehaviour
{
    public float RelPosX;
    public float RelPosY;

	// Use this for initialization
	void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector2 sitiodondevalacamara = Camera.main.ViewportToWorldPoint(new Vector2(RelPosX, RelPosY));
        transform.position = sitiodondevalacamara;
    }
}
