﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class SetLanguage : MonoBehaviour {

	void Awake()
	{
		if(this.gameObject.name != "Main Camera")
			ChangeLanguage(this.gameObject);



	}

	public void ChangeLanguage(GameObject go){
		if(go.name == "English"){
			LanguageDictonary.stringList.Clear();
			LanguageDictonary.SetLanguage(SystemLanguage.English);
		}
		else if(go.name == "Spanish"){
			LanguageDictonary.stringList.Clear();
			LanguageDictonary.SetLanguage(SystemLanguage.Spanish);
		}
		else if (go.name == "Polish"){
			LanguageDictonary.stringList.Clear();
			LanguageDictonary.SetLanguage(SystemLanguage.Polish);
		}
		else if (go.name == "Italian"){
			LanguageDictonary.stringList.Clear();
			LanguageDictonary.SetLanguage(SystemLanguage.Italian);
		}
		else if (go.name == "Catalan"){
			LanguageDictonary.stringList.Clear();
			LanguageDictonary.SetLanguage(SystemLanguage.Catalan);
		}
        else if (go.name == "Romana")
        {
            LanguageDictonary.stringList.Clear();
            LanguageDictonary.SetLanguage(SystemLanguage.Romanian);
        }

        if (go.name != "ControlMenu"){
		PlayerPrefs.SetString("Language",go.name);
		SceneManager.LoadScene("UI");
		}

	}


}
