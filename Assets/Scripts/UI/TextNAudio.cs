﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class TextNAudio_atom
{
	public string nameOfText;
	
	public AudioClip clip;
}

public class TextNAudio : MonoBehaviour {

	public TextNAudio_atom[] textNAudios;
	private int tnaIndex;
	public AudioSource audioS;
	private Text text;
	// Use this for initialization
	void Start () {
		tnaIndex = 0;
		text = gameObject.GetComponent<Text>();
		StartCoroutine (TextNAudioShow());
	}
	
	// Update is called once per frame
	IEnumerator TextNAudioShow () {
		FadeOut component;
	
		while (tnaIndex < textNAudios.Length)
		{
			TextNAudio_atom atom = textNAudios[tnaIndex];
			
			audioS.clip = atom.clip;
			audioS.Play ();
			if(LanguageDictonary.stringList.Count > 0)
				text.text = LanguageDictonary.stringList[atom.nameOfText];
			else
				text.text = atom.nameOfText + "ERROR_DICTIONARY_DOES_NOT_WORK";
			++tnaIndex;
			yield return new WaitForSeconds(audioS.clip.length + 1f);
		}
		
		component = this.gameObject.AddComponent<FadeOut>();
		component.timespan = 2f;
		
		yield return new WaitForSeconds(2f);

        SceneManager.LoadScene("DemoTesting");
	}
}
