﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VariableBar : MonoBehaviour
{
    [SerializeField]
    private Image color;

    [SerializeField]
    private Image border;

    [SerializeField]
    private Image background;

    private float relativeSize = 1;
    public float FillAmount
    {
        get { return color.fillAmount; }
        set
        {
            color.fillAmount = value * relativeSize;
        }
    }

    public float RelativeSize
    {
        set
        {
            float v = Mathf.Clamp01(value);
            relativeSize = v;
            color.fillAmount = v;
            background.fillAmount = v;

            Vector2 borderPos = new Vector2();
            borderPos.x = v * color.rectTransform.rect.width;
            //border.rectTransform.anchoredPosition = borderPos;
        }

        get { return relativeSize; }
    }
}