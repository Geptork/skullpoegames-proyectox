﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{

    void Start()
    {
        if (UnityEngine.Cursor.visible == false)
        {
            UnityEngine.Cursor.visible = true;
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("app_intro1");
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
