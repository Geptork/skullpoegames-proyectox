﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ChangeButtons : MonoBehaviour {
	private bool buttonClick = false;
	private string[] letters;
	private Text temp;

	public GameObject MainShoot;
	public GameObject SecondShot;
	public GameObject Up,Down;
	public GameObject Left,Right;




	// Use this for initialization
	void Start () {


		letters = new string []{
			"delete",
			"tab",
			"clear",
			"return",
			"pause",
			"escape",
			"space",
			"up",
			"down",
			"right",
			"left",
			"insert",
			"home",
			"end",
			"page up",
			"page down",
			"f1",
			"f2",
			"f3",
			"f4",
			"f5",
			"f6",
			"f7",
			"f8",
			"f9",
			"f10",
			"f11",
			"f12",
			"f13",
			"f14",
			"f15",
			"0",
			"1",
			"2",
			"3",
			"4",
			"5",
			"6",
			"7",
			"8",
			"9",
			"!",
			"#",
			"$",
			"&",
			"'",
			"(",
			")",
			"*",
			"+",
			",",
			"-",
			".",
			"/",
			":",
			";",
			"<",
			"=",
			">",
			"?",
			"@",
			"[",
			"\\",
			"]",
			"^",
			"_",
			"`",
			"a",
			"b",
			"c",
			"d",
			"e",
			"f",
			"g",
			"h",
			"i",
			"j",
			"k",
			"l",
			"m",
			"n",
			"o",
			"p",
			"q",
			"r",
			"s",
			"t",
			"u",
			"v",
			"w",
			"x",
			"y",
			"z",
			"numlock",
			"caps lock",
			"scroll lock",
			"right shift",
			"left shift",
			"right ctrl",
			"left ctrl",
			"right alt",
			"left alt"
		};
	}
	
	// Update is called once per frame
	void Update () {
		if(buttonClick){

			foreach(string n in letters){
				if(Input.GetKeyDown(n)){
					Debug.Log(n);
					temp.text = n.ToLower();
				}
			}

				


		}
	}

	public void ButtonClicked(GameObject t){
		if(t.name == MainShoot.name){
			temp = MainShoot.GetComponent<Text>();
		}

		if(t.name == SecondShot.name){
			temp = SecondShot.GetComponent<Text>();
		}

		if(t.name == Up.name){
			temp = Up.GetComponent<Text>();
		}

		if(t.name == Down.name){
			temp = Down.GetComponent<Text>();
		}

		if(t.name == Left.name){
			temp = Left.GetComponent<Text>();
		}

		if(t.name == Right.name){
			temp = Right.GetComponent<Text>();
		}

		temp.text="...";
		buttonClick = true;

	}

}
