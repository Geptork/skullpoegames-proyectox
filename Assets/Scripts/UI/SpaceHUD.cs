﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpaceHUD : MonoBehaviour
{
    // The health bar.
    [SerializeField]
    private VariableBar healthBar;
    // The shield bar.
    [SerializeField]
    private VariableBar shieldBar;
    [SerializeField]
    private VariableBar FuelBar;
    [SerializeField]
    private VariableBar BossBar;

    [HideInInspector]
    public GameObject BossUI;
    BlinzIA IA;

    // The score text.
    [SerializeField]
    private Text scoreText;
    // The total score.
    private int totalScore;

    [Header("Mouse")]
    public bool HideMouse = false;


    void Start()
    {
        if (HideMouse)
        {
            UnityEngine.Cursor.visible = false;
        }

        BossUI = GameObject.Find("BossUI");
        BossUI.SetActive(false);

        IA = GameObject.Find("Manager").GetComponentInChildren<BlinzIA>();
    }
    void Update()
    {
        scoreText.text = TotalScore.ToString();
        
    }

    public void UpdateHUD(float CurrentHealth, float CurrentShield, float MaxHealth, float MaxShield)
    {
        healthBar.transform.FindChild("Color").GetComponent<Image>().fillAmount = (CurrentHealth / MaxHealth);
        shieldBar.transform.FindChild("Color").GetComponent<Image>().fillAmount = (CurrentShield / MaxShield);
    }
    public void UpdateFuel(float CurrentFuel, float MaxFuel)
    {
        FuelBar.transform.FindChild("Color").GetComponent<Image>().fillAmount = (CurrentFuel / MaxFuel);
    }
    public void UpdateForBoss(float damaged)
    {
        
        BossBar.transform.FindChild("BossColor").GetComponent<Image>().fillAmount = ((IA.MaxHealth - damaged) / IA.MaxHealth);

        if(IA.MaxHealth <= damaged)
        {
            BossBar.transform.FindChild("BossColor").GetComponent<Image>().fillAmount = 0f;
        }


    }
    public int TotalScore
    {
        get { return totalScore; }
        set
        {
            totalScore = value;
        }
    }

}