﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Contador60 : MonoBehaviour
{
    public Text Contador;
    public float InitValue;

    private float counter;
    //el resultado por defecto es 0, lo pongo a 60 para q entre en los ifs del update y se setee bien
    private int result = 60;

    private bool Died = false;

    //Satelite
    public GameObject Satelite;

    [HideInInspector]
    public bool SatItemGrab = false;

    private TextManager TxtMan;

    void Start()
    {
        TxtMan = GameObject.Find("SpaceGameplay").GetComponent<TextManager>();
    }
    void Update ()
    {
        //contador que incrementa por segundos
        if(SatItemGrab == false)
            counter += Time.deltaTime;

        if (result != 0 && Satelite.GetComponent<SateliteIA>().Died == false)
        {
            //resto el valor incial definido por inspector con el contador para hacer una cuenta atras
            float resta = InitValue - counter;
            //paso a int para quitar los decimales
            result = (int)resta;
            //pinto el resultado al text de la UI (Contador)
            Contador.text = result.ToString();
        }
        //si el tiempo llega a 0, paro la resta y mato al player
        if (result <= 0 && Died == false && Satelite.GetComponent<SateliteIA>().Died == false)
        {
            counter = 0;
            Contador.text = 0.ToString();
            ForcePlayerKill();
            Died = true;
        }

        //si matas al satelite para la cuenta atras (Debes coger el item) y llama al evento de dialogo
        else if (Satelite.GetComponent<SateliteIA>().Died == true && result != 0 && SatItemGrab == true)
        {
            Contador.text = "";

            TxtMan.SattelitDied = true;

        }
    }
    void ForcePlayerKill()
    {
        PlayerHealthController PlayerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealthController>();
        float CurrentHealth = PlayerHealth.GetHealth();
        float CurrentShield = PlayerHealth.GetShield();
        PlayerHealth.DoDamage(CurrentShield + CurrentHealth);
    }
}
