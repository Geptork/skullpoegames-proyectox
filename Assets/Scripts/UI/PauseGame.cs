﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseGame : MonoBehaviour
{

    public bool isPaused;
    AudioSource Music;
    //PARA UN FUTURO MENU DE PAUSA
    private GameObject MPausa;

    // Use this for initialization
    void Start()
    {

        isPaused = false;
        Music = GameObject.Find("Music_Manager").GetComponent<AudioSource>();
        MPausa = GameObject.Find("PauseMenu");
        MPausa.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape) && isPaused == false)
        {
            Time.timeScale = 0.0f;
            isPaused = true;
            Music.volume = 0.15f;
            MPausa.SetActive(true);
            UnityEngine.Cursor.visible = true;
        }
        else if
        (Input.GetKeyDown(KeyCode.Escape) && isPaused == true)
        {
            Time.timeScale = 1.0f;
            isPaused = false;
            Music.volume = 0.6f;
            MPausa.SetActive(false);
            UnityEngine.Cursor.visible = false;
        }
    }
    public void Restart()
    {
        Time.timeScale = 1.0f;
        isPaused = false;
        Music.volume = 0.6f;
        MPausa.SetActive(false);
        UnityEngine.Cursor.visible = false;
    }
}