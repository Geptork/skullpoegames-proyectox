﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class StartLevel : MonoBehaviour
{

    public GameObject menu;

    public void LoadLevel(string levelName)
    {
        PlayerPrefs.SetInt("LoadTag", 0);
        SceneManager.LoadScene("intro");
    }

    public void ExitApplication()
    {
        Application.Quit();
    }

    public void ChangeLevel(GameObject go)
    {
        menu.SetActive(false);
        go.SetActive(true);
    }

    public void BackLevel(GameObject go)
    {
        menu.SetActive(true);
        go.SetActive(false);
    }

    public void ChangeLanguage(GameObject go)
    {
        SceneManager.LoadScene("Languages");

    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits");

    }

    public void LoadSavedGame()
    {
        PlayerPrefs.SetInt("LoadTag", 1);
        SceneManager.LoadScene(PlayerPrefs.GetString("Level"));
    }

}
