﻿using UnityEngine;
using System.Collections;

public class OnMouseHoverSound : MonoBehaviour {

	private AudioClip audioC;
	
	private AudioSource audioS;

	// Use this for initialization
	void Start()
	{
		audioC = Resources.Load<AudioClip>("s_menu_seleccion");
		audioS = FindObjectOfType<MusicManager>().gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseEnter()
	{
		audioS.clip = audioC;
		audioS.Play();
	}
}
