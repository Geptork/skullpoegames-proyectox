﻿using UnityEngine;
using System.Collections;

public class MoveDown : MonoBehaviour {
    public SpriteAnimator SA;
    public float Velocity = 0.1F;
    public float MaxDesp = 1.1f;
    public float delay = 0.2f;
    private float desp = 0f;
    private bool go = false;

    // NO USES ESTE SCRIPT PARA NADA, ES PARA ARREGLAR UNA PUTA MIERDA DE ANIMACION EN PNGS

    void Update() {
        if (SA != null) {
            StartCoroutine(DELAY());
            if (go)
            {
                if (desp < MaxDesp)
                {
                    transform.position += new Vector3(0, -Velocity, 0);
                }
                desp += Velocity;
            }
        }
    }

    IEnumerator DELAY() { 
    yield return new WaitForSeconds(delay);
        go = true;
    }
}
