﻿using UnityEngine;
using System.Collections;

public class Fuel : MonoBehaviour {
    public float CurrentFuel = 1000;
    public float MaxFuel = 1000;
    public bool DecreaseOnStart = false;
    public float TimeBetweenPeriodicDecrease = 1f;
    public float PeriodicFuelLoose = 1f;
    public bool Decreasing = false;

    private SpaceHUD HUD;

	public void IncreaseFuel(float FuelToincrease){
        CurrentFuel += FuelToincrease;
        if(CurrentFuel >= MaxFuel)
        {
            CurrentFuel = MaxFuel;
        }
        UpdateHUD();
   }

    public void StopDecreasing()
    {
        Decreasing = false;
    }

    public void StartDecreasing()
    {
        Decreasing = true;
    }

    public void DecreaseFuel(float FuelTodecrease)
    {
        CurrentFuel -= FuelTodecrease;
        if (CurrentFuel <= 0)
        {
            CurrentFuel = 0;
        }
        UpdateHUD();
    }

    void Start()
    {
        if (DecreaseOnStart)
        {
            Decreasing = true;
        }
        StartCoroutine(DecreseFuelPerTime());

        HUD = GameObject.Find("GameplayPrueba").GetComponent<SpaceHUD>();
    }

    IEnumerator DecreseFuelPerTime()
    {
        yield return new WaitForSeconds(TimeBetweenPeriodicDecrease);
        if (Decreasing)
        {
            DecreaseFuel(PeriodicFuelLoose);
        }
        StartCoroutine(DecreseFuelPerTime());
    }

    private void UpdateHUD()
    {
        HUD.UpdateFuel(CurrentFuel, MaxFuel);
    }
}
