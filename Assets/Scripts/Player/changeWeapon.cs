﻿using UnityEngine;
using System.Collections;

public class changeWeapon : MonoBehaviour
{

    public KruxonShotControl KSC;
    private GameObject initialWeaponM;
    private GameObject initialWeaponS;
    private GameObject initialWeaponSp;
    private float initialFirerateM;
    private float initialFirerateS;
    private float initialDamageM;
    private float initialDamageS;
    private float initialDamageSp;
    private int prevMaxAmmo;
    private int prevAmmo;
    private float initialMOfsetX;
    private float initialMOfsetY;
    private float initialSOfsetX;
    private float initialSOfsetY;
    private float initialSPOfsetX;
    private float initialSPOfsetY;

    void Start()
    {
        initialWeaponM = KSC.mainBullet;
        initialWeaponS = KSC.secondaryBullet;
        initialWeaponSp = KSC.specialBullet;
        initialFirerateM = KSC.mainFireRate;
        initialFirerateS = KSC.secondaryFireRate;

        initialMOfsetX = KSC.XpositionMainBullet;
        initialMOfsetY = KSC.YpositionMainBullet;
        initialSOfsetX = KSC.XpositionSecondaryBullet;
        initialSOfsetY = KSC.YpositionSecondaryBullet;
        initialSPOfsetX = KSC.XpositionSpecialBullet;
        initialSPOfsetY = KSC.YpositionSpecialBullet;

        prevMaxAmmo = KSC.maxSpecialAmmunition;
        prevAmmo = KSC.specialAmmunition;
        initialDamageM = KSC.mainBullet.GetComponent<bulletController2>().damage;
        initialDamageS = KSC.secondaryBullet.GetComponent<bulletController2>().damage;
        initialDamageSp = KSC.specialBullet.GetComponent<bulletController2>().damage;
    }

    //Cambiar el arma principal entera
    public void ChangeMainWeapon(GameObject Bullet, float FireRate, float Xposition, float Yposition, bool Permanent)
    {
        KSC.mainBullet = Bullet;
        KSC.mainFireRate = FireRate;
        KSC.XpositionMainBullet = Xposition;
        KSC.YpositionMainBullet = Yposition;

        if (Permanent)
        {
            initialWeaponM = Bullet;
            initialFirerateM = FireRate;
            initialMOfsetX = Xposition;
            initialMOfsetY = Yposition;
            initialFirerateM = FireRate;
            initialDamageM = Bullet.GetComponent<bulletController2>().damage;
        }
    }

    //Cambiar el arma secundaria entera
    public void ChangeSecondaryWeapon(GameObject Bullet, float FireRate, float Xposition, float Yposition, bool Permanent)
    {
        KSC.secondaryBullet = Bullet;
        KSC.secondaryFireRate = FireRate;
        KSC.XpositionSecondaryBullet = Xposition;
        KSC.YpositionSecondaryBullet = Yposition;
        if (Permanent)
        {
            initialWeaponS = Bullet;
            initialSOfsetX = Xposition;
            initialSOfsetY = Yposition;
            initialFirerateS = FireRate;
        }
        initialDamageS = Bullet.GetComponent<bulletController2>().damage;
    }

    //Cambiar el arma especial entera
    public void ChangeSpeacialWeapon(GameObject Bullet, int Ammo, int maxAmmo, float Xposition, float Yposition, bool Permanent)
    {
        KSC.specialBullet = Bullet;
        KSC.specialAmmunition = Ammo;
        KSC.XpositionSpecialBullet = Xposition;
        KSC.YpositionSpecialBullet = Yposition;
        KSC.maxSpecialAmmunition = maxAmmo;

        if (Permanent)
        {
            prevAmmo = Ammo;
            prevMaxAmmo = maxAmmo;
            initialSPOfsetX = Xposition;
            initialSPOfsetY = Yposition;
            initialDamageSp = Bullet.GetComponent<bulletController2>().damage;
        }
    }

    //para solo cambiar el FireRate llamar a la funcion solo con un float
    public void ChangeMainWeapon(float FireRate)
    {
        KSC.mainFireRate = FireRate;
    }
    //para solo cambiar el FireRate llamar a la funcion solo con un float
    public void ChangeSecondaryWeapon(float FireRate)
    {
        KSC.secondaryFireRate = FireRate;
    }

    //para buffos temporales, (como aumento cadencia durante un periodo de tiempo especifico) necesitamos volver a nuestro a un estado anterior en un determiando tiempo)
    public void ChangeMainWeaponTemp(float Time, float Firerate)
    {
        StartCoroutine(CMWD(Time, initialFirerateM));
        ChangeMainWeapon(Firerate);
    }

    //cambiar arma principal entera solo durante un periodo Time
    public void ChangeMainWeaponTemp(float Time, GameObject Bullet, float FireRate, float Xposition, float Yposition)
    {
        GameObject OldBullet = initialWeaponM;
        StartCoroutine(CMWD(Time, OldBullet, initialFirerateM, KSC.XpositionMainBullet, KSC.YpositionMainBullet));
        ChangeMainWeapon(Bullet, FireRate, Xposition, Yposition, false);
    }

    //para buffos temporales, (como aumento cadencia durante un periodo de tiempo especifico) necesitamos volver a nuestro a un estado anterior en un determiando tiempo)
    public void ChangeSecondaryWeaponTemp(float Time, float Firerate)
    {
        StartCoroutine(CSWD(Time, initialFirerateS));
        ChangeSecondaryWeapon(Firerate);
    }

    //cambiar arma secundaria entera solo durante un periodo Time
    public void ChangeSecondaryWeaponTemp(float Time, GameObject Bullet, float FireRate, float Xposition, float Yposition)
    {
        GameObject OldBullet = initialWeaponS;
        StartCoroutine(CSWD(Time, OldBullet, initialFirerateS, KSC.XpositionSecondaryBullet, KSC.YpositionSecondaryBullet));
        ChangeSecondaryWeapon(Bullet, FireRate, Xposition, Yposition, false);
    }
    //-------------------
    //cambiar arma especial entera solo durante un periodo Time PRRRRRRROOOOOOOOBAAAAAAANDDDDOOOOOOOO
    public void ChangeSpecialWeaponTemp(float Time, GameObject Bullet, int Ammo, int maxAmmo, float Xposition, float Yposition)
    {
        GameObject OldBullet = initialWeaponSp;
        StartCoroutine(CSWT(Time, OldBullet, prevAmmo, prevMaxAmmo, KSC.XpositionSecondaryBullet, KSC.YpositionSecondaryBullet));
        ChangeSpeacialWeapon(Bullet, Ammo, maxAmmo, 10.5f, Yposition, false);
    }
    //PRUEBAS DAVID, riesgo de explosion
    IEnumerator CSWT(float timeDelayed, GameObject Bullet, int Ammo, int maxAmmo, float Xposition, float Yposition)
    {
        yield return new WaitForSeconds(timeDelayed);
        ChangeSpeacialWeapon(Bullet, Ammo, maxAmmo, Xposition, Yposition, false);
    }
    //-----------------
    IEnumerator CSWD(float timeDelayed, float Firerate)
    {
        yield return new WaitForSeconds(timeDelayed);
        ChangeSecondaryWeapon(Firerate);
    }

    IEnumerator CMWD(float timeDelayed, float Firerate)
    {
        yield return new WaitForSeconds(timeDelayed);
        ChangeMainWeapon(Firerate);
    }

    IEnumerator CMWD(float timeDelayed, GameObject Bullet, float FireRate, float Xposition, float Yposition)
    {
        yield return new WaitForSeconds(timeDelayed);
        ChangeMainWeapon(Bullet, FireRate, Xposition, Yposition, false);
    }

    IEnumerator CSWD(float timeDelayed, GameObject Bullet, float FireRate, float Xposition, float Yposition)
    {
        yield return new WaitForSeconds(timeDelayed);
        ChangeSecondaryWeapon(Bullet, FireRate, Xposition, Yposition, false);
    }

    //para recargar (sumar) la municion especial del arma especial llamar solo con un int
    public void ChangeSpeacialWeapon(int Ammo)
    {
        int AMMO = KSC.specialAmmunition + Ammo;
        if (KSC.maxSpecialAmmunition >= AMMO)
        {
            KSC.specialAmmunition = AMMO;
        }
        else
        {
            KSC.specialAmmunition = KSC.maxSpecialAmmunition;
        }

    }

    //para recargar la municion especial del arma especial y cambiar su valor maximo llamar con dos int
    public void ChangeSpeacialWeapon(int Ammo, int MaxAmmo)
    {


        if (KSC.maxSpecialAmmunition <= MaxAmmo)
        {
            KSC.maxSpecialAmmunition = MaxAmmo;
        }

        int AMMO = KSC.specialAmmunition + Ammo;
        if (KSC.maxSpecialAmmunition <= AMMO)
        {
            KSC.specialAmmunition = AMMO;
        }
        else
        {
            KSC.specialAmmunition = KSC.maxSpecialAmmunition;
        }
    }


    public void ChangeMainDamage(float damage)
    {
        KSC.mainBullet.GetComponent<bulletController2>().damage = damage;
    }

    public void ChangeSecondaryDamage(float damage)
    {
        KSC.secondaryBullet.GetComponent<bulletController2>().damage = damage;
    }

    public void ChangeSpecialDamage(float damage)
    {
        KSC.specialBullet.GetComponent<bulletController2>().damage = damage;
    }

    public void ChangeMainDamageTemp(float Time, float damage)
    {
        StartCoroutine(CDMW(Time, initialDamageM));
        ChangeMainDamage(damage);
    }

    public void ChangeSecondaryDamageTemp(float Time, float damage)
    {
        StartCoroutine(CDSW(Time, initialDamageS));
        ChangeSecondaryDamage(damage);
    }

    public void ChangeSpecialDamageTemp(float Time, float damage)
    {
        StartCoroutine(CDSPW(Time, initialDamageSp));
        ChangeSpecialDamage(damage);
    }

    IEnumerator CDMW(float timeDelayed, float damage)
    {
        yield return new WaitForSeconds(timeDelayed);
        ChangeMainDamage(damage);
    }

    IEnumerator CDSW(float timeDelayed, float damage)
    {
        yield return new WaitForSeconds(timeDelayed);
        ChangeSecondaryDamage(damage);
    }

    IEnumerator CDSPW(float timeDelayed, float damage)
    {
        yield return new WaitForSeconds(timeDelayed);
        ChangeSpecialDamage(damage);
    }
}
