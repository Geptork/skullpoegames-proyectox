﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class KruxonShotControl : MonoBehaviour {

    //----------------------------------------------------------------------------
    //public vars
    [Header("Estado juego")]
    public Estadisticas estadisticas;

    [Header("Controlls")]
    private string[] _Controlls = new string[] { "mouse 0", "mouse 1" , "[5]", "[4]", "[6]", "joystick button 2", "joystick button 0", "joystick button 3", "joystick button 1", "f" , "joystick button 4", "joystick button 5" };
    public enum Controlls {None, XboxOneController, MouseAndKeyboard};
    public Controlls InputControlls;
    private string Fire1;
    private string Fire1Opcional;
    private string Fire2;
    private string Fire2Opcional;
    private string Fire3;
    private string Fire3Opcional;

    //MAIN SHOT
    [Header("Main Shot")]
    public GameObject mainBullet; //bullet prefab
    public float mainFireRate = 0.2f; 
    public float XpositionMainBullet; //X position respect the player or enemy
    public float YpositionMainBullet; //Y position respect the player or enemy

    public AudioClip AudioMain;
    //SECONDARY SHOT
    [Header("Secondary Shot")]
    public GameObject secondaryBullet; //bullet prefab
    public float secondaryFireRate = 0.2f;
    public float XpositionSecondaryBullet; //X position respect the player or enemy
    public float YpositionSecondaryBullet; //Y position respect the player or enemy

    public AudioClip AudioSecon;
    //SPECIAL SHOT
    [Header("Special Shot")]
    public GameObject specialBullet; //bullet prefab
    public float XpositionSpecialBullet; //X position respect the player or enemy
    public float YpositionSpecialBullet; //Y position respect the player or enemy
    public int specialAmmunition;
    public int maxSpecialAmmunition = 3;

    public AudioClip AudioSpecial;

    //private vars
    //MAIN SHOT
    bool enableMainCoroutine = true; //var to control coroutine
    bool enableMainFire = true; //var to control the bullet instantiation 
    Vector3 mainShotPoint;
    //SECONDARY SHOT
    bool enableSecondaryCoroutine = true; //var to control coroutine
    bool enableSecondaryFire = true; //var to control the bullet instantiation 
    Vector3 secondaryShotPoint;
    //SPECIAL SHOT
    Vector3 specialShotPoint;
    //----------------------------------------------------------------------------
    //Audio
    private AudioSource Audio;

    // Use this for initialization
    void Start ()
    {
        Audio = GetComponent<AudioSource>();

        int a, b, c, d, e, f;
        a = b = c = d = e = f = -1;

        if (specialAmmunition > maxSpecialAmmunition) specialAmmunition = maxSpecialAmmunition;
        //Declare the shotPoints
        mainShotPoint = new Vector3(transform.position.x + XpositionMainBullet, transform.position.y + YpositionMainBullet, transform.position.z);
        secondaryShotPoint = new Vector3(transform.position.x + XpositionSecondaryBullet, transform.position.y + YpositionSecondaryBullet, transform.position.z);
        specialShotPoint = new Vector3(transform.position.x + XpositionSpecialBullet, transform.position.y + YpositionSpecialBullet, transform.position.z);

        switch (InputControlls)
        {
            case Controlls.MouseAndKeyboard:
                a = 0;
                b = 2;
                c = 1;
                d = 3;
                e = 4;
                f = 9;
                break;
            case Controlls.XboxOneController:
                a = 5;
                b = 6;
                c = 8;
                d = 10;
                e = 7;
                f = 11;
                break;

            default:
                a = 0;
                b = 2;
                c = 1;
                d = 3;
                e = 4;
                f = 9;
            break;
        }

        Fire1 = _Controlls[a];
        Fire1Opcional = _Controlls[b];
        Fire2 = _Controlls[c];
        Fire2Opcional = _Controlls[d];
        Fire3 = _Controlls[e];
        Fire3Opcional = _Controlls[f];

        if (estadisticas == null)
        {
            estadisticas = GameObject.Find("EstadoJuego").GetComponent<Estadisticas>();
        }
    }
	
	//UPDATE--------------------------------------------------------------------
	void Update () {
        
        //MAIN SHOT-------------------------------------------------------------
        //if press button to shot and enableFire, instantiate the bullet
        if ((Input.GetKey(Fire1) || Input.GetKey(Fire1Opcional)) && enableMainFire && !Input.GetKey(Fire2) && !Input.GetKey(Fire2Opcional))
        {
            estadisticas.Shoots++;
            enableMainFire = false;
            enableMainCoroutine = true;
            //Declare the shotPoint
            mainShotPoint = new Vector3(transform.position.x + XpositionMainBullet, transform.position.y + YpositionMainBullet, transform.position.z);
            GameObject MainShoot = (GameObject)Instantiate(mainBullet, mainShotPoint, transform.rotation);
            if (!mainBullet.GetComponent<bulletBehaviour>().SeparateOfShooter)
            {
                MainShoot.transform.parent = GameObject.Find("Player").transform ;
            }
            Audio.PlayOneShot(AudioMain);
        }

        //if not, and you press button to shot and enableCoroutine -> Start Courotine
        else if ((Input.GetKey(Fire1) || Input.GetKey(Fire1Opcional)) && enableMainCoroutine && !Input.GetKey(Fire2) && !Input.GetKey(Fire2Opcional))
        {
            StartCoroutine(waitToEnableMainFire());
            enableMainCoroutine = false;
        }
        //----------------------------------------------------------------------

        //SECONDARY SHOT--------------------------------------------------------
        //if press button to shot and enableFire, instantiate the bullet
        if ((Input.GetKey(Fire2) || Input.GetKey(Fire2Opcional)) && enableSecondaryFire && !Input.GetKey(Fire1) && !Input.GetKey(Fire1Opcional))
        {
            estadisticas.Shoots++;
            enableSecondaryFire = false;
            enableSecondaryCoroutine = true;
            //Declare the shotPoint
            secondaryShotPoint = new Vector3(transform.position.x + XpositionSecondaryBullet, transform.position.y + YpositionSecondaryBullet, transform.position.z);
            GameObject secondaryShoot = (GameObject)Instantiate(secondaryBullet, secondaryShotPoint, transform.rotation);
            if (!secondaryBullet.GetComponent<bulletBehaviour>().SeparateOfShooter)
            {
                secondaryShoot.transform.parent = GameObject.Find("Player").transform;
            }
            Audio.PlayOneShot(AudioSecon);

        }

        //if not, and you press button to shot and enableCoroutine -> Start Courotine
        else if ((Input.GetKey(Fire2) || Input.GetKey(Fire2Opcional)) && enableSecondaryCoroutine && !Input.GetKey(Fire1) && !Input.GetKey(Fire1Opcional))
        {
            StartCoroutine(waitToEnableSecondaryFire());
            enableSecondaryCoroutine = false;
        }
        //----------------------------------------------------------------------

        //SPECIAL SHOT----------------------------------------------------------
        if ((Input.GetKeyDown(Fire3) || Input.GetKey(Fire3Opcional)) && specialAmmunition > 0)
        {
            estadisticas.Shoots++;
            specialAmmunition--;
            specialShotPoint = new Vector3(transform.position.x + XpositionSpecialBullet, transform.position.y + YpositionSpecialBullet, transform.position.z);
            GameObject specialShoot = (GameObject)Instantiate(specialBullet, specialShotPoint, transform.rotation);
            if (!secondaryBullet.GetComponent<bulletBehaviour>().SeparateOfShooter)
            {
                specialShoot.transform.parent = GameObject.Find("Player").transform;
            }
            Audio.PlayOneShot(AudioSpecial);

        }
        //----------------------------------------------------------------------
    }
    //END UPDATE----------------------------------------------------------------

    //Couroutine to wait the next main bullet-----------------------------------
    IEnumerator waitToEnableMainFire()
    {
        yield return new WaitForSeconds(mainFireRate);
        enableMainFire = true;
    }
    //--------------------------------------------------------------------------
    //Couroutine to wait the next secondary bullet------------------------------
    IEnumerator waitToEnableSecondaryFire()
    {
        yield return new WaitForSeconds(secondaryFireRate);
        enableSecondaryFire = true;
    }
    //--------------------------------------------------------------------------
    //Increase special ammunition (To call from other Script)-------------------
    public void increaseSpecialAmmunition(int ammunitionToIncrease)
    {
        if(maxSpecialAmmunition >= (ammunitionToIncrease + specialAmmunition))
        {
            specialAmmunition += ammunitionToIncrease;
        }else
        {
            specialAmmunition = maxSpecialAmmunition;
        }
    }
    //---------------------------------------------------------------------------
}
