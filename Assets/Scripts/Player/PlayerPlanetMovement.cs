﻿using UnityEngine;
using System.Collections;

public class PlayerPlanetMovement : MonoBehaviour
{
    public float maxSpeed = 5f;
    bool facingRight = true;
    Rigidbody2D r2D;
    Animator anim;

    bool grounded = false;  
    public Transform groundCheck; 
    float groundRadius = 0.2f; 
    public LayerMask whatIsGroud; //con que va a colisionar, desactivar Player para q no colisione consigo mismo

    public float jumpForce;

    bool alive = true;
    void Start ()
    {
        r2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        anim.SetBool("Alive", true);
    }

    void Update()
    {
        //muerto
        if (alive == false)
        {
            return;
        }
        //---------------------------------Para el video---------------
        if(Input.GetKeyDown("l"))
        {
            anim.SetBool("Suicide", true);
            alive = false;
        }
        //---------------------------------------------------------------
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGroud); 
        anim.SetBool("Ground", grounded);

        anim.SetFloat("vSpeed", r2D.velocity.y); //velocidad mientras subo o bajo
        if (grounded && Input.GetKeyDown (KeyCode.Space))
        {
            anim.SetBool("Ground", false);
            r2D.AddForce(new Vector2(0, jumpForce));
        }
        if(/*PLayerLife &&*/Input.GetKeyDown("k"))
        {
            anim.SetBool("Alive", false);
            alive = false;
        }

        if (!grounded) return; //no puede controlar una vez saltas

        float move = Input.GetAxis("Horizontal");
        r2D.velocity = new Vector2(move * maxSpeed, r2D.velocity.y);
        anim.SetFloat("Speed", Mathf.Abs(move));
        //girar el sprite depende de hacia donde va el player
       if (move > 0 && !facingRight)
        {
            Flip();
        }
        else if (move < 0 && facingRight)
        {
            Flip();
        }
       //disparo primario (palante)
        if (Input.GetMouseButtonDown(0))
        {
            anim.SetBool("Shoot", true);
        }
        else
        {
            anim.SetBool("Shoot",false);
        }
        //disparo Secundario (arriba)
        if (Input.GetMouseButtonDown(1))
        {
            anim.SetBool("ShootUp", true);
        }
        else
        {
            anim.SetBool("ShootUp", false);
        }
    }

    void Flip() //flip x, para q el pj mire hacia donde se mueve
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}