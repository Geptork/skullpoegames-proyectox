﻿using UnityEngine;
using System.Collections;
using System;

public struct Boundaries {
	public float xMin, xMax, yMin, yMax;
}

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{

    protected Boundaries boundaries;

    [SerializeField]
    public static Rigidbody2D rigid;

    [SerializeField]
    public float speed;

    private Vector2 movementInput;
    private Vector2 _velocity;
    private SpriteAnimator SpAnim;
    private bool Died = false;
    void Awake()
    {
        Vector2 maxCameraBounds = Camera.main.ViewportToWorldPoint(new Vector3(1, 1));
        Vector2 minCameraBounds = Camera.main.ViewportToWorldPoint(new Vector3(0, 0));

        boundaries.xMax = maxCameraBounds.x - 0.75f;
        boundaries.yMax = maxCameraBounds.y - 0.75f;

        boundaries.xMin = minCameraBounds.x + 0.75f;
        boundaries.yMin = minCameraBounds.y + 0.75f;

        if (rigid == null) rigid = GetComponent<Rigidbody2D>();

        SpAnim = gameObject.GetComponent<SpriteAnimator>();
    }

    void FixedUpdate()
    {
        if(Died == true)
        {
            rigid.velocity = new Vector2 (0,0);
            return;
        }
        movementInput.x = Input.GetAxis("Horizontal");
        movementInput.y = Input.GetAxis("Vertical");

        Vector3 movement = (Vector2)movementInput;
        _velocity = movement * speed;
        rigid.velocity = _velocity;
        rigid.position = new Vector3(
                Mathf.Clamp(rigid.position.x, boundaries.xMin, boundaries.xMax),
                Mathf.Clamp(rigid.position.y, boundaries.yMin, boundaries.yMax),
                0.0f);

        //Animaciones Nave
        //Bajar - Acelerar mientras baja - Frenar mientras baja
        if (Input.GetKey("s") && !Input.GetKey("a") && !Input.GetKey("d"))
        {
            SpAnim.Play("Down", true, 0);
        }
        if (Input.GetKey("s") && Input.GetKeyDown("a"))
        {
            SpAnim.ForcePlay("DownFrena", false, 0);
        }
        if (Input.GetKey("s") && Input.GetKeyDown("d"))
        {
            SpAnim.ForcePlay("DownAcelera", false, 0);
        }
        //Subir
        if (Input.GetKey("w") && !Input.GetKey("a") && !Input.GetKey("d"))
        {
            SpAnim.ForcePlay("Up", true, 0);
        }
        if (Input.GetKey("w") && Input.GetKeyDown("a"))
        {
            SpAnim.ForcePlay("UpFrena", false, 0);
        }
        if (Input.GetKey("w") && Input.GetKeyDown("d"))
        {
            SpAnim.ForcePlay("UpAcelera", false, 0);
        }
        //Acelerar
        if (Input.GetKey("d") && !Input.GetKey("s") && !Input.GetKey("w"))
        {
            SpAnim.Play("Acelera", false, 0);
        }
        if (Input.GetKey("d") && Input.GetKeyDown("w"))
        {
            SpAnim.ForcePlay("UpAcelera", false, 0);
        }
        if (Input.GetKey("d") && Input.GetKeyDown("s"))
        {
            SpAnim.ForcePlay("DownAcelera", false, 0);
        }
        //Frenar
        if (Input.GetKey("a") && !Input.GetKey("s") && !Input.GetKey("w"))
        {
            SpAnim.Play("Frena", false, 0);
        }
        if (Input.GetKey("a") && Input.GetKeyDown("w"))
        {
            SpAnim.ForcePlay("UpFrena", false, 0);
        }
        if (Input.GetKey("a") && Input.GetKeyDown("s"))
        {
            SpAnim.ForcePlay("DownFrena", false, 0);
        }
        //Idle
        if (!Input.GetKey("a") && !Input.GetKey ("d") && !Input.GetKey("s") && !Input.GetKey ("w"))
        {
            SpAnim.Play("Walk", true, 0);
        }
        //Died
        if (GetComponent<PlayerHealthController>().Dying == true && Died == false)
        {
            SpAnim.ForcePlay("Dead", false, 0);
            Died = true;
        }
    }
}
