﻿using UnityEngine;
using System.Collections;

public class OctopusModeBehaviour : MonoBehaviour {
    public GameObject Player;

    public GameObject Helper1;
    public float Helper1StartPositionX;
    public float Helper1StartPositionY;

    public GameObject Helper2;
    public float Helper2StartPositionX;
    public float Helper2StartPositionY;

    public float Xseparation = 1;
    public float Yseparation = 2;
    public float HelperVelocity = 1;
    public float posDelay = 0.05f;

    private Rigidbody2D R2DH1;
    private Rigidbody2D R2DH2;

    IEnumerator MoveHelper(bool Helper1, bool Helper2, float velocityX, float velocityY )
    {
        if (Helper1)
        {
            yield return new WaitForSeconds(posDelay);
            R2DH1.velocity = new Vector3(velocityX, velocityY);
        }

        if (Helper2)
        {
            yield return new WaitForSeconds(posDelay);
            R2DH2.velocity = new Vector3(velocityX, velocityY);
        }
    }

    IEnumerator MoveHelper(bool Helper1, bool Helper2, float velocityX)
    {
        if (Helper1)
        {
            yield return new WaitForSeconds(posDelay);
            R2DH1.velocity = new Vector3(velocityX, R2DH1.velocity.y);
        }

        if (Helper2)
        {
            yield return new WaitForSeconds(posDelay);
            R2DH2.velocity = new Vector3(velocityX, R2DH1.velocity.y);
        }
    }

    private void InstanciateHelpers()
    {
        Helper1 = (GameObject)Instantiate(Helper1, new Vector3(Helper1StartPositionX, Helper1StartPositionY), Quaternion.identity);
        Helper2 = (GameObject)Instantiate(Helper2, new Vector3(Helper2StartPositionX, Helper2StartPositionY), Quaternion.identity);
    }

    void StartOctopusMode () {
        InstanciateHelpers();
        R2DH1 = Helper1.GetComponent<Rigidbody2D>();
        R2DH2 = Helper2.GetComponent<Rigidbody2D>();
	}
	
    private void RecalculatePos(GameObject Helper, bool isH1, bool isH2)
    {
        if (Helper.transform.position.x > Player.transform.position.x + Xseparation)
        {
            StartCoroutine(MoveHelper(isH1, isH2, -HelperVelocity));
        }

        if (Helper.transform.position.x < Player.transform.position.x - Xseparation)
        {
            StartCoroutine(MoveHelper(isH1, isH2, HelperVelocity));
        }
        
        if (Helper.transform.position.x < Player.transform.position.x + Xseparation && Helper.transform.position.x > Player.transform.position.x - Xseparation )
        {
            StartCoroutine(MoveHelper(isH1, isH2, 0));
        }

        float YH2Offset = -Yseparation/2;

        if (isH2)
        {
            YH2Offset = Yseparation/2;
        }

        if (Helper.transform.position.y + YH2Offset> Player.transform.position.y + Yseparation)
        {
            if (isH1)
            {
                StartCoroutine(MoveHelper(isH1, isH2, R2DH1.velocity.x, -HelperVelocity));
            }
            if (isH2)
            {
                StartCoroutine(MoveHelper(isH1, isH2, R2DH2.velocity.x, -HelperVelocity));
            }
        }

        if (Helper.transform.position.y + YH2Offset < Player.transform.position.y - Yseparation)
        {
            if (isH1)
            {
                StartCoroutine(MoveHelper(isH1, isH2, R2DH1.velocity.x, +HelperVelocity));
            }
            if (isH2)
            {
                StartCoroutine(MoveHelper(isH1, isH2, R2DH2.velocity.x, +HelperVelocity));
            }
        }


    }

	// Update is called once per frame
	void Update () {
        if (Helper1 != null)
        {
            RecalculatePos(Helper1, true, false);
        }

        if (Helper2 != null)
        {
            RecalculatePos(Helper2, false, true);
        }

    }
}
