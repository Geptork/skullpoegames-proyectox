﻿using UnityEngine;
using System.Collections;
using System;


[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovementAndroid : MonoBehaviour
{
	

	[SerializeField]
	protected float speed = 0.01f;

	[SerializeField]
	protected float speedAcceleration = 10f;

	protected Boundaries boundaries;

	public bool movement = false;

	void Awake(){

		Vector2 maxCameraBounds = Camera.main.ViewportToWorldPoint(new Vector3(1,1));
		Vector2 minCameraBounds = Camera.main.ViewportToWorldPoint(new Vector3(0,0));

		boundaries.xMax = maxCameraBounds.x - 0.75f;
		boundaries.yMax = maxCameraBounds.y - 0.75f;

		boundaries.xMin = minCameraBounds.x + 0.75f;
		boundaries.yMin = minCameraBounds.y + 0.75f;

	}

	void FixedUpdate()
	{


		if(movement){
			
			if(Input.touchCount > 0 &&  Input.GetTouch(0).phase == TouchPhase.Moved){
				//Touch myTouch = Input.GetTouch(0);
				//Touch[] myTouches = Input.touches;
			//	for(int i=0; i< Input.touchCount; i++){
					Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
				transform.Translate(touchDeltaPosition.x * speed, touchDeltaPosition.y * speed, 0);

				transform.position = new Vector3(
					Mathf.Clamp(transform.position.x,boundaries.xMin, boundaries.xMax),
					Mathf.Clamp(transform.position.y, boundaries.yMin, boundaries.yMax),
					0.0f
					);
					
			//	}
			}
		}
		else{
			Vector3 dir = Vector3.zero;
			dir.x = Input.acceleration.x;
			//dir.y = (Input.acceleration.y + 0.6f)*2;
			dir.y = Mathf.Clamp(0.6f+Input.acceleration.y, -2f,2f);
			
			if(dir.sqrMagnitude > 1)
				dir.Normalize();
			
			
			//dir.y = Mathf.Clamp(dir.y,0,3);
			dir *=Time.deltaTime;



			transform.Translate(dir*speedAcceleration);

			transform.position = new Vector3(
				Mathf.Clamp(transform.position.x,boundaries.xMin, boundaries.xMax),
				Mathf.Clamp(transform.position.y, boundaries.yMin, boundaries.yMax),
				0.0f
				);

		}



	}



}
