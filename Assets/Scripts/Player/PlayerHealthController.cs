﻿using UnityEngine;
using System.Collections;

public class PlayerHealthController : MonoBehaviour
{

    [Header("Estado juego")]
    public Estadisticas estadisticas;
    //private vars
    private float CurrentHealth;
    private float CurrentShield;
    SpaceHUD HUD;
    //public vars
    public bool Dying = false;
    public bool InYisusMode = false;
    public GameObject Shield;
    public Light ShieldLight;
    public float InitHealth = 100.0f;
    public float InitShield = 100.0f;
    public float MaxHealth = 100.0f;
    public float MaxShield = 100.0f;
    private int CurrParpa = 0;
    private int MaxParpa = 7;
    private float waitbparpa = 0.15f;
    private SpriteRenderer SP;

    //0 - 1 % shield absorbed damage (1 -> full protected)
    public float ShieldAbsorb = 0.5f;

    //function to check incoming parameters
    private bool Check(float ValueToCheck, float Max)
    {
        bool testresult = false;
        if (ValueToCheck >= 0.0f && ValueToCheck <= Max)
        {
            testresult = true;
        }
        return testresult;
    }

    //set init values
    void Start()
    {
        SP = GetComponent<SpriteRenderer>();
        HUD = GameObject.Find("GameplayPrueba").GetComponent<SpaceHUD>();
        CurrentHealth = InitHealth;
        CurrentShield = InitShield;
        if (!Check(CurrentHealth, MaxHealth))
        {
            Debug.LogError("WRONG HEALTH VALUE");
        }
        if (!Check(CurrentShield, MaxShield))
        {
            Debug.LogError("WRONG SHIELD VALUE");
        }
        if (!Check(ShieldAbsorb, 1.0F))
        {
            Debug.LogError("WRONG SHIELD ABSORB VALUE % (0-1)");
        }

        UpdateHUD();
        UpdateShield();
    }

    //public functions to get shield/health parameters
    public float GetShield()
    {
        return CurrentShield;
    }

    //public functions to get shield/health parameters
    public float GetHealth()
    {
        return CurrentHealth;
    }

    //public subprograms to set shield/health parameters
    public void SetShield(float ShieldToSet)
    {
        //testing before set
        if (Check(ShieldToSet, MaxShield))
        {
            //seting value
            CurrentShield = ShieldToSet;
        }
        else
        {
            Debug.LogError("WRONG SHIELD VALUE");
        }
        UpdateHUD();
    }

    //public subprograms to set shield/health parameters
    public void SetHealth(float HealthToSet)
    {
        //testing before set
        if (Check(HealthToSet, MaxHealth))
        {
            //seting value
            CurrentHealth = HealthToSet;
        }
        else
        {
            Debug.LogError("WRONG HEALTH VALUE");
        }
        UpdateHUD();
    }

    //subprogram to damage player shield
    private void TakeShield(float damage)
    {
        //Recalculate damaged absorved by our shield
        float DamageRecalculated = damage * (1 - ShieldAbsorb);
        if (DamageRecalculated > CurrentShield)
        {
            CurrentShield = 0.0f;
        }
        else
        {
            CurrentShield -= DamageRecalculated;
        }
    }

    //subprogram to damage player health
    private void TakeHealth(float damage)
    {
        if (damage >= CurrentHealth)
        {
            CurrentHealth = 0.0f;
        }
        else
        {
            CurrentHealth -= damage;
        }
    }
    private void UpdateShield()
    {
        Shield.SetActive(true);

        ShieldLight.enabled = true;
        Invoke("LightDisabled", 0.2f);
        float shieldpercent = (CurrentShield / MaxShield);
        ShieldLight.color = new Color(1.0f, shieldpercent, shieldpercent);
        //Shield.GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
        Shield.GetComponent<Renderer>().material.color = new Color(1.0f, shieldpercent, shieldpercent);
        if (CurrentShield == 0.0)
        {
            Shield.SetActive(false);
            Shield.GetComponent<Renderer>().material.color = new Color(0.0f, 0.0f, 0.0f);
        }
    }

    private void LightDisabled()
    {
        ShieldLight.enabled = false;
        CancelInvoke();
    }
    //public subprogram to damage  player (use this from your external script)
    public void DoDamage(float IncomingDamage)
    {
        if (InYisusMode)
        {
            IncomingDamage = 0f;
        }
        else
        {
            if (CurrentShield == 0)
            {
                StartCoroutine(Parpadeo(waitbparpa));
            }
        }

        if (estadisticas != null)
        {
            estadisticas.DamageReceived += IncomingDamage;
        }
        else
        {
            estadisticas = GameObject.FindGameObjectWithTag("EstadoJuego").GetComponent<Estadisticas>();
            if(estadisticas == null)
            {
                estadisticas = GameObject.Find("EstadoJuego").GetComponent<Estadisticas>();
            }
            estadisticas.DamageReceived += IncomingDamage;
        }

        ShieldLight.intensity = IncomingDamage * 0.09f;
        //if player have shield damaged it
        if (CurrentShield > 0.0)
        {
            TakeShield(IncomingDamage);
        }
        else
        {
            TakeHealth(IncomingDamage);
        }
        //PRUEBAS: Si el daño recibido es mayor que el escudo, el daño que no absorve el escudo se lo lleva la Health
        if (CurrentShield < IncomingDamage)
        {
            TakeHealth(IncomingDamage - CurrentShield);
            print("Overkill");
        }
        //--
        UpdateHUD();
        UpdateShield();

        //Check new values
        if (!Check(CurrentHealth, MaxHealth))
        {
            Debug.LogError("WRONG HEALTH VALUE");
        }
        if (!Check(CurrentShield, MaxShield))
        {
            Debug.LogError("WRONG SHIELD VALUE");
        }

        if (CurrentHealth <= 0.0f && !Dying)
        {
            Dying = true;
            Kill();
        }
    }

    /*
    Up and Down -grade subprograms:
    max value = old value +- incoming parameter from external script

    Example
    MaxHealth = 100.0
    CurrentHealth = 10.0
    PlayerHealthControl.UpgradeHealth(20.0);
    MaxHealth = 120.0
    CurrentHealth = 120.0
   */
    public void UpgradeHealth(float MoreHealth)
    {
        MaxHealth += MoreHealth;
        CurrentHealth = MaxHealth;
        UpdateHUD();
    }

    public void DowngradeHealth(float LessHeatlh)
    {
        MaxHealth -= LessHeatlh;
        CurrentHealth = MaxHealth;
        UpdateHUD();
    }

    public void UpgradeShield(float MoreShield, float NewAbsorbPercent)
    {
        MaxShield += MoreShield;
        CurrentShield = MaxShield;
        if (Check(NewAbsorbPercent, 1.0f))
        {
            ShieldAbsorb = NewAbsorbPercent;
            //Display new shield here
        }
        else
        {
            Debug.LogError("WRONG SHIELD ABSORB VALUE % (0-1)");
        }
        UpdateHUD();
        UpdateShield();
    }

    public void DowngradeShield(float LessShield, float NewAbsorbPercent)
    {
        MaxShield -= LessShield;
        CurrentHealth = MaxShield;
        if (Check(NewAbsorbPercent, 1.0f))
        {
            ShieldAbsorb = NewAbsorbPercent;
            //Display new shield here
        }
        else
        {
            Debug.LogError("WRONG SHIELD ABSORB VALUE % (0-1)");
        }
        UpdateHUD();
    }
    private void UpdateHUD()
    {
        HUD.UpdateHUD(CurrentHealth, CurrentShield, MaxHealth, MaxShield);
    }

    private void Kill()
    {
        if (!InYisusMode)
        {
            StopAllCoroutines();
            Destroy(gameObject, 2.0f);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Enemy" || collider.gameObject.tag == "Armored")
        {
            if (!InYisusMode)
            {
                CurrentHealth = 0.0f;
                CurrentShield = 0.0f;
            }
            UpdateHUD();
            if (!Dying)
            {
                Kill();
                if (!InYisusMode)
                {
                    Dying = true;
                }
            }
            if (!collider.GetComponent<EnemyHealthControl>().isABoss)
            {
                //collider.GetComponent<EnemyHealthControl>().DoDamage(100 * collider.GetComponent<EnemyHealthControl>().GetHealth());
                collider.GetComponent<EnemyHealthControl>().SetHealth(0.0f);
            }
        }
    }

    IEnumerator Parpadeo(float wait)
    {
        InYisusMode = true;
        CurrParpa++;
        yield return new WaitForSeconds(wait);

        if (SP.enabled)
        {
            SP.enabled = false;
        }else
        {
            SP.enabled = true;
        }

        if(MaxParpa == CurrParpa)
        {
            SP.enabled = true;
            InYisusMode = false;
            CurrParpa = 0;
            StopAllCoroutines();
        }
        else
        {
            StartCoroutine(Parpadeo(wait));
        }
    }
}