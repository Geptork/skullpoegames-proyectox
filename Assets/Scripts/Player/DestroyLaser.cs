﻿using UnityEngine;
using System.Collections;

public class DestroyLaser : MonoBehaviour {
    public float Time;
	// Use this for initialization
	void Start () {
        Destroy(gameObject, Time);
	}
	
}
