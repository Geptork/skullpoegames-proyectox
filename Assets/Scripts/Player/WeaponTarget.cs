﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class WeaponTarget : MonoBehaviour {

    public Instanciador Instan;

    public enum EnemyTipe { None, Bultox, Astorfix, BigBultox, Nepto, CinematicBultox, CinematicKruxon, Pile, Osisteo};
    public enum EnemyIA { None, Smart, Fool, BigBultox };
    [Header("Comunes")]
    public float FireRate;
    private bool DONTSHOOT = false;
    public bool ShootOnStart = false;
    public EnemyTipe _BulletType;
    public EnemyIA _IA;
    public float DamagePerHit;
    [Header("Smart")]
    public float escaladovelocidad;
    public float dispersion;
    [Header("Fool")]
    public float xoffset;
    public float yoffset;
    private string Name;




    string   SelectBullet()
    {
        string PrefabPath = "";
        switch (_BulletType)
        {
            case EnemyTipe.None:

                PrefabPath = "Prefabs/Ammo/Bultox_Bullet";
                Name = "Bultox_Bullet";
                break;
            case EnemyTipe.CinematicBultox:
                PrefabPath = "Prefabs/Ammo/Bultox_BulletCinema";
                Name = "Bultox_BulleCinema";
                break;
            case EnemyTipe.CinematicKruxon:
                PrefabPath = "Prefabs/Ammo/Bullet_0Cinema";
                Name = "Bullet_0Cinema";
                break;
            case EnemyTipe.Bultox:

                PrefabPath = "Prefabs/Ammo/Bultox_BulletAlt";
                Name = "Bultox_BulletAlt";
                break;
            case EnemyTipe.Astorfix:

                PrefabPath = "Prefabs/Ammo/Axtorfis_Bullet";
                Name = "Axtorfis_Bullet";
                break;
            case EnemyTipe.BigBultox:

                PrefabPath = "Prefabs/Ammo/Bultox_BulletBIG"; //NO
                Name = "Bultox_BulletBIG";
                break;
            case EnemyTipe.Nepto:

                PrefabPath = "Prefabs/Ammo/BulletN";
                Name = "BulletN";
                break;
            case EnemyTipe.Osisteo:

                PrefabPath = "Prefabs/Ammo/OsisteoProyectil";
                Name = "OsisteoProyectil";
                break;
        }
        return PrefabPath;
    }


	// Use this for initialization
	public void ShootTarget () {
        if (_BulletType != EnemyTipe.Pile)
        {
            string PrefabPath = SelectBullet();
            GameObject Target = GameObject.Find("Player");
            //gameObject.name = "Weapon"; OJOCUIDAO
            Vector3 TargetPosition = Target.transform.localPosition;
            Vector3 ShootPoints = this.gameObject.transform.FindChild("ShootPoint").transform.position;
            Vector3 MyPosition = this.gameObject.transform.root.transform.localPosition;
            float TargetX = float.Parse(TargetPosition.x.ToString());
            float TargetY = float.Parse(TargetPosition.y.ToString());
            float myweaponX = this.gameObject.transform.FindChild("ShootPoint").transform.position.x;
            float myweaponY = this.gameObject.transform.FindChild("ShootPoint").transform.position.y;
            float MyPositionX = float.Parse(MyPosition.x.ToString()) + myweaponX;
            float MyPositionY = float.Parse(MyPosition.y.ToString()) - myweaponY / 2;
            Vector3 PosShoot = new Vector3(MyPositionX, MyPositionY, 0.0f);
            GameObject newelement;
            newelement = Instan.GiveMeObject(Name);
            if (newelement == null)
            {
                newelement = (GameObject)Instantiate(Resources.Load(PrefabPath), this.gameObject.transform.FindChild("ShootPoint").transform.position, transform.rotation);
            }
            else
            {
                //print("hemos usado el instanciador para cargar el prefab:" + Name);
            }
            newelement.transform.position = this.gameObject.transform.FindChild("ShootPoint").transform.position;
            newelement.SetActive(true);
            newelement.name = name;
            Rigidbody2D rb;
            bulletController2 Bc;
            Bc = newelement.GetComponent<bulletController2>();
            Bc.damage = DamagePerHit;
            rb = newelement.GetComponent<Rigidbody2D>();
            if (rb == null)
            {
                rb = newelement.AddComponent<Rigidbody2D>();
            }
            if (rb.gravityScale != null)
            {
                rb.gravityScale = 0;
            }
            float velocityX = Mathf.Pow(Mathf.Abs(TargetX - myweaponX), 1);
            float velocityY = Mathf.Pow(Mathf.Abs(TargetY - myweaponY), 1);

            float MODULO = Mathf.Sqrt((velocityX * velocityX + velocityY * velocityY));

            if (TargetY < MyPositionY)
            {
                velocityY = -velocityY;
            }
            float dis = UnityEngine.Random.Range(-1.0f, 1.0f) * dispersion;
            rb.velocity = new Vector3(-(velocityX / MODULO) * escaladovelocidad, (velocityY / MODULO) * escaladovelocidad + dis, 0);
            if (TargetX > MyPositionX)
            {
                Destroy(newelement);
            }
            if (DONTSHOOT) { Destroy(newelement); }
        }
    }

    public void Shoot()
    {
        if (_BulletType != EnemyTipe.Pile)
        {
            string PrefabPath = SelectBullet();
            Vector3 PosShoot = new Vector3(transform.position.x + xoffset, transform.position.y + yoffset, 0.0f);
            GameObject newelement;
            newelement = Instan.GiveMeObject(Name);
            if (newelement == null)
            {
                newelement = (GameObject)Instantiate(Resources.Load(PrefabPath), PosShoot, transform.rotation);
            }
            else
            {
                //print("hemos usado el instanciador para cargar el prefab:" + Name);
            }
            newelement.transform.position = PosShoot;
            newelement.SetActive(true);
            newelement.name = name;
            Rigidbody2D rb;
            bulletController2 Bc;
            Bc = newelement.GetComponent<bulletController2>();
            Bc.damage = DamagePerHit;
            rb = newelement.GetComponent<Rigidbody2D>();
            if (rb == null)
            {
                rb = newelement.AddComponent<Rigidbody2D>();
            }
            if (rb.gravityScale != null)
            {
                rb.gravityScale = 0;
            }
        }

    }

    public void WakeUp() {


         switch (_IA)
                {
                    case EnemyIA.None:
                        InvokeRepeating("Shoot", 0, FireRate);
                        break;
                    case EnemyIA.Smart:
                        InvokeRepeating("ShootTarget", 0, FireRate);
                        break;
                    case EnemyIA.Fool:
                        InvokeRepeating("Shoot", 0, FireRate);
                        break;
                 }
}

    public void Sleep()
    {
        DONTSHOOT = true;
        CancelInvoke("ShootTarget");
        Destroy(gameObject);
    }

    void Start()
    {
        if (ShootOnStart)
        {
            WakeUp();
        }

        /*
            if (Instan == null)
            {
                Instan = GameObject.FindGameObjectWithTag("Instanciador").GetComponent<Instanciador>();
            }
        */
    }
}
