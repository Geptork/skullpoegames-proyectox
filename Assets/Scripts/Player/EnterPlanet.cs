﻿using UnityEngine;
using System.Collections;

public class EnterPlanet : MonoBehaviour {

	public GameObject target;
	public float speed;
	public float scaleSpeed;

	private bool _final;
	private bool _move;


	// Use this for initialization
	void Start () {
		_final = true;
		_move = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(_final){
			GetComponent<PlayerMovement>().enabled = false;
			GetComponent<KruxonShotControl>().enabled = false;
		
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			
			_move = true;
		}
			
		if(_move)
			Action();
	}

	public bool SetFinal{
		set{_final = value;}
	}

	private void Action(){
		transform.position = Vector3.MoveTowards(transform.position,target.transform.position, speed * Time.deltaTime );
		transform.localScale -= new Vector3(scaleSpeed,scaleSpeed,scaleSpeed); 
	}
}
