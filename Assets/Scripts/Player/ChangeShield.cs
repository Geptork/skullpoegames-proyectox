﻿using UnityEngine;
using System.Collections;

public class ChangeShield : MonoBehaviour
{
    public PlayerHealthController PHC;
    public GameObject PlayerShield;
    public GameObject Player;

    public void ChangeIt(GameObject Shield, Light ShieldLight, float absorb, float NewShieldHealth, GameObject Imprv)
    {
        //PHC.Shield = Shield;
        float ShieldHealthtoAdd = -PHC.GetShield() + NewShieldHealth;
        PHC.UpgradeShield(ShieldHealthtoAdd, absorb);
        Shield.SetActive(true);
        Shield.transform.position = PlayerShield.transform.position;
        Shield.transform.parent = PlayerShield.transform.parent;
        Destroy(PlayerShield);
        PlayerShield = Shield;
        PHC.Shield = Shield;
        PHC.ShieldLight = ShieldLight;
        PHC.DoDamage(0);
        Destroy(Imprv);
    }

    public void ChangeIt(float ShieldHealthtoAdd, float NewAbsorb)
    {
        PHC.UpgradeShield(ShieldHealthtoAdd, NewAbsorb);
    }

    public void ChangeIt(float ShieldHealthtoAdd)
    {
        float absorb = PHC.ShieldAbsorb;
        PHC.UpgradeShield(ShieldHealthtoAdd, absorb);
    }

}