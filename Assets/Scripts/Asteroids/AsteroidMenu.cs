﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AsteroidMenu : MonoBehaviour {

	public GameObject[] asteroids;
	private float timer = 0;
	private float TIME_LEFT = 1.5f; 

	private List<GameObject> asteroidsInGame;
	private Vector3[] _target;
	private Vector3 _position;
	private float[] _speed;


	// Use this for initialization
	void Start () {

		asteroidsInGame = new List<GameObject>();
		_target = new Vector3[10];
		_speed = new float[10];

		for(int i=0; i< _target.Length; i++){
			_target[i] = new Vector3 (-11.5f,Random.Range(-8f,8f),0);
			_speed[i] = Random.Range(1f,2f);
		}



	
	}

	// Update is called once per frame
	void Update () {

		timer -= Time.deltaTime;



		if(asteroidsInGame.Count < 8){
			asteroidsInGame.Add(Initialize());
		}

		if(timer <= 0){
			asteroidsInGame.First().GetComponent<AsteroidMenuMov>().SetMovible(_target[Random.Range(0,8)],_speed[Random.Range(0,8)]);
			asteroidsInGame.Remove(asteroidsInGame.First());
			timer = TIME_LEFT;
		}

	
	}

	GameObject Initialize(){
		GameObject go;
		_position = new Vector3 (Random.Range(14f,22f),Random.Range(-5f,5f),0);
		go = Instantiate(asteroids[Random.Range(0,4)],_position,Quaternion.identity) as GameObject;
		return go;
	}
}
