﻿using UnityEngine;
using System.Collections;

public class AsteroidMenuMov : MonoBehaviour {

	private bool movible;
	private bool matable;
	private Vector3 _target;
	private float _speed;
	private Vector3 _myPos;

	void Start(){
		movible = false;
		matable = false;
	}
	
	void Update(){
		if(movible)
			MoveF ();

		if(matable)
			DestructMe();
	}
	
	public void SetMovible(Vector3 t,float sp){
		movible = true;
		_target = t;
		_speed = sp;
	}
	
	private void MoveF(){
		transform.position = Vector3.MoveTowards(transform.position,_target, _speed  * Time.deltaTime );
		if(transform.position.x <= -10){
			movible = false;
			matable = true;
		}
	}

	private void DestructMe(){
		Destroy(this.gameObject);
	}
}
