using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


public class Asteroid : MonoBehaviour
{
    [SerializeField]
    private float damage;

    [SerializeField]
    private float strength;

    private SpriteAnimator _animator;

    private Collider2D _collider;
   

    float Strength
    {
        get
        {
            return strength;
        }
        set
        {
            strength = value;
            if(strength < 0)
            {
                Explode();
            }
        }
    }

    void Start()
    {
        _animator = GetComponent<SpriteAnimator>();
        _collider = GetComponent<Collider2D>();
    }

    void OnEnabled()
    {
        _collider.enabled = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && gameObject.tag == "Meteorite")
        {
            other.GetComponent<PlayerHealthController>().DoDamage(damage);
            gameObject.GetComponent<EnemyHealthControl>().DoDamage(100*gameObject.GetComponent<EnemyHealthControl>().GetHealth());
        }
    }

    void Explode()
    {
        _animator.Play("Explode");
        _collider.enabled = false;
    }
    
    //Changed: Dead now, it didn't overload the Destroy function so...
    
   /* public void Kill()
    {
        List<string> bolsadeItems = new List<string>();
        bolsadeItems.Add("ShieldRestorer");
        bolsadeItems.Add("PiedraMeteoro");

        gameObject.GetComponent<ItemDrop>().Drop(bolsadeItems);

        Destroy (gameObject);
    }*/
}
