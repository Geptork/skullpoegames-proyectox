﻿using UnityEngine;
using System.Collections;

public class Estadisticas : MonoBehaviour {

    public float TotalCollectablesinGame = 50f;
    public int Shoots = 0;
    public int Kills = 0;
    public int Score = 0;
    public float DamageReceived = 0f;
    public float DamageDone = 0f;
    public float CurrentCollectablesTaken= 0f;
    [Range(0,100)]
    public float CollectablesPercent = 0f;
}
