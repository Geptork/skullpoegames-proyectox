using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControladorNiebla: MonoBehaviour {
    [HideInInspector]
    public float currentFrame;
    [HideInInspector]
    public float frameOffset;
    [System.Serializable]
    public class Controller{
      public float Start;
      public float Stop;
        [HideInInspector]
      public bool Actived = false;
      [HideInInspector]
      public bool Deactivated = true;
    }

    [SerializeField]
    public GameObject Niebla;
    public List<Controller> NieblaEventos;


    void Start()
    {
        Niebla.GetComponent<ParticleSystem>().enableEmission = false;
    }

    private void ChangeNiebla(bool TurnOn)
    {
        if (!TurnOn)
        {
            Niebla.GetComponent<ParticleSystem>().enableEmission = false;
        }else
        {
            Niebla.GetComponent<ParticleSystem>().enableEmission = true;
        }


    }


    void Update()
    {
        currentFrame = Time.timeSinceLevelLoad - frameOffset;

        for (int ev = 0; ev < NieblaEventos.Count; ev++)
        {

            if (currentFrame >= NieblaEventos[ev].Start && !NieblaEventos[ev].Actived)
            {
                ChangeNiebla(true);
                NieblaEventos[ev].Actived = true;
                NieblaEventos[ev].Deactivated = false;
            }

            if (currentFrame >= NieblaEventos[ev].Stop)
            {
                if(!NieblaEventos[ev].Deactivated){
                    ChangeNiebla(false);
                    NieblaEventos[ev].Deactivated = true;
                    
              }
            }
        }

      }

  }
