using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CometasManager : MonoBehaviour
{
    public Instanciador Instan;
  public enum Enviorment {None, Cometas};
  public bool ComInstanciate = false;
  public float TimeBetween = 0.5f;
    [HideInInspector]
    public float RainHeight;
    [HideInInspector]
    public float MinGravity;
    [HideInInspector]
    public float MaxGravity;
    [HideInInspector]
    public float XStarRain;
    [HideInInspector]
    public float Speed;
    [HideInInspector]
    public float YSpeed;
    [HideInInspector]
    private string path;
    [HideInInspector]
    private string name;
    [HideInInspector]
    public float Probperframe;
  private bool thisone;
  public Enviorment level;


    void Start()
    {
        ComInstanciate = false;

    }

    private void addmeteor(string PrefabPath, float velocityX, float velocityY, float gravity, string name)
    {

            Camera cam = Camera.main;
            float height = 2f * cam.orthographicSize;
            float width = height * cam.aspect;
            height += RainHeight;
            float posx = UnityEngine.Random.Range(XStarRain, width);
            float posy = UnityEngine.Random.Range(height, RainHeight);
            Vector3 newpos = new Vector3(posx, posy, 0f);
            GameObject newelement;

            newelement = Instan.GiveMeObject(name); //#INSTANCIADOR

            if (newelement == null)
            {
                newelement = (GameObject)Instantiate(Resources.Load(PrefabPath), newpos, transform.rotation);
            }
            else
            {
                //print("hemos usado el instanciador para cargar el prefab:" + name);
            }
            newelement.transform.position = newpos;
            newelement.SetActive(true);
            newelement.name = name;
            Rigidbody2D rb;
            SpriteAnimator Animator = newelement.GetComponent<SpriteAnimator>();
            if (Animator != null)
            {
                Animator.enabled = true;
            }
            rb = newelement.GetComponent<Rigidbody2D>();
            if (rb == null)
            {
                rb = newelement.AddComponent<Rigidbody2D>();
            }
            rb.gravityScale = gravity;
            rb.velocity = new Vector3(velocityX, velocityY, 0);
        
    }

  public void seleccom(float nextcom){
    float apcom1 = 0.2F;
    float apcom2 = 0.4F;
    float apcom3 = 0.6F;
    float apcom4 = 0.8F;
    float apcom5 = 1.0F;

    if(nextcom < apcom1 && nextcom > 0.0f){
      path += "1";
      name += "1";
    }else if(nextcom < apcom2 && nextcom > apcom1){
      path += "2";
      name += "2";
    }else if(nextcom < apcom3 && nextcom > apcom2){
      path += "3";
      name += "3";
    }else if(nextcom < apcom4 && nextcom > apcom3){
      path += "4";
      name += "4";
    }else if(nextcom < apcom5 && nextcom > apcom4){
      path += "5";
      name += "5";
    }
  }

  public void Update(){
    float gravity = UnityEngine.Random.Range(MinGravity, MaxGravity);
    path = "Prefabs/Cometas/";
    float nextcom = UnityEngine.Random.Range(0.0F, 1.0F);
    float COMApear = UnityEngine.Random.Range(0.0F, 1.0F);
    switch (level){
      case Enviorment.Cometas:
        path += "RojoCometa";
        name = "RojoCometa";
        seleccom(nextcom);
        thisone = true;
        break;
      case Enviorment.None:
        thisone = false;
        break;
    }
    if (COMApear < Probperframe && thisone == true && !ComInstanciate){
            ComInstanciate = true;
            addmeteor(path, -Speed, -YSpeed, gravity , name);
            StartCoroutine(SetComBool(TimeBetween, false));
        }
  }

    IEnumerator SetComBool(float time, bool Value)
    {
        yield return new WaitForSeconds(time);
        ComInstanciate = Value;
    }

}
