﻿using UnityEngine;
using System.Collections;
using System;

enum ParallaxDirection {
	Left, Right
}

public class ParallaxController : MonoBehaviour {

	
	
	/// <summary>
	/// The basic speed. A negative value will make the 
	/// elements move from right to left.
	/// </summary>
	[SerializeField] public float basicSpeed = 1f;
	
	[SerializeField] private ParallaxDirection direction;

	[SerializeField] private ParallaxLayer[] layers;

	void Awake() {
		layers = GetComponentsInChildren<ParallaxLayer>();
		
		if (direction == ParallaxDirection.Left) basicSpeed *= -1f;
	}
	
			
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < this.layers.Length; i++) {
			UpdateLayer(layers[i]);
		}
	}
	
	private void UpdateLayer(ParallaxLayer layer) {
		if(!layer.CanMove) return;
		layer.transform.Translate(Time.deltaTime * layer.Speed * basicSpeed, 0, 0);
	}
	
}
