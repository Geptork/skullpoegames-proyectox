﻿using UnityEngine;
using System.Collections;

public class bulletBehaviour : MonoBehaviour
{

    //public vars
    public enum Behaviour { Standard, Smart, Triple, SawBlade, HotGun, Rebote};
    public bool SeparateOfShooter = true;
    public Behaviour behaviour;

    [Header("Triple")]
    public int angleTripleBullet = 30;
    [Header("Only if the bullet have Triple and separate Behaviour")]
    public float Xoffset = 0f;
    [Header("Saw Blade")]

    [Range(0.00000001f, 0.5f)]
    public float SawBladeAceleration;
    [Range(0.00000001f, 0.5f)]
    public float SawBladeAngularAceleration;
    [Range(0.00000001F, 2*Mathf.PI)]
    public float SawMaxAceleration;

    [Header("Only for HotGun")]
    //valores recomendados
    [Range(0f, 0.1f)]
    public float Dispersion = 0.1f;
    [Range(0f, 0.5f)]
    public float TimeBetweenRateChange = 0.082f;
    [Range(0f, 0.05f)]
    public float SizeOfChange = 0.002f;
    [Header("Sobre-escribe 'mainFireRate' en KruxonController -solo con comp HotGun-")]
    public float IniFireRate = 2f;
    public float MinFireRate = 0.03f;

    private KruxonShotControl KSC;
    private float OldFireRate;
    private bool Hot = false;
    

    [Header("All")]
    public float bulletSpeed = 5;


    //privat vars
    [HideInInspector]
    public float angler = 0.0f;

    //vars to triple Bullet
    bool initiateTriple = true;
    bool initiateUp = true;
    bool initiateDown = true;


    void Start()
    {
        if (behaviour == Behaviour.HotGun)
        {
            KSC = GameObject.Find("Player").GetComponent<KruxonShotControl>();
        }
    }
    // Update is called once per frame
    void Update()
    {
        //All bullet behaviours
        switch (behaviour)
        {
            //STANDARD
            case Behaviour.Standard:
                transform.position = new Vector2(transform.position.x + (-bulletSpeed * Time.deltaTime), transform.position.y);
                break;

            //SMART
            case Behaviour.Smart:

                break;
            //Rebote
            case Behaviour.Rebote:
                transform.position = new Vector2(transform.position.x + (bulletSpeed * Mathf.Cos(angler)* Time.deltaTime), transform.position.y + (bulletSpeed*Mathf.Sin(angler) * Time.deltaTime));
                break;
            //TRIPLE
            case Behaviour.Triple:
                //Create 2 clones bullets only if is the original bullet (are not the Up and Down bullets)
                if (initiateTriple)
                {
                    if(gameObject.name != "bulletUP" && gameObject.name != "bulletDOWN")
                    {
                        initiateTriple = false;
                        GameObject bulletUp = Instantiate(this.gameObject, transform.position, Quaternion.identity) as GameObject;
                        bulletUp.name = "bulletUP";
                        GameObject bulletDown = Instantiate(this.gameObject, transform.position, Quaternion.identity) as GameObject;
                        bulletDown.name = "bulletDOWN";

                        if (!SeparateOfShooter)
                        {
                            //HACER TAMBIEN PARA ENEMIGOS MAS ADELANTE
                            bulletDown.transform.parent = GameObject.Find("Player").transform;
                            bulletUp.transform.parent = GameObject.Find("Player").transform;
                            bulletDown.transform.localPosition += new Vector3(Xoffset, 0, 0);
                            bulletUp.transform.localPosition += new Vector3(Xoffset, 0, 0);

                        }
                        
                    }
                }
                //Movement MainBullet
                if(gameObject.name != "bulletUP" && gameObject.name != "bulletDOWN")
                {
                    transform.Translate(-Vector3.right * Time.deltaTime * bulletSpeed);
                }
                //Rotate and Move bulletUP
                if (gameObject.name == "bulletUP")
                {
                    if (initiateUp)
                    {
                        initiateUp = false;
                        
                        transform.Rotate(Vector3.forward, angleTripleBullet);
                    }
                    transform.Translate(-Vector3.right * Time.deltaTime * bulletSpeed);
                }
                //Rotate and Move bulletDOWN
                if (gameObject.name == "bulletDOWN")
                {
                    if (initiateDown)
                    {
                        initiateDown = false;
                        transform.Rotate(Vector3.forward, -angleTripleBullet);
                    }
                    transform.Translate(-Vector3.right * Time.deltaTime * bulletSpeed);
                }

                
                break;


            case Behaviour.SawBlade:
                SawBladeAceleration += SawBladeAceleration/10;
                float Ac = Mathf.Exp(SawBladeAceleration);
                SawBladeAngularAceleration += SawBladeAngularAceleration/10;
                float F = Mathf.Exp(SawBladeAngularAceleration);
                if (F > SawMaxAceleration)
                {
                    F = SawMaxAceleration;
                }
                Vector3 NPosition = new Vector3(0, Random.Range(-0.1f, 0.1f), 0);
                transform.position += NPosition;
                transform.RotateAround(transform.position, Vector3.forward, -F*Mathf.PI);
                transform.position = new Vector2(transform.position.x + (-bulletSpeed * Time.deltaTime)*Ac, transform.position.y);
                break;

            case Behaviour.HotGun: //NO TERMINADA
                //Pruebas


                //no compatible con los controles de xbox por ahora (meter mas ORs)


                if ((Input.GetKey("mouse 0") || Input.GetKey("[5]"))){
                    if (!Hot) { StartCoroutine(ChangeFireRate()); }
                }

                if ((Input.GetKeyUp("mouse 0") || Input.GetKeyUp("[5]")))
                {
                    KSC.mainFireRate = IniFireRate;
                    Hot = false;
                }
                transform.position = new Vector2(transform.position.x + (-bulletSpeed * Time.deltaTime), transform.position.y);

                if (Hot)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + UnityEngine.Random.Range(-Dispersion, Dispersion));
                }

                break;
        }
    }

    IEnumerator ChangeFireRate()
    {
        yield return new WaitForSeconds(TimeBetweenRateChange);


        if(KSC.mainFireRate  <= MinFireRate)
        {
            //si llegamos al limite perdemos precision (añadir formato de dispersion)
            
            KSC.mainFireRate = MinFireRate;
            Hot = true;
        }
        else
        {
            KSC.mainFireRate -= SizeOfChange;
        }
    }

}
