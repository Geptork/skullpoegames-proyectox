using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class MeteorManager : MonoBehaviour
{
    public Instanciador Instan;
  public enum Enviorment {None, RedRocks, BlueRocks};
  public bool Rockinstanciate = false;
  public float TimeBetween = 0.5f;
    [HideInInspector]
    public float RainHeight;
    [HideInInspector]
    public float MinGravity;
    [HideInInspector]
    public float MaxGravity;
    [HideInInspector]
    public float Speed;
    [HideInInspector]
    public float YSpeed;

    private string path;

  private string name;
    [HideInInspector]
    public float Probperframe;
  private bool thisone;
  public Enviorment level;

    void Start()
    {
        Rockinstanciate = false;
        if (Instan == null)
        {
            Instan = GameObject.Find("Instanciador").GetComponent<Instanciador>();
        }
    }

  private void addmeteor( string PrefabPath, float velocityX, float velocityY, float gravity, string name){


            Camera cam = Camera.main;
            float height = 2f * cam.orthographicSize;
            float width = height * cam.aspect;
            width += 2.0F;
            height += RainHeight;
            float posx = width + 2.0F;
            float posy = UnityEngine.Random.Range(RainHeight, -5.0F);
            Vector3 newpos = new Vector3(posx, posy, 0f);
            GameObject newelement;

            newelement = Instan.GiveMeObject(name); //#INSTANCIADOR

            if (newelement == null)
            {
                newelement = (GameObject)Instantiate(Resources.Load(PrefabPath), newpos, transform.rotation);
            }
            else
            {
                //print("hemos usado el instanciador para cargar el prefab:" + name);
            }
            newelement.SetActive(true);
            newelement.name = name;
            newelement.transform.position = newpos;
            Rigidbody2D rb;
            SpriteAnimator Animator = newelement.GetComponent<SpriteAnimator>();
            if (Animator != null)
            {
                Animator.enabled = true;
            }
            rb = newelement.GetComponent<Rigidbody2D>();
            if (rb == null)
            {
                rb = newelement.AddComponent<Rigidbody2D>();
            }
            rb.gravityScale = gravity;
            rb.velocity = new Vector3(velocityX, velocityY, 0);
        
        
    }

  public void selecroc(float nextroc){
        //float aproca1 = 0.2f;
        //float aproca2 = 0.4f;
        //float aproca3 = 0.6f;
        //float aproca4 = 0.8f;
        //float aproca5 = 1.0f;
        //lo de arriba esta comentado porque la roca 5 es demasiado grande y la he sacado

        float aproca1 = 0.25f;
        float aproca2 = 0.5f;
        float aproca3 = 0.75f;
        float aproca4 = 1.0f;

        if (nextroc < aproca1 && nextroc > 0.0f){
      path += "1";
      name += "1";
    }else if(nextroc < aproca2 && nextroc > aproca1){
      path += "2";
      name += "2";
    }else if(nextroc < aproca3 && nextroc > aproca2){
      path += "3";
      name += "3";
    }else if(nextroc < aproca4 && nextroc > aproca3){
      path += "4";
      name += "4";
    //}else if(nextroc<aproca5 && nextroc> aproca4){
      //path += "5";
      //name += "5";
    }
  }

  public void Update(){
    float gravity = UnityEngine.Random.Range(MinGravity, MaxGravity);
    path = "Prefabs/Asteroids/";
    float nextroc = UnityEngine.Random.Range(0.0F, 1.0F);
    float RockApear = UnityEngine.Random.Range(0.0F, 1.0F);
    switch (level){
      case Enviorment.RedRocks:
        path += "RedRoca";
        name = "RedRoca";
        selecroc(nextroc);
        thisone = true;
        break;
      case Enviorment.BlueRocks:
        path += "roca";
        name = "roca";
        selecroc(nextroc);
        thisone = true;
        break;
      case Enviorment.None:
        thisone = false;
        break;
    }
    if (RockApear < Probperframe && thisone == true && !Rockinstanciate){
      addmeteor(path, -Speed, -YSpeed, gravity , name);
            Rockinstanciate = true;
            StartCoroutine(SetRockBool(TimeBetween, false));
    }
  }

    IEnumerator SetRockBool(float time, bool Value)
    {
        yield return new WaitForSeconds(time);
        Rockinstanciate = Value;
    }
}
