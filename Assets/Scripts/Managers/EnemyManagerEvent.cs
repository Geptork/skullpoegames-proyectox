﻿using UnityEngine;
using System.Collections;

public class EnemyManagerEvent : MonoBehaviour {

    public float Framechange;

    [Range(0.0f, 1.0f)]
    public float probBeSmart;

    [Header ("Space Enemies")]
    [Range(0.0f, 1f)]
    public float probBeMotorized;
    [Range(0.0f, 0.1f)]
    public float ProbBultoxAppear;
    public float MotorVelocity;
    [Range(0.0f, 0.1f)]
    public float ProbAxtorfisAppear;
    [Range(0.0f, 0.1f)]
    public float ProbNeptoAppear;
    [Range(0.0f, 0.1f)]
    public float ProbPileAppear;
    [Space (10)]
    public float NeptoHealth;
    public float BultoxHealth;
    public float BigBultoxHealth;
    public float PileHealth;
    public float AstorfixHealth;

    [Header ("Desert Enemies")]
    [Range(0.0f, 0.1f)]
    public float ProbMelforasAppear;
    [Range(0.0f, 0.1f)]
    public float ProbFliperAppear;
    [Range(0.0f, 0.1f)]
    public float ProbOsisteoAppear;
    [Range(0.0f, 0.1f)]
    public float ProbShalbrowAppear;
    [Space(10)]
    public float MelforasHealth;
    public float FliperHealth;
    public float OsisteoHealth;
    public float ShalbrowHealth;

    [Header ("Options")]
    public float RSpeedXDOWN;
    public float RSpeedXTOP;
    public float RSinusSpeedDOWN;
    public float RSinusSpeedTOP;
    public float RSinusRadiusDOWN;
    public float RSinusRadiusTOP;
    public float RpositionXDOWN;
    public float RpositionXTOP;
    public float RpositionYDOWN;
    public float RpositionYTOP;
    public float RspeedbulletscaleDOWN;
    public float RspeedbulletscaleTOP;
    public float RshootrateDOWN;
    public float RshootrateTOP;
    public float RshootdispersionDOWN;
    public float RshootdispersionTOP;
    public float RdamageDOWN;
    public float RdamageTOP;
    [HideInInspector]
    public bool Deployed = false;

}
