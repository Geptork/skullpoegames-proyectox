﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Cinematica : MonoBehaviour
{
    [HideInInspector]
    public int contador = 0;

    public bool CinematicOnStart = false;
    public float CinematicDelayOnStart = 2f;

    public float TimeBetweenEvents = 15f;
    public int NumberOfEvents = 0;
    public Instanciador Instan;


    //random entre estos dos valores (numero de naves en pantalla)
    public int MinNumShips = 5;
    public int MaxNumShips = 10;

    [Header("Inside of 'Prefabs/Enemies/Cinematica/' folder")]
    public string AllyPrefabName = "Player-C";
    public string EnemyPrefabName = "Bultox_cinematic";

    void Start()
    {
        if (CinematicOnStart)
        {
            StartCoroutine(CinematicOnDelayed(CinematicDelayOnStart));
        }

    }
    //Función para instanciar enemigos o aliados
    private void addShip(string PrefabPath, string name, float positionX, float positionY, float velocity)
    {
        float posx = positionX;
        float posy = positionY;
        Vector3 newpos = new Vector3(posx, posy, 10f);

            GameObject newelement;
            newelement = Instan.GiveMeObject(name); //Pruebas GiveMePreInstantiatedObject por GiveMeObject, NO FUNCIONA

        if (newelement == null)
            {
                newelement = (GameObject)Instantiate(Resources.Load(PrefabPath), newpos, transform.rotation);
            }
            newelement.transform.position = newpos;
            newelement.SetActive(true);
            newelement.name = name;
            Rigidbody2D rb;
            rb = newelement.GetComponent<Rigidbody2D>();
            if (rb == null)
            {
                rb = newelement.AddComponent<Rigidbody2D>();
            }
            rb.velocity = new Vector2(velocity, 0);
            newelement.GetComponentInChildren<WeaponTarget>().WakeUp();
        
    }
    //Buscamos al Bultox y lo añadimos usando addShip
    private void addEnemy(float PosX, float PosY)
    {
        string PrefabPath = "Prefabs/Enemies/Cinematica/" + EnemyPrefabName;
        string name = EnemyPrefabName;
        float velocity = -2f;
        addShip(PrefabPath, name, PosX, PosY, velocity);

    }
    //Buscamos a la nave y la añadimos usando addShip
    private void addAlly(float PosX, float PosY)
    {
        string PrefabPath = "Prefabs/Enemies/Cinematica/" + AllyPrefabName;
        string name = AllyPrefabName;
        float velocity = 2f;
        addShip(PrefabPath, name, PosX, PosY, velocity);
    }
    //Cinematica en si, se debe llamar esta funcion para reproducirla
    public void CinematicON()
    {
        contador++;
        float tdelay = 0f;
        Camera cam = Camera.main;
        float height = 2f * cam.orthographicSize;
        float width = height * cam.aspect;

        float PosAllyX;
        float PosEnemyX;
        float PosbothY;

        ArrayList OldPosY = new ArrayList();
        int numShips = UnityEngine.Random.Range( MinNumShips, MaxNumShips);
        
        for (int i = 0; i < numShips; i++)
        {
            PosAllyX = -width/2 - Random.Range(2f, 8f);
            PosEnemyX = width/2 + Random.Range(2f, 8f);
            PosbothY = 0.0f;

            // Codigo antisolapamiento de naves
            bool recalculadoY = false;
            var maxiter = 100;
            var iter = 0;
            while (!recalculadoY)
            {
                iter++;
                PosbothY = UnityEngine.Random.Range(-4.0f, 4.0f);
                recalculadoY = true;
                for (int x = 0; x < OldPosY.Count; x++)
                {
                    float posy = float.Parse(OldPosY[x].ToString());
                    //Comparamos si la posicion en que se instancia la nueva nave es demasiado cercana a todas las instanciadas anteriormente
                    if (posy > (PosbothY - 0.15f) && posy < (PosbothY) || posy < (PosbothY + 0.15f) && posy > (PosbothY))
                    {
                        recalculadoY = false;
                    }
                }

                if (OldPosY.Count == 0)
                {
                    recalculadoY = true;
                }

                //si alcanzamos el numero maximo de iteraciones para recalcular la posicion, es que es imposible no solaparlos.
                if (iter >= maxiter)
                {
                    recalculadoY = true;
                }
            }
            //Final antisolapamiento

            tdelay += 0.4f;
            StartCoroutine(addEnemyDelayed(tdelay, PosEnemyX, PosbothY));
            tdelay += 0.4f;
            StartCoroutine(addAllyDelayed(tdelay, PosAllyX, PosbothY));
            OldPosY.Add(PosbothY);
        }
        CancelInvoke("CinematicON");

        if (contador < NumberOfEvents)
        {
            Invoke("CinematicON", TimeBetweenEvents);
        }
    }

    IEnumerator addEnemyDelayed(float waitTime, float PosEnemyX, float PosbothY)
    {
        yield return new WaitForSeconds(waitTime);
        addEnemy(PosEnemyX, PosbothY);
    }

    IEnumerator addAllyDelayed(float waitTime, float PosAllyX, float PosbothY)
    {
        yield return new WaitForSeconds(waitTime);
        addAlly(PosAllyX, PosbothY);
    }

    IEnumerator CinematicOnDelayed(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        CinematicON();
    }
}