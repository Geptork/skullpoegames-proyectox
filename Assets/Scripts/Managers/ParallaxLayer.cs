﻿using UnityEngine;
using System.Collections;

public class ParallaxLayer : MonoBehaviour {

	[SerializeField] private float speed = 0f;
	
	private bool canMove = true;
	
	public float Speed {
		get { return speed; }
	}
	
	public bool CanMove {
		get { return canMove; }
		set { canMove = value; }
	}
}
