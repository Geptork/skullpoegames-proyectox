﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

    //AudioSource
    AudioSource musicComponent;
    float initialVolume;
    float currentVolume;

    //SONGS//

    //General music of this level
    public AudioClip levelMusic;

    //BOSS music
    public AudioClip introBossMusic;
    public AudioClip bossMusic;


    
	void Start () {
        musicComponent = GetComponent<AudioSource>();
        musicComponent.clip = levelMusic;
        musicComponent.loop = true;
        musicComponent.Play();
        initialVolume = musicComponent.volume;
    }

    //Function to call from other Script
    public void ChangeCurrentMusicTo(string newSong, float timeFadeOut, float newVolume)
    {
        //Select the new Song
        switch (newSong)
        {
            //Level Ended: Only need time to FadeOut.
            case "LevelEnded":
                StartCoroutine(stopMusic(timeFadeOut));
                break;
            
            //Boss: introSong, bossSong, time to FadeOut previous song, time between transition and boss song, volume of boss Song.
            case "Boss":
                StartCoroutine(PlayBossMusic(introBossMusic, bossMusic, timeFadeOut, 1.2f, newVolume));
                break;
        }
    }

    //Level ended, stop Music
    IEnumerator stopMusic(float fadeOutCurrentMusic)
    {
        //FadeOut Current Music
        initialVolume = musicComponent.volume;
        currentVolume = initialVolume;
        while (currentVolume > 0)
        {
            currentVolume -= (Time.deltaTime / fadeOutCurrentMusic) * initialVolume;
            musicComponent.volume = currentVolume;
            yield return null;
        }
        musicComponent.Stop();
    }


    //Change to Boss Music
    IEnumerator PlayBossMusic(AudioClip introSong, AudioClip bossSong, float fadeOutCurrentMusic, float fadeOutIntroTime, float nextSongVolume)
    {
        //FadeOut Current Music
        initialVolume = musicComponent.volume;
        currentVolume = initialVolume;
        while (currentVolume > 0)
        {
            currentVolume -= (Time.deltaTime / fadeOutCurrentMusic) * initialVolume;
            musicComponent.volume = currentVolume;
            yield return null;
        }
        //Intro Boss music
        musicComponent.loop = false;
        musicComponent.clip = introSong;
        musicComponent.volume = nextSongVolume;
        musicComponent.Play();
        yield return new WaitForSeconds(introSong.length - fadeOutIntroTime);
        //FadeOut Intro
        initialVolume = musicComponent.volume;
        currentVolume = initialVolume;
        while (currentVolume > 0)
        {
            currentVolume -= (Time.deltaTime / fadeOutIntroTime) * initialVolume;
            musicComponent.volume = currentVolume;
            yield return null;
        }
        //Boss music
        musicComponent.clip = bossSong;
        musicComponent.loop = true;
        musicComponent.volume = nextSongVolume;
        musicComponent.Play();
    }
}




