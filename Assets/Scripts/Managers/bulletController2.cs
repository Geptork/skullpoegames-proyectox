﻿using UnityEngine;
using System.Collections;

public class bulletController2 : MonoBehaviour {

    public Instanciador Instan;
    //public vars
    public enum Target { enemy, meteorite, enemyAndMeteorite, player, CinematicEnemy, CinematicAlly, Rebotada };
    public Target target;
    public float damage = 0f;
    public bool destroyOnImpact = true;
    public BloodManager BloodMan;


    void Start()
    {
        if(BloodMan == null)
        {
            BloodMan = GameObject.Find("Managers").GetComponent<BloodManager>();
        }

        if (Instan == null)
        {
            Instan = GameObject.FindGameObjectWithTag("Instanciador").GetComponent<Instanciador>();
        }
    }

    private void InstImpact(Color color, Vector3 Scale, string Path, string Name, Vector3 Pos, bool DestroyThis, float DestroyTime )
    {
        GameObject Blood; 
        Blood = Instan.GiveMeObject(Name); 
        Vector3 InstPos = Pos;

        if (Blood == null)
        {
            Blood = (GameObject)Instantiate(Resources.Load(Path), InstPos, transform.rotation);
        }
        Blood.transform.position = InstPos;
        Blood.GetComponent<ParticleSystem>().startColor = color;
        Blood.transform.localScale = Scale;

        Blood.SetActive(true);
        if (DestroyThis)
        {
            Destroy(Blood, DestroyTime);
        }
        else
        {
            //print("hemos usado el instanciador para cargar el prefab:" + Name);
        }
    }
    void OnTriggerEnter2D (Collider2D other)
    {
        switch (target)
        {
            //Target: Enemy
            case Target.enemy:
                if (other.gameObject.tag == "Armored")
                {
                    if (destroyOnImpact == true)
                    {
                            target = Target.Rebotada;
                            float limiteSuperior = this.gameObject.GetComponent<SpriteRenderer>().bounds.extents.y * 0.2f;
                            
                            float limiteInferior = this.gameObject.GetComponent<SpriteRenderer>().bounds.extents.y * 0.8f;

                            float PosBala = transform.position.y;
                            float TamanioCollider = other.gameObject.GetComponent<SpriteRenderer>().bounds.extents.y;
                            float PosArmor = other.gameObject.transform.position.y;

                            bool GolpeSuperior = (PosBala > PosArmor + 0.25*(TamanioCollider/2));
                            bool GolpeInferior = (PosBala < PosArmor - 0.25 *(TamanioCollider/2));
                            float angler = 0.0f;

                            if (GolpeSuperior)
                            {
                                angler = UnityEngine.Random.Range(-Mathf.PI/2, -Mathf.PI);
                                other.transform.Rotate(0, 0, -0.7f);
                                other.transform.Translate(0.02f, 0f,0f);
                            }

                            if (GolpeInferior)
                            {
                                angler = UnityEngine.Random.Range(+Mathf.PI/2, +Mathf.PI);
                                other.transform.Rotate(0, 0, 0.7f);
                                other.transform.Translate(0.02f, 0f, 0f);
                            }

                            if(!GolpeSuperior && !GolpeInferior)
                            {
                                angler = UnityEngine.Random.Range(2*Mathf.PI - Mathf.PI/10, 2*Mathf.PI + Mathf.PI/10);
                                other.transform.Translate(0.02f, 0f, 0f);
                            }

                            this.gameObject.GetComponent<bulletBehaviour>().angler = angler;
                            this.gameObject.GetComponent<bulletBehaviour>().behaviour = bulletBehaviour.Behaviour.Rebote;
                            transform.Rotate(Vector3.forward, transform.rotation.z + ((angler * 180) / Mathf.PI));
                            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                        if (other.gameObject.name == "Pilefarnes")
                        {
                            string BloodPath = "Prefabs/Particles/SangreSalpicadura";
                            string Name = "SangreSalpicadura";
                            float OffsetX = 0.75f;
                            GameObject Blood;
                            Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                            Color color = new Color(BloodMan.ImpactoPile.r, BloodMan.ImpactoPile.g, BloodMan.ImpactoPile.b);
                            Vector3 Scale = new Vector3(0.6f, 0.6f, 0.6f);
                            InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);
                        }
                    }
                }

                    if (other.gameObject.tag == "Enemy")
                {
                    //RESTAR VIDA AL ENEMIGO
                    other.GetComponent<EnemyHealthControl>().DoDamage(damage);
                    if (destroyOnImpact == true)
                    {
                        Destroy(this.gameObject);
                    }
                }

                //si es un bultox, tenemos que ponerle sangre HACER ASTORFIS CON SANGRE LILA Y TAMBIEN EL BLINZ CON SANGRE MAS ESCALADA
                if (other.gameObject.name == "Bultox")
                {
                    //deberiamos hacer un random de sangres para bultox para que no sean todas iguales
                    string BloodPath = "Prefabs/Particles/SangreSalpicadura";
                    float OffsetX = 0.75f;
                    Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                    Color color = new Color (BloodMan.SangreBultox.r, BloodMan.SangreBultox.g, BloodMan.SangreBultox.b);
                    Vector3 Scale = new Vector3(1, 1, 1);
                    string Name = "SangreSalpicadura";
                    InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);
                }

                if (other.gameObject.name == "Axtorfis")
                {
                    //deberiamos hacer un random de sangres para bultox para que no sean todas iguales
                    string BloodPath = "Prefabs/Particles/SangreSalpicadura";
                    float OffsetX = 0.75f;
                    string Name = "SangreSalpicadura";
                    Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                    Color color = new Color (BloodMan.SangreAstorfix.r, BloodMan.SangreAstorfix.g, BloodMan.SangreAstorfix.b);
                    Vector3 Scale = new Vector3(1,1,1);
                    InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);
       
                }

                if (other.gameObject.name == "BultoxGigante")
                {
                    //deberiamos hacer un random de sangres para bultox para que no sean todas iguales

                    //offset especificos para bultox gordos
                    string BloodPath = "Prefabs/Particles/SangreSalpicadura";
                    float OffsetX = 0.5f;
                    float OffsetBodyBigg = 4f;
                    if(transform.position.y < (other.gameObject.transform.position.y - other.gameObject.GetComponent<Collider2D>().bounds.extents.y) + OffsetBodyBigg)
                    {
                        OffsetX += 2.25f;
                    }
                    string Name = "SangreSalpicadura";
                    Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                    Color color = new Color(BloodMan.SangreBultox.r, BloodMan.SangreBultox.g, BloodMan.SangreBultox.b);
                    Vector3 Scale = new Vector3(4, 4, 4);
                    InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);

                }
                if (other.gameObject.name == "NaveNeptofaria")
                {
                    //deberiamos hacer un random de sangres para bultox para que no sean todas iguales
                    string BloodPath = "Prefabs/Particles/ChispasNepto";
                    float OffsetX = 0.5f;
                    Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                    Color color = new Color(BloodMan.ImpactoNepto.r, BloodMan.ImpactoNepto.g, BloodMan.ImpactoNepto.b);
                    string Name = "ChispasNepto";
                    Vector3 Scale = new Vector3(1, 1, 1);
                    InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);

                }
                //------------------------AJUSTAR-------------------------------------------------------------
                if (other.gameObject.name == "BlinzerionF1")
                {
                    //deberiamos hacer un random de sangres para bultox para que no sean todas iguales
                    string BloodPath = "Prefabs/Particles/ChispasNepto";
                    float OffsetX = 0.45f;
                    string Name = "ChispasNepto";
                    Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                    Color color = new Color(BloodMan.ImpactoNepto.r, BloodMan.ImpactoNepto.g, BloodMan.ImpactoNepto.b);
                    Vector3 Scale = new Vector3(3, 3, 3);
                    InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);

                }
                if (other.gameObject.name == "BlinzerionF2")
                {
                    //deberiamos hacer un random de sangres para bultox para que no sean todas iguales
                    string BloodPath = "Prefabs/Particles/SangreSalpicadura";
                    float OffsetX = 4.2f;
                    string Name = "SangreSalpicadura";
                    Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                    Color color = new Color(BloodMan.SangreBlinz.r, BloodMan.SangreBlinz.g, BloodMan.SangreBlinz.b);
                    Vector3 Scale = new Vector3(5, 5, 5);
                    InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);

                }
                if (other.gameObject.name == "BlinzerionF3")
                {
                    //deberiamos hacer un random de sangres para bultox para que no sean todas iguales
                    string BloodPath = "Prefabs/Particles/SangreSalpicadura";
                    float OffsetX = 2f;
                    string Name = "SangreSalpicadura";
                    Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                    Color color = new Color(BloodMan.SangreBlinz.r, BloodMan.SangreBlinz.g, BloodMan.SangreBlinz.b);
                    Vector3 Scale = new Vector3(4, 4, 4);
                    InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);
                }

                //EN CONSTRUCCION!!!!!!!!!!!!!!!!!!!!
                if (other.gameObject.name == "Kdio")                //EN CONSTRUCCION!!!!!!!!!!!!!!!!!!!!                   
                {
                        if (other.GetComponent<EnemyHealthControl>().GetShield() > 0F)                 //EN CONSTRUCCION!!!!!!!!!!!!!!!!!!!!
                        {
                            //INSTANCIAR CHISPAS
                            string BloodPath = "Prefabs/Particles/ChispasNepto"; // CREAR SUS PROPIAS PARTICULAS
                            float OffsetX = 2f;
                            string Name = "ChispasNepto"; // CREAR SUS PROPIAS PARTICULAS
                            Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                            Color color = new Color(BloodMan.ImpactoChispasKdio.r, BloodMan.ImpactoChispasKdio.g, BloodMan.ImpactoChispasKdio.b);
                            Vector3 Scale = new Vector3(4, 4, 4);
                            InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);
                        }
                        else
                        {
                            //INSTANCIAR SANGRE MORADA
                            string BloodPath = "Prefabs/Particles/ImpactoGas"; // CREAR SUS PROPIAS PARTICULAS
                            float OffsetX = 2f;
                            string Name = "ImpactoGas"; // CREAR SUS PROPIAS PARTICULAS
                            Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                            Color color = new Color(BloodMan.ImpactoHumoKdio.r, BloodMan.ImpactoHumoKdio.g, BloodMan.ImpactoHumoKdio.b);
                            Vector3 Scale = new Vector3(1.5f, 1.5f, 1.5f);
                            InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);
                        }

                    //EN CONSTRUCCION!!!!!!!!!!!!!!!!!!!!

                }
                break;

            //Target: meteorite
            case Target.meteorite:
                if (other.gameObject.tag == "Meteorite")
                {
                    //RESTAR VIDA AL METEORITO
                    other.GetComponent<EnemyHealthControl>().DoDamage(damage);
                    if (destroyOnImpact == true)
                    {
                        Destroy(this.gameObject);
                    }
                }
                break;

            //Target: enemyAndMeteorite
            case Target.enemyAndMeteorite:

                if (other.gameObject.tag == "Armored")
                {
                    if (destroyOnImpact == true)
                    {
                        target = Target.Rebotada;
                        float limiteSuperior = this.gameObject.GetComponent<SpriteRenderer>().bounds.extents.y * 0.2f;

                        float limiteInferior = this.gameObject.GetComponent<SpriteRenderer>().bounds.extents.y * 0.8f;

                        float PosBala = transform.position.y;
                        float TamanioCollider = other.gameObject.GetComponent<SpriteRenderer>().bounds.extents.y;
                        float PosArmor = other.gameObject.transform.position.y;

                        bool GolpeSuperior = (PosBala > PosArmor + 0.25 * (TamanioCollider / 2));
                        bool GolpeInferior = (PosBala < PosArmor - 0.25 * (TamanioCollider / 2));
                        float angler = 0.0f;

                        if (GolpeSuperior)
                        {
                            angler = UnityEngine.Random.Range(-Mathf.PI / 2, -Mathf.PI);
                        }

                        if (GolpeInferior)
                        {
                            angler = UnityEngine.Random.Range(+Mathf.PI / 2, +Mathf.PI);
                        }

                        if (!GolpeSuperior && !GolpeInferior)
                        {
                            angler = UnityEngine.Random.Range(2 * Mathf.PI - Mathf.PI / 10, 2 * Mathf.PI + Mathf.PI / 10);
                        }

                        this.gameObject.GetComponent<bulletBehaviour>().angler = angler;
                        this.gameObject.GetComponent<bulletBehaviour>().behaviour = bulletBehaviour.Behaviour.Rebote;
                        transform.Rotate(Vector3.forward, transform.rotation.z + ((angler * 180) / Mathf.PI));
                        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                        if (other.gameObject.name == "Pilefarnes")
                        {
                            string BloodPath = "Prefabs/Particles/SangreSalpicadura";
                            string Name = "SangreSalpicadura";
                            float OffsetX = 0.75f;
                            Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                            Color color= new Color(BloodMan.ImpactoPile.r, BloodMan.ImpactoPile.g, BloodMan.ImpactoPile.b);
                            Vector3 Scale = new Vector3(0.6f, 0.6f, 0.6f);
                            InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);

                        }
                    }
                }

                if (other.gameObject.tag == "Meteorite" || other.gameObject.tag == "Enemy")
                {
                    //RESTAR VIDA AL ENEMIGO Y METEORITO
                    other.GetComponent<EnemyHealthControl>().DoDamage(damage);
                    if (destroyOnImpact == true)
                    {
                        Destroy(this.gameObject);
                    }
                }

                //si es un bultox, tenemos que ponerle sangre HACER ASTORFIS CON SANGRE LILA Y TAMBIEN EL BLINZ CON SANGRE MAS ESCALADA
                if (other.gameObject.name == "Bultox")
                {
                    //deberiamos hacer un random de sangres para bultox para que no sean todas iguales
                    string BloodPath = "Prefabs/Particles/SangreSalpicadura";
                    float OffsetX = 0.75f;
                    Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                    Color color = new Color(BloodMan.SangreBultox.r, BloodMan.SangreBultox.g, BloodMan.SangreBultox.b);
                    Vector3 Scale = new Vector3(1, 1, 1);
                    string Name = "SangreSalpicadura";
                    InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);
                }

                if (other.gameObject.name == "Axtorfis")
                {
                    //deberiamos hacer un random de sangres para bultox para que no sean todas iguales
                    string BloodPath = "Prefabs/Particles/SangreSalpicadura";
                    float OffsetX = 0.75f;
                    string Name = "SangreSalpicadura";
                    Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                    Color color = new Color(BloodMan.SangreAstorfix.r, BloodMan.SangreAstorfix.g, BloodMan.SangreAstorfix.b);
                    Vector3 Scale = new Vector3(1, 1, 1);
                    InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);
                }

                if (other.gameObject.name == "BultoxGigante")
                {
                    //deberiamos hacer un random de sangres para bultox para que no sean todas iguales

                    string BloodPath = "Prefabs/Particles/SangreSalpicadura";
                    float OffsetX = 0.5f;
                    float OffsetBodyBigg = 4f;
                    if (transform.position.y < (other.gameObject.transform.position.y - other.gameObject.GetComponent<Collider2D>().bounds.extents.y) + OffsetBodyBigg)
                    {
                        OffsetX += 2.25f;
                    }
                    string Name = "SangreSalpicadura";
                    Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                    Color color = new Color(BloodMan.SangreBultox.r, BloodMan.SangreBultox.g, BloodMan.SangreBultox.b);
                    Vector3 Scale = new Vector3(4, 4, 4);
                    InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);
                }

                if (other.gameObject.name == "Kdio")                //EN CONSTRUCCION!!!!!!!!!!!!!!!!!!!!                   
                {
                    if (other.GetComponent<EnemyHealthControl>().GetShield() > 0F)                 //EN CONSTRUCCION!!!!!!!!!!!!!!!!!!!!
                    {
                        //INSTANCIAR CHISPAS
                        string BloodPath = "Prefabs/Particles/ChispasNepto"; // CREAR SUS PROPIAS PARTICULAS
                        float OffsetX = 2f;
                        string Name = "ChispasNepto"; // CREAR SUS PROPIAS PARTICULAS
                        Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                        Color color = new Color(BloodMan.ImpactoChispasKdio.r, BloodMan.ImpactoChispasKdio.g, BloodMan.ImpactoChispasKdio.b);
                        Vector3 Scale = new Vector3(4, 4, 4);
                        InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);
                    }
                    else
                    {
                        //INSTANCIAR SANGRE MORADA
                        string BloodPath = "Prefabs/Particles/ImpactoGas"; // CREAR SUS PROPIAS PARTICULAS
                        float OffsetX = 2f;
                        string Name = "ImpactoGas"; // CREAR SUS PROPIAS PARTICULAS
                        Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                        Color color = new Color(BloodMan.ImpactoHumoKdio.r, BloodMan.ImpactoHumoKdio.g, BloodMan.ImpactoHumoKdio.b);
                        Vector3 Scale = new Vector3(1.5f, 1.5f, 1.5f);
                        InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);
                    }

                    //EN CONSTRUCCION!!!!!!!!!!!!!!!!!!!!

                }

                break;

            //Target: player
            case Target.player:
                if (other.gameObject.tag == "Player")
                {
                    if (this.gameObject.name == "Bultox_BulletAlt" || this.gameObject.name == "Bultox_BulletAlt(Clone)")
                    {
                        //deberiamos hacer un random de sangres para bultox para que no sean todas iguales
                        string BloodPath = "Prefabs/Particles/SangreSalpicadura";
                        string Name = "SangreSalpicadura";
                        Color color = new Color(BloodMan.SangreBultoxEnNave.r, BloodMan.SangreBultoxEnNave.g, BloodMan.SangreBultoxEnNave.b);
                        Vector3 Scale = new Vector3(1, 1, 1);
                        InstImpact(color, Scale, BloodPath, Name, gameObject.transform.position, true, 1F);
                    }
                    //RESTAR VIDA AL PLAYER
                    other.GetComponent<PlayerHealthController>().DoDamage(damage);
                    if (destroyOnImpact == true)
                    {
                        Destroy(this.gameObject);
                    }
                }
                break;
                //cinematic
            case Target.CinematicEnemy:
                if (other.gameObject.tag == "Enemy-C")
                {
                    //RESTAR VIDA AL ENEMIGO Y METEORITO
                    other.GetComponent<EnemyHealthControl>().DoDamage(damage);
                    if (destroyOnImpact == true)
                    {
                        Destroy(this.gameObject);
                    }

 
                }
                break;
            //cinematic
            case Target.CinematicAlly:
                if (other.gameObject.tag == "Nave-C")
                {
                    //RESTAR VIDA AL ENEMIGO Y METEORITO
                    other.GetComponent<EnemyHealthControl>().DoDamage(damage);
                    if (destroyOnImpact == true)
                    {
                        Destroy(this.gameObject);
                    }
                }
                break;
            //cinematic
            case Target.Rebotada:
                //.DoDamage(damage)
                EnemyHealthControl EHC = other.GetComponent<EnemyHealthControl>();
                PlayerHealthController PHC = other.GetComponent<PlayerHealthController>();
                if (other.gameObject.tag == "Armored")
                {
                    if (destroyOnImpact == true)
                    {
                        string BloodPath = "Prefabs/Particles/SangreSalpicadura";
                        string Name = "SangreSalpicadura";
                        float OffsetX = 0.75f;
                        GameObject Blood;
                        Vector3 InstPos = new Vector3(transform.position.x + OffsetX, transform.position.y, 0);
                        Color color = new Color(BloodMan.ImpactoPile.r, BloodMan.ImpactoPile.g, BloodMan.ImpactoPile.b);
                        Vector3 Scale = new Vector3(0.6f, 0.6f, 0.6f);
                        InstImpact(color, Scale, BloodPath, Name, InstPos, true, 1F);
                    }
                }
                else
                {


                    if (destroyOnImpact == true && (other.gameObject.tag == "Player"|| other.gameObject.tag == "Enemy"))
                    {
                        if (EHC != null)
                        {
                            EHC.DoDamage(damage);
                        }

                        if (PHC != null)
                        {
                            PHC.DoDamage(damage);
                        }
                        Destroy(this.gameObject);

                        if(other.gameObject.tag == "Player")
                        {
                            string BloodPath = "Prefabs/Particles/ChispasNepto";
                            Color color = new Color(BloodMan.ImpactoNepto.r, BloodMan.ImpactoNepto.g, BloodMan.ImpactoNepto.b);
                            string Name = "ChispasNepto";
                            Vector3 Scale = new Vector3(1, 1, 1);
                            InstImpact(color, Scale, BloodPath, Name, gameObject.transform.position, true, 1F);
                        }
                    }
                }
            break;
        }
    }
}
