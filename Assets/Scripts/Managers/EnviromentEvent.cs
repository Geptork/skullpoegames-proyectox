using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class EnviromentEvent : MonoBehaviour
{
  public float Framechange;
  //cometas
  public float ComRainHeight;
  public float ComMinGravity;
  public float ComMaxGravity;
  public float ComXStarRain;
  public float ComSpeed;
  public float ComYSpeed;
  public float ComProbperframe;
//meteoritos
  public float METRainHeight;
  public float METMinGravity;
  public float METMaxGravity;
  public float METSpeed;
  public float METYSpeed;
  public float METProbperframe;
    [HideInInspector]
    public bool Deployed = false;
}
