﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class EventManager : MonoBehaviour
{
    //private 
    float currentFrame;
    float frameOffset;
    Cinematica Cine;
    MusicManager Music;
    //public

    //Cinematica
    [Header("Cinematica")]
    public float CinematicStartFrame;
    public bool CinematicDeployed = false;
    //Boss Blinzerion
    [Header("Blinzerion Boss")]
    public float BlinStartFrame;
    public float KdioStartFrame;
    public bool BlinDeployed = false;
    public bool KdioDeployed = false;
    [Header("Enemy Manager 2")]
    public EnemyManager2 EM2;
    public List<EnemyManagerEvent> EnemyManagerEvents;
    [Header("Planeta")]
    public GameObject Planeta;
    public float TimeAfterBoss;
    GameObject BossBar;
    FadeIn FI;

    void Start ()
    {
        Cine = GameObject.Find("Cinematic").GetComponent<Cinematica>();
        Music = GameObject.Find("Manager").GetComponentInChildren<MusicManager>();
        BossBar = GameObject.Find("GameplayPrueba").GetComponent<SpaceHUD>().BossUI;
        FI = GameObject.Find("GameplayPrueba").GetComponentInChildren<FadeIn>();
    }

    // Update 
    void Update()
    {
        currentFrame = Time.timeSinceLevelLoad - frameOffset;


        //Deploy Cinematic
        if (currentFrame > CinematicStartFrame && CinematicDeployed == false)
        {
            Cine.CinematicON();
            CinematicDeployed = true;
        }

        //Deploy Blinzerion
        if (currentFrame > BlinStartFrame && BlinDeployed == false)
        {
            AddBlin();
            BlinDeployed = true;
            EM2.ProbBultoxAppear = 0.0f; //provisional 
            EM2.ProbAxtorfisAppear = 0.0f; //provisional 
            Music.ChangeCurrentMusicTo("Boss", 3f, 0.6f);
        }
        //Deploy Kdio

        if (currentFrame > KdioStartFrame && KdioDeployed == false)
        {
            AddKdio();
            KdioDeployed = true;
            EM2.ProbBultoxAppear = 0.0f; //provisional 
            EM2.ProbAxtorfisAppear = 0.0f; //provisional 
            Music.ChangeCurrentMusicTo("Boss", 3f, 0.6f);
        }
        for (int ev = 0; ev < EnemyManagerEvents.Count; ev++)
        {

            if (currentFrame > EnemyManagerEvents[ev].Framechange && !EnemyManagerEvents[ev].Deployed)
            {
                EnemyManagerEvents[ev].Deployed = true;
                ChangeEnemies(EnemyManagerEvents[ev]);
            }
        }
        //------------------------PROVISIONAL----------------------------
        if((currentFrame > BlinStartFrame+15) && BossBar == null)
        {
            Invoke("PlanetaON", TimeAfterBoss);
            Invoke("FadeIn", TimeAfterBoss- (FI.timespan-2));       
        }
        //-------------------------------------------------------------
    }

    //Add Blinzerion
    void AddKdio()
    {
        GameObject Kdio = GameObject.FindGameObjectWithTag("Instanciador").GetComponent<Instanciador>().GiveMeObject("Kdio");

        if(Kdio == null)
        {
            Debug.LogError("KDIO DEBE ESTAR EN EL INSTANCIADOR");
        }

        Kdio.SetActive(true);
        Kdio.transform.position = new Vector3(40f, 0, 0);

        //Desactivar enemigos 
        EM2.ProbBultoxAppear = 0.0f; //provisional 
        EM2.ProbAxtorfisAppear = 0.0f; //provisional 
        //Music.ChangeCurrentMusicTo("Boss", 3f, 0.6f); //poner musica especifica de kdio
    }
        //Add Blinzerion
        void AddBlin()
    {
        float tdelayed = 0.0f;
        //Paramos el scroll del Background
        ParallaxController EnviBack = GameObject.Find("Enviroment").GetComponent<ParallaxController>();
        EnviBack.basicSpeed = 0f;

        //Añadimos todos los Blinzerion a la vez, los gestiona la IA
        EnemyManager2 Enemy = GameObject.Find("Manager").GetComponentInChildren<EnemyManager2>();
        tdelayed += 0.5f;
        StartCoroutine(addBossPartDelayed(Enemy, tdelayed, "BlinzerionF1", "Prefabs/Enemies/Blinzerion/BlinzerionF1", 16, 0, 0, 180, 0, false));
        tdelayed += 0.5f;
        StartCoroutine(addBossPartDelayed(Enemy, tdelayed, "BlinzerionF1.5", "Prefabs/Enemies/Blinzerion/BlinzerionF1.5", 4, 0, 0, 180, 0, false));
        tdelayed += 0.5f;
        StartCoroutine(addBossPartDelayed(Enemy, tdelayed, "BlinzerionF2", "Prefabs/Enemies/Blinzerion/BlinzerionF2", 4, 0, 0, 180, 0, false));
        tdelayed += 0.5f;
        StartCoroutine(addBossPartDelayed(Enemy, tdelayed, "BlinzerionF3", "Prefabs/Enemies/Blinzerion/BlinzerionF3", 4, 0, 0, 180, 0, false));
        tdelayed += 0.5f;
        StartCoroutine(addBossPartDelayed(Enemy, tdelayed, "Centinela1", "Prefabs/Enemies/Cent/Centinela1", 0, -6.6f, 0, 0, 0, false));
        tdelayed += 0.5f;
        StartCoroutine(addBossPartDelayed(Enemy, tdelayed, "Centinela2", "Prefabs/Enemies/Cent/Centinela2", 0f, 6.6f, 0, 0, 0, false));

        //Añadimos los Cents anti "trampas"
        tdelayed += 0.5f;
        StartCoroutine(addBossPartDelayed(Enemy, tdelayed, "Centinela1.1", "Prefabs/Enemies/Cent/Centinela1", -5.5f, -20.6f, 0, 0, 0, false));
        tdelayed += 0.5f;
        StartCoroutine(addBossPartDelayed(Enemy, tdelayed, "Centinela2.1", "Prefabs/Enemies/Cent/Centinela2", -5.5f, 20.6f, 0, 0, 0, true));

    }

    IEnumerator addBossPartDelayed(EnemyManager2 Enemy, float waitTime, string name, string PrefabPath, float X, float Y, float RX, float RY, float RZ, bool lastOne)
    {
        yield return new WaitForSeconds(waitTime);
        Enemy.addblinz(PrefabPath, name, X, Y, RX, RY, RZ);
        GameObject BossPart = GameObject.Find(name);
        BossPart.transform.position += new Vector3(100,0,0);
        if (lastOne)
        {
            //Movemos a los Blinzerions instanciados a sus respectivos puestos
            GameObject.Find("BlinzerionF1").transform.position -= new Vector3(100, 0, 0);
            GameObject.Find("BlinzerionF1.5").transform.position -= new Vector3(100, 0, 0);
            GameObject.Find("BlinzerionF2").transform.position -= new Vector3(100, 0, 0);
            GameObject.Find("BlinzerionF3").transform.position -= new Vector3(100, 0, 0);
            GameObject.Find("Centinela1").transform.position -= new Vector3(100, 0, 0);
            GameObject.Find("Centinela2").transform.position -= new Vector3(100, 0, 0);
            GameObject.Find("Centinela1").GetComponent<PosCent>().Yposition = -3.6f;
            GameObject.Find("Centinela2").GetComponent<PosCent>().Yposition = 3.6f;
            GameObject.Find("Centinela1").GetComponent<PosCent>().VelMov = 1f;
            GameObject.Find("Centinela2").GetComponent<PosCent>().VelMov = 1f;

            //Desplazamos los Cents anti "trampas"
            GameObject.Find("Centinela1.1").transform.position -= new Vector3(100, 0, 0);
            GameObject.Find("Centinela2.1").transform.position -= new Vector3(100, 0, 0);
            GameObject.Find("Centinela1.1").GetComponent<PosCent>().Yposition = -6f;
            GameObject.Find("Centinela2.1").GetComponent<PosCent>().Yposition = 6f;
            GameObject.Find("Centinela1.1").GetComponent<PosCent>().VelMov = 1f;
            GameObject.Find("Centinela2.1").GetComponent<PosCent>().VelMov = 1f;

            //activamos la IA del Blinzerion
            BlinzIA IA = GameObject.Find("Manager").GetComponentInChildren<BlinzIA>();
            IA.enabled = true;

            //Seteamos los Blinzerions instanciados a sus respectivos puestos de la IA 
            IA.Blinz0 = GameObject.Find("BlinzerionF1");
            IA.Blinz1 = GameObject.Find("BlinzerionF1.5");
            IA.Blinz2 = GameObject.Find("BlinzerionF2");
            IA.Blinz3 = GameObject.Find("BlinzerionF3");

            //Seteamos los Cents en la IA
            IA.Cent1 = GameObject.Find("Centinela1");
            IA.Cent2 = GameObject.Find("Centinela2");
            IA.Restart();
        }
    }

       private void ChangeEnemies(EnemyManagerEvent EME)
        {
            EM2.probBeSmart = EME.probBeSmart;
            EM2.PileHealth = EME.PileHealth;
            EM2.NeptoHealth = EME.NeptoHealth;
            EM2.BigBultoxHealth = EME.BigBultoxHealth;
            EM2.BultoxHealth = EME.BultoxHealth;
            EM2.AstorfixHealth = EME.AstorfixHealth;
            EM2.ProbPileAppear = EME.ProbPileAppear;
            EM2.probBeMotorized = EME.probBeMotorized;
            EM2.MotorVelocity = EME.MotorVelocity;
            EM2.ProbBultoxAppear = EME.ProbBultoxAppear;
            EM2.ProbNeptoAppear = EME.ProbNeptoAppear;
            EM2.ProbAxtorfisAppear = EME.ProbAxtorfisAppear;
            EM2.RSpeedXDOWN= EME.RSpeedXDOWN;
            EM2.RSpeedXTOP = EME.RSpeedXTOP;
            EM2.RSinusSpeedDOWN= EME.RSinusSpeedDOWN;
            EM2.RSinusSpeedTOP = EME.RSinusSpeedTOP;
            EM2.RSinusRadiusDOWN= EME.RSinusRadiusDOWN;
            EM2.RSinusRadiusTOP= EME.RSinusRadiusTOP;
            EM2.RpositionXDOWN= EME.RpositionXDOWN ;
            EM2.RpositionXTOP= EME.RpositionXTOP ;
            EM2.RpositionYDOWN= EME.RpositionYDOWN ;
            EM2.RpositionYTOP= EME.RpositionYTOP ;
            EM2.RspeedbulletscaleDOWN = EME.RspeedbulletscaleDOWN;
            EM2.RspeedbulletscaleTOP= EME.RspeedbulletscaleTOP ;
            EM2.RshootrateDOWN= EME.RshootrateDOWN;
            EM2.RshootrateTOP= EME.RshootrateTOP ;
            EM2.RshootdispersionDOWN= EME.RshootdispersionDOWN ;
            EM2.RshootdispersionTOP= EME.RshootdispersionTOP ;
            EM2.RdamageDOWN= EME.RdamageDOWN ;
            EM2.RdamageTOP = EME.RdamageTOP ;

        EM2.ProbMelforasAppear = EME.ProbMelforasAppear;
        EM2.ProbFliperAppear = EME.ProbFliperAppear;
        EM2.ProbOsisteoAppear = EME.ProbOsisteoAppear;
        EM2.ProbShalbrowAppear = EME.ProbShalbrowAppear;
        EM2.MelforasHealth = EME.MelforasHealth;
        EM2.FliperHealth = EME.FliperHealth;
        EM2.OsisteoHealth = EME.OsisteoHealth;
        EM2.ShalbrowHealth = EME.ShalbrowHealth;



    }
    public void PlanetaON()
    {
        //Planeta.SetActive(true);
        SceneManager.LoadScene("Credits");
    }
    public void FadeIn()
    {
        FI.enabled = true;
    }
}
