﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextManager : MonoBehaviour
{

    public AudioSource Audio;
    public Text DialogosText;
    public Text Eventos;
    public Dialogos DialogosBase;
    public float CurrentFrame;
    private float FrameOffset;

    [Header ("Jetos")]
    public GameObject radio;
    public GameObject ProtaFace;
    public GameObject KruxonFace;
    public GameObject ArkFace;

    //Radio1 - Se activa al recoger RadioCollectable (Item)
    [Header ("Radio 1")]
    public bool Radio1isActive;
    //Animated radio GameObject

    //AUDIO CLIPS R1
    [Header("Radio 1 Audio Clips")]
    public AudioClip r1;
    public AudioClip r2;
    public AudioClip r3;

    //Text R1
    [Header("Radio 1 Labels")]
    public GameObject labelRadio1;
    public GameObject labelRadio2;
    public GameObject labelRadio3;

    [Header ("Cinematica 1")]
    //Dialogo Cinematica
    public bool InCine;
    public bool ThisCineIsIntro = true;
    public bool ThisCineIsDesertLanding = false;
    public float DialogCineFrame;
    [Header("Lvl 1: Red Space")]
    public bool InLvl1;
    public bool SattelitDied;

    void Start()
    {
        CurrentFrame = 0.0F;
        FrameOffset = 0.0F;
        radio.SetActive(false);
        KruxonFace.SetActive(false);
        ProtaFace.SetActive(false);

        SattelitDied = false;

    }
void Update()
    {
        CurrentFrame = Time.timeSinceLevelLoad - FrameOffset;

        if (Radio1isActive == true)
        {
            radio.SetActive(true);
            StartCoroutine(Messages());
            Radio1isActive = false;
        }
        if(InCine == true && CurrentFrame > DialogCineFrame)
        {
            StartCoroutine(CinematicaDialog());
            InCine = false;
        }
        if(InLvl1 == true && CurrentFrame > 1f)
        {
            StartCoroutine(Lvl1Start());
            InLvl1 = false;
        }

        if (SattelitDied == true && InLvl1 == true)
        {
            StartCoroutine(SateliteMuerto());
        }
    }
    //Corutine que reporduce los messages en orden y espera para saltar al siguiente
    IEnumerator Messages()
    {
        ShowAndSound(r1, labelRadio1);
        yield return new WaitForSeconds(Audio.clip.length);

        labelRadio1.SetActive(false);
        ShowAndSound(r2, labelRadio2);
        yield return new WaitForSeconds(Audio.clip.length);

        labelRadio2.SetActive(false);
        ShowAndSound(r3, labelRadio3);
        yield return new WaitForSeconds(Audio.clip.length);

        //Finalmente desactiva el sprite de la radio y el último mensaje
        radio.SetActive(false);
        labelRadio3.SetActive(false);
    }

    //function that plays the messages (audio and text)
    bool ShowAndSound(AudioClip r, GameObject label)
    {

        Audio.clip = r;
        Audio.Play();
        label.SetActive(true);

        return true;
    }

    //Cinematica1
    IEnumerator CinematicaDialog()
    {
        if (ThisCineIsIntro)
        {
            DialogosText.text = DialogosBase.Cine_01Player;
            ProtaFace.SetActive(true);
            yield return new WaitForSeconds(9f);
            ProtaFace.SetActive(false);
            DialogosText.text = DialogosBase.Cine_02Kruxon;
            KruxonFace.SetActive(true);
            yield return new WaitForSeconds(8f);
            KruxonFace.SetActive(false);
            DialogosText.text = DialogosBase.Cine_03Player;
            ProtaFace.SetActive(true);
            yield return new WaitForSeconds(8f);
            ProtaFace.SetActive(false);

            DialogosText.text = " ";
        }
        if (ThisCineIsDesertLanding)
        {
            ArkFace.SetActive(true);
            DialogosText.text = DialogosBase.CineL2_01NavesArkelianas;
            ProtaFace.SetActive(false);
            yield return new WaitForSeconds(5f);
            ArkFace.SetActive(false);
            KruxonFace.SetActive(false);
            DialogosText.text = DialogosBase.CineL2_02Player;
            ProtaFace.SetActive(true);
            yield return new WaitForSeconds(5f);
            ProtaFace.SetActive(false);
            DialogosText.text = DialogosBase.CineL2_03Kruxon;
            KruxonFace.SetActive(true);
            yield return new WaitForSeconds(5f);
            KruxonFace.SetActive(false);
            DialogosText.text = DialogosBase.CineL2_04Player;
            ProtaFace.SetActive(true);
            yield return new WaitForSeconds(5f);
            ProtaFace.SetActive(false);
            DialogosText.text = DialogosBase.CineL2_05Kruxon;
            KruxonFace.SetActive(true);
            yield return new WaitForSeconds(5f);
            KruxonFace.SetActive(false);
            DialogosText.text = DialogosBase.CineL2_05Player;
            ProtaFace.SetActive(true);
            yield return new WaitForSeconds(5f);
            ProtaFace.SetActive(false);
            DialogosText.text = DialogosBase.CineL2_06Kruxon;
            KruxonFace.SetActive(true);
            yield return new WaitForSeconds(5f);
            ProtaFace.SetActive(false);
            DialogosText.text = DialogosBase.CineL2_06Player;
            ProtaFace.SetActive(true);
            yield return new WaitForSeconds(5f);
            ProtaFace.SetActive(false);
            DialogosText.text = DialogosBase.CineL2_02NavesArkelianas;
            ArkFace.SetActive(true);
            yield return new WaitForSeconds(5f);
            ArkFace.SetActive(false);
            KruxonFace.SetActive(false);
            DialogosText.text = DialogosBase.CineL2_07Player;
            ProtaFace.SetActive(true);
            yield return new WaitForSeconds(5f);
            ProtaFace.SetActive(false);
            DialogosText.text = DialogosBase.CineL2_07Kruxon;
            KruxonFace.SetActive(true);
            yield return new WaitForSeconds(5f);
            ProtaFace.SetActive(false);
            KruxonFace.SetActive(false);
            DialogosText.text = " ";
        }
    }
    //Lvl 1 Start
    IEnumerator Lvl1Start ()
    {
        KruxonFace.SetActive(true);
        DialogosText.text = DialogosBase.Lvl1_01Kruxon;
        yield return new WaitForSeconds(5f);
        KruxonFace.SetActive(false);
        DialogosText.text = DialogosBase.Lvl1_02Prota;
        ProtaFace.SetActive(true);
        yield return new WaitForSeconds(2f);
        Eventos.text = DialogosBase.Lvl1_03Evento;

        yield return new WaitForSeconds(2f);
        ProtaFace.SetActive(false);
        DialogosText.text = " ";
    }
    IEnumerator SateliteMuerto ()
    {
        Eventos.text = DialogosBase.Lvl1_04Evento;
        yield return new WaitForSeconds(2f);
        Eventos.text = " ";

        KruxonFace.SetActive(true);
        DialogosText.text = DialogosBase.Lvl1_05Kruxon;
        yield return new WaitForSeconds(5f);
        KruxonFace.SetActive(false);
        ProtaFace.SetActive(true);
        DialogosText.text = DialogosBase.Lvl1_06Prota;
    }
}
