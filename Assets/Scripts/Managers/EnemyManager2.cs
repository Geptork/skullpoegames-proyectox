using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class EnemyManager2 : MonoBehaviour
{
    public Instanciador Instan;
    public float TimeBetwenEnemiesMin = 0.5f;

    [Header ("Enemies Space")]
    public bool BultoxInstanciate = false;
    public bool AstorInstanciate = false;
    public bool NeptoInstanciate = false;
    public bool PileInstance = false;

    [Header ("Enemies Desert")]
    public bool MelforasInstanciate = false;
    public bool FliperIstanciate = false;
    public bool OsisteoInstanciate = false;
    public bool ShalbrowInstanciate = false;



    [Header("Behavoiur")]
    public EnemyBehaviourV2.BehaviourAI BultoxBehaviour = EnemyBehaviourV2.BehaviourAI.Standard;
    public EnemyBehaviourV2.BehaviourAI NeptoBehaviour = EnemyBehaviourV2.BehaviourAI.Sinus;
    public EnemyBehaviourV2.BehaviourAI PilefarnesBehaviour = EnemyBehaviourV2.BehaviourAI.Mine;
    public EnemyBehaviourV2.BehaviourAI BigBultoxBehaviour = EnemyBehaviourV2.BehaviourAI.Standard;
    [Space (10)]
    public EnemyBehaviourV2.BehaviourAI MelforasBehaviour = EnemyBehaviourV2.BehaviourAI.Kamikaze;
    public EnemyBehaviourV2.BehaviourAI FliperBehaviour = EnemyBehaviourV2.BehaviourAI.Mine;
    public EnemyBehaviourV2.BehaviourAI OsisteoBehaviour = EnemyBehaviourV2.BehaviourAI.Standard;
    public EnemyBehaviourV2.BehaviourAI ShalbrowBehaviour = EnemyBehaviourV2.BehaviourAI.Invisible;

    [HideInInspector]
    public float probBeSmart;
    [HideInInspector]
    public float probBeMotorized;
    [HideInInspector]
    public float MotorVelocity;
    [HideInInspector]
    public float ProbBultoxAppear;
    [HideInInspector]
    public float ProbPileAppear;
    [HideInInspector]
    public float ProbAxtorfisAppear;
    [HideInInspector]
    public float ProbNeptoAppear;
    //Desert
    [HideInInspector]
    public float ProbMelforasAppear;
    [HideInInspector]
    public float ProbFliperAppear;
    [HideInInspector]
    public float ProbOsisteoAppear;
    [HideInInspector]
    public float ProbShalbrowAppear;
    //------
    [HideInInspector]
    public float RSpeedXDOWN;
    [HideInInspector]
    public float RSpeedXTOP;
    
    [HideInInspector]
    public float NeptoHealth;
    [HideInInspector]
    public float BultoxHealth;
    [HideInInspector]
    public float BigBultoxHealth;
    [HideInInspector]
    public float PileHealth;
    [HideInInspector]
    public float AstorfixHealth;
    //Desert
    [HideInInspector]
    public float MelforasHealth;
    [HideInInspector]
    public float FliperHealth;
    [HideInInspector]
    public float OsisteoHealth;
    [HideInInspector]
    public float ShalbrowHealth;
    //---
    [HideInInspector]
    public float RSinusSpeedDOWN;
    [HideInInspector]
    public float RSinusSpeedTOP;
    [HideInInspector]
    public float RSinusRadiusDOWN;
    [HideInInspector]
    public float RSinusRadiusTOP;
    [HideInInspector]
    public float RpositionXDOWN;
    [HideInInspector]
    public float RpositionXTOP;
    [HideInInspector]
    public float RpositionYDOWN;
    [HideInInspector]
    public float RpositionYTOP;
    [HideInInspector]
    public float RspeedbulletscaleDOWN;
    [HideInInspector]
    public float RspeedbulletscaleTOP;
    [HideInInspector]
    public float RshootrateDOWN;
    [HideInInspector]
    public float RshootrateTOP;
    [HideInInspector]
    public float RshootdispersionDOWN;
    [HideInInspector]
    public float RshootdispersionTOP;
    [HideInInspector]
    public float RdamageDOWN;
    [HideInInspector]
    public float RdamageTOP;
    private float tdelay = 0.0f;

    [HideInInspector]
    public float frameOffset;
    [HideInInspector]
    public enum EnemyTipe { None, Bultox, Axtorfis, BultoxGigante, Pilefarmes, Satelite, Fliper, Melforas, Osisteo, Shalbrow};
    public enum IAc { None, Fool, Smart};
    [System.Serializable]
    public class EnemyCaracter
    {
        public EnemyTipe WhatEnemy;
        public IAc EnemyIA;
        public EnemyBehaviourV2.BehaviourAI EBAI = EnemyBehaviourV2.BehaviourAI.Standard;
        public float Health = 1000;
        public string tag = "Enemy";
        public float probEnemySmart;
        public float ShakeCamTime = 0.0f;
        public bool ShakeCamOnStart = false;
        public float SpeedXaxis = 0.5f;
        public float positionX;
        public float positionY;
        public float StartFrame;
        public float SinusSpeed;
        public float SinusRadius;
        public float ShootRate;
        public float ShootDispersion;
        private bool thisone;
        public float speedbulletsscale = 0.5f;
        public float damage;
        [HideInInspector]
        public string path = "Raise Error";
        [HideInInspector]
        public string name = "Raise Error";
        [HideInInspector]
        public bool Actived = false;
    }
    [HideInInspector]
    public float currentFrame;
    public List<EnemyCaracter> ArrayEnemies;

    IEnumerator addenemy(string PrefabPath, float speedx, float SinusSpeed, float SinusRadius,  string name, float positionX, float positionY, float speedbulletsscale, float ShootRate, float ShootDispersion, float damage, IAc EnemyIA, bool delayed, EnemyBehaviourV2.BehaviourAI eBAI, float ShakeCamTime, bool ShakeOnStart, float Health, string tag)
    {
        if (delayed) {yield return new WaitForSeconds(tdelay);} else {yield return new WaitForSeconds(0.001f);}
        
        float posx = positionX;
        float posy = positionY;

            Vector3 newpos = new Vector3(posx, posy, 0f);
            GameObject newelement;

            newelement = Instan.GiveMeObject(name); //#INSTANCIADOR
            if (newelement == null) //#INSTANCIADOR
            {
                newelement = (GameObject)Instantiate(Resources.Load(PrefabPath), newpos, transform.rotation);
            }else
            {
                //print("hemos usado el instanciadro para cargar el prefab:" + name);
            }//#INSTANCIADOR
            newelement.transform.position = newpos; //#INSTANCIADOR
            newelement.SetActive(true); //#INSTANCIADOR
            newelement.name = name; //#INSTANCIADOR
            EnemyBehaviourV2 EB;
            EnemyHealthControl EHC;
            EHC= newelement.GetComponent<EnemyHealthControl>();
            EHC.Died = false;
            newelement.tag = tag;
            EHC.SetHealth(Health);
            EB = newelement.GetComponent<EnemyBehaviourV2>();
            if (ShakeOnStart)
            {
                EB.ShakeOfStartTime = ShakeCamTime;
                EB.ShakeOnStart = true;
            }
            EB.behaviour = eBAI;
            EB.speedXaxis = speedx;
            EB.phaseAngleSinus = SinusRadius;
            EB.freqSinus = SinusSpeed;
            WeaponTarget WT;
            GameObject Children;
            Children = newelement.transform.FindChild("Weapon").gameObject;
            WT = Children.GetComponent<WeaponTarget>();
            WT.escaladovelocidad = speedbulletsscale;
            WT.FireRate = ShootRate;
            WT.dispersion = ShootDispersion;
            WT.DamagePerHit = damage;
            if (EnemyIA == IAc.Fool)
            {
                WT._IA = WeaponTarget.EnemyIA.Fool;
            }

            if (EnemyIA == IAc.Smart)
            {
                WT._IA = WeaponTarget.EnemyIA.Smart;
            }
            WT.WakeUp();
        
    }


    public void addblinz(string PrefabPath, string name, float positionX, float positionY, float rotateX, float rotateY, float rotateZ)
    {
        float posx = positionX;
        float posy = positionY;

            GameObject newelement;

            newelement = Instan.GiveMeObject(name); //#INSTANCIADOR
            Vector3 newpos = new Vector3(posx, posy, 0f);

            if (newelement == null)
            {
                newelement = (GameObject)Instantiate(Resources.Load(PrefabPath), newpos, transform.rotation);
            }else
            {
               // print("hemos usado el instanciador para cargar el prefab:" + name);
            }

            newelement.transform.position = newpos; //#INSTANCIADOR
            newelement.name = name;
            newelement.transform.localEulerAngles = new Vector3(rotateX, rotateY, rotateZ);
            newelement.SetActive(true);

    }

        void selectitem(EnemyCaracter Enemy)
    {
        switch (Enemy.WhatEnemy)
        {
            case EnemyTipe.Bultox:
                Enemy.name = "Bultox";
                Enemy.path = "Prefabs/Enemies/Bultox";
                break;
            case EnemyTipe.None:
                Enemy.name = "Bultox";
                Enemy.path = "Prefabs/Enemies/Bultox";
                break;
            case EnemyTipe.Axtorfis:
                Enemy.name = "Axtorfis";
                Enemy.path = "Prefabs/Enemies/Axtorfis";
                break;
            case EnemyTipe.BultoxGigante:
                Enemy.name = "BultoxGigante";
                Enemy.path = "Prefabs/Enemies/BultoxGigante";
                break;
            case EnemyTipe.Pilefarmes:
                Enemy.name = "Pilefarnes";
                Enemy.path = "Prefabs/Enemies/Pilefarnes";
                break;
            case EnemyTipe.Satelite:
                Enemy.name = "Satelite";
                Enemy.path = "Prefabs/Enemies/Satelite";
                break;
            case EnemyTipe.Melforas:
                Enemy.name = "Melforas";
                Enemy.path = "Prefabs/Enemies/Melforas";
                break;
            case EnemyTipe.Fliper:
                Enemy.name = "Fliper";
                Enemy.path = "Prefabs/Enemies/Fliper";
                break;
            case EnemyTipe.Osisteo:
                Enemy.name = "Osisteo";
                Enemy.path = "Prefabs/Enemies/Osisteo";
                break;
            case EnemyTipe.Shalbrow:
                Enemy.name = "Shalbrow";
                Enemy.path = "Prefabs/Enemies/Shalbrow";
                break;
        }
    }
    void Update() {

        currentFrame = Time.timeSinceLevelLoad - frameOffset;

        for (int ev = 0; ev < ArrayEnemies.Count; ev++)
        {
            if (currentFrame >= ArrayEnemies[ev].StartFrame && !ArrayEnemies[ev].Actived)
            {
                selectitem(ArrayEnemies[ev]);
                StartCoroutine(addenemy(ArrayEnemies[ev].path, ArrayEnemies[ev].SpeedXaxis, ArrayEnemies[ev].SinusSpeed, ArrayEnemies[ev].SinusRadius, ArrayEnemies[ev].name, ArrayEnemies[ev].positionX, ArrayEnemies[ev].positionY, ArrayEnemies[ev].speedbulletsscale, ArrayEnemies[ev].ShootRate, ArrayEnemies[ev].ShootDispersion, ArrayEnemies[ev].damage, ArrayEnemies[ev].EnemyIA, false, ArrayEnemies[ev].EBAI, ArrayEnemies[ev].ShakeCamTime, ArrayEnemies[ev].ShakeCamOnStart, ArrayEnemies[ev].Health, ArrayEnemies[ev].tag));
                ArrayEnemies[ev].Actived = true;
            }
        }

        IAc RandomIA;
        if (probBeSmart> UnityEngine.Random.Range(0.0F, 1.0F))
        {
            RandomIA = IAc.Smart;
        }else
        {
            RandomIA = IAc.Fool;
        }

        tdelay = 0.5f;
        //print(BultoxInstanciate);
        if (ProbBultoxAppear > UnityEngine.Random.Range(0.0F, 1.0F) && !BultoxInstanciate){
            //a�adir bultox random 
            float Rspeedx = UnityEngine.Random.Range(RSpeedXDOWN, RSpeedXTOP);
            float RSinusSpeed = UnityEngine.Random.Range(RSinusSpeedDOWN, RSinusSpeedTOP);
            float RSinusRadius = UnityEngine.Random.Range(RSinusRadiusDOWN, RSinusRadiusTOP);
            float RpositionX = UnityEngine.Random.Range(RpositionXDOWN, RpositionXTOP);
            float RpositionY = UnityEngine.Random.Range(RpositionYDOWN, RpositionYTOP);
            float Rspeedbulletsscale = UnityEngine.Random.Range(RspeedbulletscaleDOWN, RspeedbulletscaleTOP);
            float RShootRate = UnityEngine.Random.Range(RshootrateDOWN, RshootrateTOP);
            float RShootDispersion = UnityEngine.Random.Range(RshootdispersionDOWN, RshootdispersionTOP);
            float Rdamage = UnityEngine.Random.Range(RdamageDOWN, RdamageTOP);
            BultoxInstanciate = true;
            string NAME = "Bultox";
            string PATH= "Prefabs/Enemies/Bultox";
            if (probBeMotorized > UnityEngine.Random.Range(0.0F, 1.0F))
            {
                NAME += "Motor";
                PATH += "Motor";
                Rspeedx += MotorVelocity;
            }
            StartCoroutine(addenemy(PATH, Rspeedx, RSinusSpeed, RSinusRadius, NAME , RpositionX, RpositionY, Rspeedbulletsscale, RShootRate, RShootDispersion, Rdamage, RandomIA, true, BultoxBehaviour, 0, false, BultoxHealth, "Enemy"));
            StartCoroutine(SetBoolBultox(TimeBetwenEnemiesMin, false));
        }

        if (probBeSmart > UnityEngine.Random.Range(0.0F, 1.0F))
        {
            RandomIA = IAc.Smart;
        }
        else
        {
            RandomIA = IAc.Fool;
        }

        tdelay += 0.5f;
        if (ProbAxtorfisAppear > UnityEngine.Random.Range(0.0F, 1.0F) && !AstorInstanciate){
            //a�adir Astorfix random
            float Rspeedx = UnityEngine.Random.Range(RSpeedXDOWN, RSpeedXTOP);
            float RSinusSpeed = UnityEngine.Random.Range(RSinusSpeedDOWN, RSinusSpeedTOP);
            float RSinusRadius = UnityEngine.Random.Range(RSinusRadiusDOWN, RSinusRadiusTOP);
            float RpositionX = UnityEngine.Random.Range(RpositionXDOWN, RpositionXTOP);
            float RpositionY = UnityEngine.Random.Range(RpositionYDOWN, RpositionYTOP);
            float Rspeedbulletsscale = UnityEngine.Random.Range(RspeedbulletscaleDOWN, RspeedbulletscaleTOP);
            float RShootRate = UnityEngine.Random.Range(RshootrateDOWN, RshootrateTOP);
            float RShootDispersion = UnityEngine.Random.Range(RshootdispersionDOWN, RshootdispersionTOP);
            float Rdamage = UnityEngine.Random.Range(RdamageDOWN, RdamageTOP);
            AstorInstanciate = true;
            StartCoroutine(addenemy("Prefabs/Enemies/Axtorfis", Rspeedx, RSinusSpeed, RSinusRadius, "Axtorfis", RpositionX, RpositionY, Rspeedbulletsscale, RShootRate, RShootDispersion, Rdamage, RandomIA, true, EnemyBehaviourV2.BehaviourAI.Sinus, 0, false, AstorfixHealth, "Enemy"));
            StartCoroutine(SetBoolAstor(TimeBetwenEnemiesMin, false));
        }

        if (probBeSmart > UnityEngine.Random.Range(0.0F, 1.0F))
        {
            RandomIA = IAc.Smart;
        }
        else
        {
            RandomIA = IAc.Fool;
        }

        tdelay += 0.5f;
        if (ProbNeptoAppear > UnityEngine.Random.Range(0.0F, 1.0F) && ! NeptoInstanciate)
        {
            //a�adir Nepto random
            float Rspeedx = UnityEngine.Random.Range(RSpeedXDOWN, RSpeedXTOP);
            float RSinusSpeed = UnityEngine.Random.Range(RSinusSpeedDOWN, RSinusSpeedTOP);
            float RSinusRadius = UnityEngine.Random.Range(RSinusRadiusDOWN, RSinusRadiusTOP);
            float RpositionX = UnityEngine.Random.Range(RpositionXDOWN, RpositionXTOP);
            float RpositionY = UnityEngine.Random.Range(RpositionYDOWN, RpositionYTOP);
            float Rspeedbulletsscale = UnityEngine.Random.Range(RspeedbulletscaleDOWN, RspeedbulletscaleTOP);
            float RShootRate = UnityEngine.Random.Range(RshootrateDOWN, RshootrateTOP);
            float RShootDispersion = UnityEngine.Random.Range(RshootdispersionDOWN, RshootdispersionTOP);
            float Rdamage = UnityEngine.Random.Range(RdamageDOWN, RdamageTOP);
            NeptoInstanciate = true;
            StartCoroutine(addenemy("Prefabs/Enemies/NaveNeptofaria", Rspeedx, RSinusSpeed, RSinusRadius, "NaveNeptofaria", RpositionX, RpositionY, Rspeedbulletsscale, RShootRate, RShootDispersion, Rdamage, RandomIA, true, NeptoBehaviour, 0, false, NeptoHealth, "Enemy"));
            StartCoroutine(SetBoolNepto(TimeBetwenEnemiesMin, false));

        }

        tdelay += 0.5f;
        if (ProbPileAppear > UnityEngine.Random.Range(0.0F, 1.0F) && !PileInstance)
        {
            //a�adir Pilefarnes random
            float Rspeedx = UnityEngine.Random.Range(RSpeedXDOWN, RSpeedXTOP);
            float RSinusSpeed = UnityEngine.Random.Range(RSinusSpeedDOWN, RSinusSpeedTOP);
            float RSinusRadius = UnityEngine.Random.Range(RSinusRadiusDOWN, RSinusRadiusTOP);
            float RpositionX = UnityEngine.Random.Range(RpositionXDOWN, RpositionXTOP);
            float RpositionY = UnityEngine.Random.Range(RpositionYDOWN, RpositionYTOP);
            float Rspeedbulletsscale = UnityEngine.Random.Range(RspeedbulletscaleDOWN, RspeedbulletscaleTOP);
            float RShootRate = UnityEngine.Random.Range(RshootrateDOWN, RshootrateTOP);
            float RShootDispersion = UnityEngine.Random.Range(RshootdispersionDOWN, RshootdispersionTOP);
            float Rdamage = UnityEngine.Random.Range(RdamageDOWN, RdamageTOP);
            PileInstance = true;
            StartCoroutine(addenemy("Prefabs/Enemies/Pilefarnes", Rspeedx, RSinusSpeed, RSinusRadius, "Pilefarnes", RpositionX, RpositionY, Rspeedbulletsscale, RShootRate, RShootDispersion, Rdamage, RandomIA, true, PilefarnesBehaviour, 0, false, PileHealth, "Armored"));
            StartCoroutine(SetBoolPile(TimeBetwenEnemiesMin, false));
        }

        //Enemigos Desierto (si no va es culpa de David)
        tdelay += 0.5f;
        if (ProbMelforasAppear > UnityEngine.Random.Range(0.0F, 1.0F) && !MelforasInstanciate)
        {
            //a�adir Melforas random
            float Rspeedx = UnityEngine.Random.Range(RSpeedXDOWN, RSpeedXTOP);
            float RSinusSpeed = UnityEngine.Random.Range(RSinusSpeedDOWN, RSinusSpeedTOP);
            float RSinusRadius = UnityEngine.Random.Range(RSinusRadiusDOWN, RSinusRadiusTOP);
            float RpositionX = UnityEngine.Random.Range(RpositionXDOWN, RpositionXTOP);
            float RpositionY = UnityEngine.Random.Range(RpositionYDOWN, RpositionYTOP);
            float Rspeedbulletsscale = UnityEngine.Random.Range(RspeedbulletscaleDOWN, RspeedbulletscaleTOP);
            float RShootRate = UnityEngine.Random.Range(RshootrateDOWN, RshootrateTOP);
            float RShootDispersion = UnityEngine.Random.Range(RshootdispersionDOWN, RshootdispersionTOP);
            float Rdamage = UnityEngine.Random.Range(RdamageDOWN, RdamageTOP);
            MelforasInstanciate = true;
            StartCoroutine(addenemy("Prefabs/Enemies/Melforas", Rspeedx, RSinusSpeed, RSinusRadius, "Melforas", RpositionX, RpositionY, Rspeedbulletsscale, RShootRate, RShootDispersion, Rdamage, RandomIA, true, MelforasBehaviour, 0, false, MelforasHealth, "Enemy"));
            StartCoroutine(SetBoolMelf(TimeBetwenEnemiesMin, false));
        }
        tdelay += 0.5f;
        if (ProbFliperAppear > UnityEngine.Random.Range(0.0F, 1.0F) && !FliperIstanciate)
        {
            //a�adir Fliper random
            float Rspeedx = UnityEngine.Random.Range(RSpeedXDOWN, RSpeedXTOP);
            float RSinusSpeed = UnityEngine.Random.Range(RSinusSpeedDOWN, RSinusSpeedTOP);
            float RSinusRadius = UnityEngine.Random.Range(RSinusRadiusDOWN, RSinusRadiusTOP);
            float RpositionX = UnityEngine.Random.Range(RpositionXDOWN, RpositionXTOP);
            float RpositionY = UnityEngine.Random.Range(RpositionYDOWN, RpositionYTOP);
            float Rspeedbulletsscale = UnityEngine.Random.Range(RspeedbulletscaleDOWN, RspeedbulletscaleTOP);
            float RShootRate = UnityEngine.Random.Range(RshootrateDOWN, RshootrateTOP);
            float RShootDispersion = UnityEngine.Random.Range(RshootdispersionDOWN, RshootdispersionTOP);
            float Rdamage = UnityEngine.Random.Range(RdamageDOWN, RdamageTOP);
            FliperIstanciate = true;
            StartCoroutine(addenemy("Prefabs/Enemies/Fliper", Rspeedx, RSinusSpeed, RSinusRadius, "Fliper", RpositionX, RpositionY, Rspeedbulletsscale, RShootRate, RShootDispersion, Rdamage, RandomIA, true, FliperBehaviour, 0, false, FliperHealth, "Enemy"));
            StartCoroutine(SetBoolFlip(TimeBetwenEnemiesMin, false));
        }
        tdelay += 0.5f;
        if (ProbOsisteoAppear > UnityEngine.Random.Range(0.0F, 1.0F) && !OsisteoInstanciate)
        {
            //a�adir Osisteo random
            float Rspeedx = UnityEngine.Random.Range(RSpeedXDOWN, RSpeedXTOP);
            float RSinusSpeed = UnityEngine.Random.Range(RSinusSpeedDOWN, RSinusSpeedTOP);
            float RSinusRadius = UnityEngine.Random.Range(RSinusRadiusDOWN, RSinusRadiusTOP);
            float RpositionX = UnityEngine.Random.Range(RpositionXDOWN, RpositionXTOP);
            float RpositionY = UnityEngine.Random.Range(RpositionYDOWN, RpositionYTOP);
            float Rspeedbulletsscale = UnityEngine.Random.Range(RspeedbulletscaleDOWN, RspeedbulletscaleTOP);
            float RShootRate = UnityEngine.Random.Range(RshootrateDOWN, RshootrateTOP);
            float RShootDispersion = UnityEngine.Random.Range(RshootdispersionDOWN, RshootdispersionTOP);
            float Rdamage = UnityEngine.Random.Range(RdamageDOWN, RdamageTOP);
            OsisteoInstanciate = true;
            StartCoroutine(addenemy("Prefabs/Enemies/Osisteo", Rspeedx, RSinusSpeed, RSinusRadius, "Osisteo", RpositionX, RpositionY, Rspeedbulletsscale, RShootRate, RShootDispersion, Rdamage, RandomIA, true, OsisteoBehaviour, 0, false, OsisteoHealth, "Enemy"));
            StartCoroutine(SetBoolOsis(TimeBetwenEnemiesMin, false));
        }
        tdelay += 0.5f;
        if (ProbShalbrowAppear > UnityEngine.Random.Range(0.0F, 1.0F) && !ShalbrowInstanciate)
        {
            //a�adir Shalbrow random
            float Rspeedx = UnityEngine.Random.Range(RSpeedXDOWN, RSpeedXTOP);
            float RSinusSpeed = UnityEngine.Random.Range(RSinusSpeedDOWN, RSinusSpeedTOP);
            float RSinusRadius = UnityEngine.Random.Range(RSinusRadiusDOWN, RSinusRadiusTOP);
            float RpositionX = UnityEngine.Random.Range(RpositionXDOWN, RpositionXTOP);
            float RpositionY = UnityEngine.Random.Range(RpositionYDOWN, RpositionYTOP);
            float Rspeedbulletsscale = UnityEngine.Random.Range(RspeedbulletscaleDOWN, RspeedbulletscaleTOP);
            float RShootRate = UnityEngine.Random.Range(RshootrateDOWN, RshootrateTOP);
            float RShootDispersion = UnityEngine.Random.Range(RshootdispersionDOWN, RshootdispersionTOP);
            float Rdamage = UnityEngine.Random.Range(RdamageDOWN, RdamageTOP);
            ShalbrowInstanciate = true;
            StartCoroutine(addenemy("Prefabs/Enemies/Shalbrow", Rspeedx, RSinusSpeed, RSinusRadius, "Shalbrow", RpositionX, RpositionY, Rspeedbulletsscale, RShootRate, RShootDispersion, Rdamage, RandomIA, true, ShalbrowBehaviour, 0, false, ShalbrowHealth, "Enemy"));
            StartCoroutine(SetBoolShal(TimeBetwenEnemiesMin, false));
        }
    }

    void Start()
    {
        NeptoInstanciate = false;
        BultoxInstanciate = false;
        AstorInstanciate = false;
        PileInstance = false;

        MelforasInstanciate = false;
        FliperIstanciate = false;
        OsisteoInstanciate = false;
        ShalbrowInstanciate = false;

        if (Instan == null)
        {
            Instan = GameObject.Find("Instanciador").GetComponent<Instanciador>();
        }
    }

    IEnumerator SetBoolBultox(float waitTime, bool Value)
    {
        yield return new WaitForSeconds(waitTime);
        BultoxInstanciate = Value;
    }

    IEnumerator SetBoolNepto(float waitTime, bool Value)
    {
        yield return new WaitForSeconds(waitTime);
        NeptoInstanciate = Value;
    }

    IEnumerator SetBoolPile(float waitTime, bool Value)
    {
        yield return new WaitForSeconds(waitTime);
        PileInstance = Value;
    }

    IEnumerator SetBoolAstor(float waitTime, bool Value)
    {
        yield return new WaitForSeconds(waitTime);
        AstorInstanciate = Value;
    }
   IEnumerator SetBoolMelf(float waitTime, bool Value)
    {
        yield return new WaitForSeconds(waitTime);
        MelforasInstanciate = Value;
    }
    IEnumerator SetBoolFlip(float waitTime, bool Value)
    {
        yield return new WaitForSeconds(waitTime);
        FliperIstanciate = Value;
    }
    IEnumerator SetBoolOsis(float waitTime, bool Value)
    {
        yield return new WaitForSeconds(waitTime);
        OsisteoInstanciate = Value;
    }
    IEnumerator SetBoolShal(float waitTime, bool Value)
    {
        yield return new WaitForSeconds(waitTime);
        ShalbrowInstanciate = Value;
    }
}
