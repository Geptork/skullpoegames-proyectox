﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Instanciador : MonoBehaviour {

    public bool DEBUG = false;
    [HideInInspector]
    public List<GameObject> Objects;
    [HideInInspector]
    public List<GameObject> PreinstantiatedObjects;
    public List<GameObject> Items;
    public List<GameObject> Enemies;
    public List<GameObject> Bullets;
    public List<GameObject> Particles;
    public List<GameObject> Others;

    private int x = 0;
    private int y = 50;
    private int z = 50;


    private void MergeLists()
    {
        for (int ObjectIndex = 0; ObjectIndex < Items.Count; ObjectIndex++)
        {
            Objects.Add(Items[ObjectIndex]);
        }

        for (int ObjectIndex = 0; ObjectIndex < Enemies.Count; ObjectIndex++)
        {
            Objects.Add(Enemies[ObjectIndex]);
        }

        for (int ObjectIndex = 0; ObjectIndex < Bullets.Count; ObjectIndex++)
        {
            Objects.Add(Bullets[ObjectIndex]);
        }

        for (int ObjectIndex = 0; ObjectIndex < Particles.Count; ObjectIndex++)
        {
            Objects.Add(Particles[ObjectIndex]);
        }

        for (int ObjectIndex = 0; ObjectIndex < Others.Count; ObjectIndex++)
        {
            Objects.Add(Others[ObjectIndex]);
        }

    }

        void Awake()
    {
        MergeLists();
    }

    public GameObject GiveMeObject(string name)
    {
        GameObject ObjectToReturn = null;

        for (int ObjectIndex = 0; ObjectIndex < Objects.Count; ObjectIndex++)
        {
            if (Objects[ObjectIndex] != null)
            {
                if (Objects[ObjectIndex].name == name || Objects[ObjectIndex].name == name + "(clone)")
                {
                    ObjectToReturn = (GameObject)Instantiate(Objects[ObjectIndex], new Vector3(0, 0, 0), transform.rotation);
                    ObjectToReturn.name = name;
                    if (DEBUG)
                    {
                        print("INSTANCIADOR USADO CON EXITO: " + ObjectToReturn.name);
                    }
                    return ObjectToReturn;
                }
            }
        }

        return ObjectToReturn;
    }

}
