using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class EnviromentManager: MonoBehaviour
{
  public List<EnviromentEvent> EnviromentEvents;
  [HideInInspector]
  public float frameOffset;
  public float currentFrame;
  private GameObject Manager;
  private CometasManager _CometasManager;
  private MeteorManager _MeteorManager;

  	public void Start () {
      currentFrame = 0.0F;
      frameOffset = 0.0F;
  	}

    private void ChangeEnviroment(EnviromentEvent Event){
      Manager = GameObject.Find("Managers");
      _CometasManager = Manager.GetComponent<CometasManager>();
      _MeteorManager = Manager.GetComponent<MeteorManager>();
      _CometasManager.RainHeight  = Event.ComRainHeight;
      _CometasManager.MinGravity = Event.ComMinGravity;
        _CometasManager.YSpeed = Event.ComYSpeed;
      _CometasManager.MaxGravity = Event.ComMaxGravity;
      _CometasManager.XStarRain = Event.ComXStarRain;
      _CometasManager.Speed = Event.ComSpeed;
      _CometasManager.Probperframe = Event.ComProbperframe;
      _MeteorManager.RainHeight = Event.METRainHeight;
        _MeteorManager.YSpeed = Event.METYSpeed;
      _MeteorManager.MinGravity = Event.METMinGravity;
      _MeteorManager.MaxGravity = Event.METMaxGravity;
      _MeteorManager.Speed = Event.METSpeed;
      _MeteorManager.Probperframe = Event.METProbperframe;
    }

  	void Update () {
      currentFrame = Time.timeSinceLevelLoad - frameOffset;
       for (int ev = 0; ev < EnviromentEvents.Count; ev++){
        if(currentFrame > EnviromentEvents[ev].Framechange && !EnviromentEvents[ev].Deployed)
            {
          EnviromentEvents[ev].Deployed = true;
          ChangeEnviroment(EnviromentEvents[ev]);
        }
       }
  	}

}
