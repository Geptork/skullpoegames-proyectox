﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Dialogos : MonoBehaviour {

    //Cinematica 1
    public string Cine_01Player;
    public string Cine_02Kruxon;
    public string Cine_03Player;
    //Cinematica 2
    public string CineL2_01NavesArkelianas;
    public string CineL2_02NavesArkelianas;
    public string CineL2_02Player;
    public string CineL2_03Player;
    public string CineL2_04Player;
    public string CineL2_05Player;
    public string CineL2_06Player;
    public string CineL2_07Player;

    public string CineL2_03Kruxon;
    public string CineL2_04Kruxon;
    public string CineL2_05Kruxon;
    public string CineL2_06Kruxon;
    public string CineL2_07Kruxon;

    //Nivel 1
    public string Lvl1_01Kruxon;
    public string Lvl1_02Prota;
    public string Lvl1_03Evento;
    //Matas el Satelite
    public string Lvl1_04Evento;

    public string Lvl1_05Kruxon;
    public string Lvl1_06Prota;
    public string Lvl1_07Kruxon;
    public string Lvl1_08Prota;

    public string Lvl1_09Evento;
    //Alerta del Blinz
    public string Lvl1_10Kruxon;
    public string Lvl1_11Kruxon;
    public string Lvl1_12Prota;

    public string Lvl1_12Evento;
    public string Lvl1_13Evento;

    //Lvl 2: Espacio rojo

    public string Lvl2_13Kruxon;
    public string Lvl2_14Evento;
    public string Lvl2_15Kruxon;
    public string Lvl2_16Prota;
    public string Lvl2_17Kruxon;
    public string Lvl2_18Prota;
    public string Lvl2_19Kruxon;
    public string Lvl2_20Prota;
    public string Lvl2_21Kruxon;

    public string Lvl2_22Evento;
    public string Lvl2_23Evento;

    public string Lvl2_24Kruxon;

    public string Lvl2_25Evento;

    public string Lvl2_26Prota;
    public string Lvl2_27Kruxon;

    //Final lvl 2, to lvl 3
    public string Lvl2_28Naves_ark;
    public string Lvl2_29Prota;
    public string Lvl2_30Kruxon;
    public string Lvl2_31Prota;
    public string Lvl2_32Kruxon;
    public string Lvl2_33Prota;
    public string Lvl2_33Kruxon;
    public string Lvl2_33Naves_ark;
    public string Lvl2_34Prota;
    public string Lvl2_35Kruxon;

    //Lvl 3: Planeta desertico

    //Cine 2
    public string Lvl3_37Naves_ark;
    public string Lvl3_38Prota;
    public string Lvl3_39Kruxon;
    public string Lvl3_40Prota;
    public string Lvl3_41Kruxon;
    public string Lvl3_42Prota;
    public string Lvl3_43Naves_ark;

    //empieza lvl
    public string Lvl3_36Evento;
    //boss 2
    public string Lvl3_44Kruxon;
    public string Lvl3_45Prota;
    public string Lvl3_46Kruxon;
    public string Lvl3_47Prota;
    public string Lvl3_48Kruxon;

    public string Lvl3_49Evento;
    public string Lvl3_50Evento;
    public string Lvl3_51Evento;

    //Final Demo (¿Cine?)
    public string Lvl3_52Naves_ark;
    public string Lvl3_53Kruxon;
    public string Lvl3_54Prota;
    public string Lvl3_55Kruxon;
    public string Lvl3_56Naves_ark;

    void Start ()
    {
        //Cinematica 1
        Cine_01Player = "I just have to scape, left out of here! I just have to find a way to let behind, or I will die!";
        Cine_02Kruxon = "Caution! Biosystem out. Scans out. Sixty seconds left to shotdown.";
        Cine_03Player = "Whats going on?  I must hurry to find a secure place... ";

       //Lvl 1: Red Space
        Lvl1_01Kruxon = "Caution! Biosystem searching enable. Searching biosystems...";
        Lvl1_02Prota = "What is this...? Radar show an automatic route. I am going it";
        Lvl1_03Evento = "Target: Find Biosystem";
        //Matas al Satelite
        Lvl1_04Evento = "Target completed";

        Lvl1_05Kruxon = "Caution! Arkelian biosystem found. Kruxon's systems normalized. Shotdown canceled.";
        Lvl1_06Prota = "That was close";
        Lvl1_07Kruxon = "Caution! System scan completed. Fly system damages. Secondary systems disable.  External energy pick ups are required.";
        Lvl1_08Prota = "I knew it, too much to be real...";

        Lvl1_09Evento = "Target: Find energy pick ups";


//<<<<<<< HEAD
        //Cinematica entrada al desierto
        CineL2_01NavesArkelianas = "Naves arkelianas: Atención, tenemos contacto visual con el objetivo. Pasamos a modo furtivo.";
        CineL2_02Player = "Me han pillado?!  Si pudiera llegar a ese planeta… Debo intentarlo. Es mi única opción de escapar.";
        CineL2_03Kruxon = "Alerta! Planeta detectado. Nombre Niemis. Analizando condiciones: planeta deshabitado; 70% de terreno árido; atmosfera 80% de gas Tronás. Último planeta anexado al tratado de territorios planetarios del sistema Arkeliano.";
        CineL2_04Player = "Un desierto con gas Tronás? Ese gas es irrespirable! Que hago?!";
        CineL2_05Kruxon = "Aviso!El traje posee un generador de anfígenos con capacidad respirable de uso limitado.";
        CineL2_05Player = "Quieres decir que puedo permanecer en el planeta durante un tiempo sin asfixiarme?";
        CineL2_06Kruxon = "El generador solo funciona durante un periodo.Se requiere de suministro constante para un uso de larga duración, hasta completar el proceso de eliminación de toxinas.";
        CineL2_06Player = "Tendré que aguantar la respiración?";
        CineL2_02NavesArkelianas = "Objetivo fijado. Repito, objetivo fijado. Se requieren órdenes para proseguir.";
        CineL2_07Player = " Morir aquí o abajo...";
        CineL2_07Kruxon = "Activando escudos de protección térmica.Escudos activos al 100 %.";

       //Lvl 1: Red Space
       Lvl1_01Kruxon = "Alerta! Encontrado un biosistema compatible, habilitado sistema de búsqueda.";
        Lvl1_02Prota = "¿Pero que está reconfigurando este trasto?";
        Lvl1_03Evento = "Objetivo: Encuentra el biosistema compatible";
        //Matas al Satelite
        Lvl1_04Evento = "Objetivo: Completado";
//=======


        //Alerta del Blinz
        Lvl1_10Kruxon = "Caution!  Radio messages incoming. This is Arkalio-Vega II Fleet. We need back up! We are under attack by unknown creature. Permit to active protocol…";
        Lvl1_11Kruxon = "Present route canceled. Redefine default route. Emergency propulsion system activated.";
        Lvl1_12Prota = "What? Why did you change that? I dont want to go! How I can chance it stupid machine?!";


        Lvl1_12Evento = "Target: Find and destroy the creature";
        Lvl1_13Evento = "Target: completed";


        //Lvl 2: Espacio rojo

        Lvl2_13Kruxon = "Caution! System scan completed! Target system damaged. Fix function is requiered. Initialing basic target system.";

        Lvl2_14Evento = "Target: Find and fix the target sytem";

        Lvl2_15Kruxon = "Planet route found. Checking outer Delxiko system";
        Lvl2_16Prota = "Seems I am alone again, but.. why are they so interested? Why they follow me?";
        Lvl2_17Kruxon = "Caution! Old warzone detected. Trash and dead is on the road. Avalible secondary routes.";
        Lvl2_18Prota = "I prefer the present route. Trashes can hide me";
        Lvl2_19Kruxon = "Redefining default route.";
        Lvl2_20Prota = "Are you get it?!";
        Lvl2_21Kruxon = "Caution! Strange criature incoming";

        Lvl2_22Evento = "Target: Fight and destroy the creature";
        Lvl2_23Evento = "Target: completed";

        Lvl2_24Kruxon = "Caution!  Weapon system  available detected";

        Lvl2_25Evento = "Target: completed";

        Lvl2_26Prota = "Can you stop to say caution?";
        Lvl2_27Kruxon = "Can let me make orders? Caution! Caution! Caution!";

        //Final lvl 2, to lvl 3
        Lvl2_28Naves_ark = "Alert, we made contact with the target. Change to reaper mode";
        Lvl2_29Prota = "Find me again?!  If I could arrive the planet. I must try. My only choice to escape.";
        Lvl2_30Kruxon = "Caution! Planet detected. Name Niemis. Checking status. Population off. 70% desert. 80% Tronás atmosphere. Last planet under Arkelian planetary system";
        Lvl2_31Prota = "A desert with Tronás atmosphere? These is unbreathable! What can I do?!";
        Lvl2_32Kruxon = "Alert! Space suit have a basic air generator";
        Lvl2_33Prota = "Are you saying that I can hold the air on the planet for a while?";
        Lvl2_33Kruxon = "Generator has limited work. Require continue pick ups to complete clean toxic sequence";
        Lvl2_33Naves_ark = "Target engaged. Repeat, target engaged. Waiting orders";
        Lvl2_34Prota = "Die here or there...";
        Lvl2_35Kruxon = "Thermal shields activated. Descending to the planet now";



        //Lvl 3: Planeta desertico
//>>>>>>> 3c96340232bac1fa52e44022b606ad1b54dc30a3

        //Cine 2
        Lvl3_37Naves_ark = "We have the position of the target. Ready to fire";
        Lvl3_38Prota = "This shit hasnt more speed? ";
        Lvl3_39Kruxon = "Caution! The system has converse engine on emergencies.  Extra limit speed. Need elements support. Element detected.";
        Lvl3_40Prota = "Where?  I cant see it";
        Lvl3_41Kruxon = "Tronás gas is element supported by the system. Use Tronás to increase the power.";
        Lvl3_42Prota = "Planet has 80% Tronás atmosphere, isnt? Well... isnt so bad";
        Lvl3_43Naves_ark = "Target going faster! He is leaving our systems!  Change actions requiered!";

        //empieza lvl
        Lvl3_36Evento = "Target I: Find air pickups";

        //boss 2
        Lvl3_44Kruxon = "Caution!, strange creature incoming.";
        Lvl3_45Prota = "Other? Where is it?";
        Lvl3_46Kruxon = "Caution!, strange element detected. Redirecting energy system.";
        Lvl3_47Prota = "No! We are lost without it!";
        Lvl3_48Kruxon = "Caution! Strange element inside attack range. I defined shape";

        Lvl3_49Evento = "Target II: Destroy the shape";
        Lvl3_50Evento = "Target II: completed";
        Lvl3_51Evento = "Target I: completed";

        //Final Demo (¿Cine?)
        Lvl3_52Naves_ark = "We engaged him, ready to fire";
        Lvl3_53Kruxon = "Caution! Converse engine malfunction. Decreasing power.";
        Lvl3_54Prota = "No!";
        Lvl3_55Kruxon = "Warning. Drive system fail. Hold system damaged. Emergency system is activated.";
        Lvl3_56Naves_ark = "Target down, extraction process on";
    }
	
}
