﻿using UnityEngine;
using System.Collections;

public class CineEntradaDesierto : MonoBehaviour
{

    private GameObject Player;
    public GameObject Stalker1;
    public GameObject Stalker2;
    public GameObject Stalker3;
    public GameObject StalkersBullets;
    public GameObject BarritaNegra;
    public GameObject Jetos;
    private Camera cam;
    private float camHeight;
    private float camWidth;
    private ShakeObject PlayerShakeObject;

    private bool SHOOTNOW = false;
    private bool CambioRitmo = false;
    private ShakeObject Nepto1Shaker;
    private ShakeObject Nepto2Shaker;
    private ShakeObject Nepto3Shaker;
    private bool Bullet1Shooted = false;
    private bool Bullet2Shooted = false;
    private bool Bullet3Shooted = false;

    void Awake()
    {
        Jetos = GameObject.Find("Jetos");
        BarritaNegra = GameObject.Find("BarritaNegra");
        Player = this.gameObject;
        PlayerShakeObject = Player.GetComponent<ShakeObject>();
        PlayerShakeObject.ShakeOfStartTime = 900;
        PlayerShakeObject.offsetx = 0;
        PlayerShakeObject.offsety = 0.01f;
        PlayerShakeObject.TranslateX = 0.02f;
        PlayerShakeObject.TranslateY = -0.01f;

        PlayerShakeObject.enabled = true;
        cam = Camera.main;
        camHeight = 2f * cam.orthographicSize;
        camWidth = camHeight * cam.aspect;

        Stalker1 = (GameObject)Instantiate(Stalker1, new Vector3(Player.transform.position.x - 5, 2, 0), transform.rotation);
        Stalker2 = (GameObject)Instantiate(Stalker2, new Vector3(Player.transform.position.x - 5, -2, 0), transform.rotation);
        Stalker3 = (GameObject)Instantiate(Stalker3, new Vector3(Player.transform.position.x - 6, 0.0f, 0), transform.rotation);

        Stalker1.transform.localScale = new Vector3(0.7f, 0.7f, 1f);
        Stalker2.transform.localScale = new Vector3(0.7f, 0.7f, 1f);
        Stalker3.transform.localScale = new Vector3(0.7f, 0.7f, 1f);

        Stalker1.GetComponentInChildren<WeaponTarget>().ShootOnStart = false;
        Stalker2.GetComponentInChildren<WeaponTarget>().ShootOnStart = false;
        Stalker3.GetComponentInChildren<WeaponTarget>().ShootOnStart = false;

        Stalker1.AddComponent<ShakeObject>().enabled = false;
        Stalker2.AddComponent<ShakeObject>().enabled = false;
        Stalker3.AddComponent<ShakeObject>().enabled = false;


        Nepto1Shaker = Stalker1.GetComponent<ShakeObject>();
        Nepto2Shaker = Stalker2.GetComponent<ShakeObject>();
        Nepto3Shaker = Stalker3.GetComponent<ShakeObject>();

        Nepto1Shaker.enabled = true;
        Nepto1Shaker.ShakeOnStart = true;
        Nepto1Shaker.ShakeOfStartTime = 900;
        Nepto1Shaker.TranslateX = 0.03f;
        Nepto1Shaker.TranslateY = 0.00f;
        Nepto1Shaker.offsetx = 0.0f;
        Nepto1Shaker.offsety = 0.01f;

        Nepto2Shaker.enabled = true;
        Nepto2Shaker.ShakeOnStart = true;
        Nepto2Shaker.ShakeOfStartTime = 900;
        Nepto2Shaker.TranslateX = 0.03f;
        Nepto2Shaker.TranslateY = 0.00f;
        Nepto2Shaker.offsetx = 0.0f;
        Nepto2Shaker.offsety = 0.01f;

        Nepto3Shaker.enabled = true;
        Nepto3Shaker.ShakeOnStart = true;
        Nepto3Shaker.ShakeOfStartTime = 900;
        Nepto3Shaker.TranslateX = 0.03f;
        Nepto3Shaker.TranslateY = 0.00f;
        Nepto3Shaker.offsetx = 0.0f;
        Nepto3Shaker.offsety = 0.01f;
    }

    void Start()
    {
        StartCoroutine(ReposicionarCamara(24f));
        StartCoroutine(ShootingOn(2f));
        StartCoroutine(ShootingOFF(16f));
        ParticleSystem Polvo;

        Polvo = GameObject.Find("PolvoDesierto").GetComponent<ParticleSystem>();
        Polvo.emissionRate = 3000;

    }

    // Update is called once per frame
    void Update()
    {

        if (Stalker1.transform.position.x > -camWidth / 4 - 2 && !CambioRitmo)
        {
            PlayerShakeObject.TranslateX = 0.02f;
            PlayerShakeObject.TranslateY = -0.01f;
            Nepto1Shaker.TranslateX = 0.07f;
            Nepto1Shaker.TranslateY = -0.01f;
            Nepto2Shaker.TranslateX = 0.06f;
            Nepto2Shaker.TranslateY = -0.01f;
            CambioRitmo = true;
        }

        if (SHOOTNOW)
        {
            if (!Bullet2Shooted)
            {
                Bullet2Shooted = true;
                GameObject Bullet = (GameObject)Instantiate(StalkersBullets, Stalker2.transform.position + new Vector3(0.15f, 0, 0), transform.rotation);
                StartCoroutine(TimeBBullets2(UnityEngine.Random.Range(1f, 2f)));
                Bullet.GetComponent<SpriteRenderer>().sortingOrder = 1;
                Bullet.GetComponent<SpriteRenderer>().sortingLayerName = "Characters";
            }

            if (!Bullet3Shooted)
            {
                Bullet3Shooted = true;
                GameObject Bullet = (GameObject)Instantiate(StalkersBullets, Stalker3.transform.position + new Vector3(0.15f, 0, 0), transform.rotation);
                StartCoroutine(TimeBBullets3(UnityEngine.Random.Range(1f, 2f)));
                Bullet.GetComponent<SpriteRenderer>().sortingOrder = 0;
                Bullet.GetComponent<SpriteRenderer>().sortingLayerName = "Characters";
            }

            if (!Bullet1Shooted)
            {
                Bullet1Shooted = true;
                GameObject Bullet = (GameObject)Instantiate(StalkersBullets, Stalker1.transform.position + new Vector3(0.15f, 0, 0), transform.rotation);
                StartCoroutine(TimeBBullets1(UnityEngine.Random.Range(1f, 2f)));
                Bullet.GetComponent<SpriteRenderer>().sortingOrder = 1;
                Bullet.GetComponent<SpriteRenderer>().sortingLayerName = "Characters";
            }
        }
    }

    IEnumerator TimeBBullets1(float tdelay)
    {
        yield return new WaitForSeconds(tdelay);
        Bullet1Shooted = false;
    }

    IEnumerator TimeBBullets2(float tdelay)
    {
        yield return new WaitForSeconds(tdelay);
        Bullet2Shooted = false;
    }
    IEnumerator TimeBBullets3(float tdelay)
    {
        yield return new WaitForSeconds(tdelay);
        Bullet3Shooted = false;
    }

    IEnumerator ShootingOn(float tdelay)
    {
        yield return new WaitForSeconds(tdelay);
        SHOOTNOW = true;
    }

    IEnumerator ShootingOFF(float tdelay)
    {
        yield return new WaitForSeconds(tdelay);
        SHOOTNOW = false;
    }

    IEnumerator ReposicionarCamara(float time)
    {

        yield return new WaitForSeconds(time);
        cam.transform.position = new Vector3(17.36f, -13.09f, -10f);
        BarritaNegra.transform.position = new Vector3(17.36f +0.14f, -13.09f - 5.8f, 0);
        Jetos.transform.position = new Vector3(17.36f - 0.528f, -13.09f - 0.270f, 0);
        Player.transform.position = new Vector3(10, -10, 0);
        Stalker3.transform.position = Player.transform.position - new Vector3(5, 0);
        SHOOTNOW = false;
        StartCoroutine(ReposicionarCamara2(17f));
    }

    IEnumerator ReposicionarCamara2(float time)
    {
        ParticleSystem Polvo;

        Polvo = GameObject.Find("PolvoDesierto").GetComponent<ParticleSystem>();
        yield return new WaitForSeconds(time);
        cam.transform.position = new Vector3(35f, 20f, -10f);
        BarritaNegra.transform.position = new Vector3(35f + 0.14f, 20f - 5.8f, 0);
        Jetos.transform.position = new Vector3(35f - 0.528f, 20f - 0.270f, 0);
        Player.transform.position = new Vector3(23.8f, 20f, 0);
        PlayerShakeObject.TranslateY = 0f;
        PlayerShakeObject.offsety = 0.0109f;
        Stalker3.layer = LayerMask.NameToLayer("Player");
        Stalker3.transform.position = Player.transform.position - new Vector3(5, 0.6f);
        SHOOTNOW = false;
        yield return new WaitForSeconds(24f);
        Polvo.emissionRate = 5000;
        yield return new WaitForSeconds(1f);
        Polvo.emissionRate = 7000;
        Nepto3Shaker.TranslateY = 0.002f;
        Nepto3Shaker.TranslateX /= 1.3f;
        yield return new WaitForSeconds(2f);
        Polvo.emissionRate = 9800;
        yield return new WaitForSeconds(5f);
        Nepto3Shaker.offsety = 0.011f;
        Nepto3Shaker.TranslateY = 0.01f;
        Nepto3Shaker.TranslateX /= 1.5f;
        yield return new WaitForSeconds(3f);
        Nepto3Shaker.TranslateY = 0.02f;
    }
}