﻿using UnityEngine;
using System.Collections;

public class gasBehab : MonoBehaviour {
    public bool StartDelayed = false;
    public float StartDelayTime;
    public float ShieldDamage = 1f;
    public float WithoutShieldDamage = 30f;
    public float MaxVelY =1f;
    public float MinVelY = -1f;
    public float MaxVelX = 1f;
    public float MinVelX = -1f;
    public float DelayBetwMovements = 2f;
    private PlayerHealthController PHC;
    public Rigidbody2D RB2D;
    public float StartDisIn = 2f;
    public Disappear DisScrip;

    void Start()
    {
        PHC = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealthController>();
        RB2D = GetComponent<Rigidbody2D>();
        if (!StartDelayed)
        {
            RB2D.velocity = new Vector3(Random.Range(MinVelX, MaxVelX), Random.Range(MinVelY, MaxVelY), 0);
        }
        StartCoroutine(NextMovement());
        if (DisScrip != null)
        {
            StartCoroutine(Disap());
        }

    }
    IEnumerator Disap()
    {
        yield return new WaitForSeconds(StartDisIn);
        DisScrip.enabled = true;
    }
    IEnumerator NextMovement()
    {
        if (StartDelayed)
        {
            yield return new WaitForSeconds(StartDelayTime);
            StartDelayed = false;
        }
        yield return new WaitForSeconds(DelayBetwMovements);
        RB2D.velocity = new Vector3(Random.Range(MinVelX, MaxVelX), Random.Range(MinVelY, MaxVelY), 0);
        StartCoroutine(NextMovement());
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            if (PHC.GetShield() > 0)
            {
                PHC.DoDamage(ShieldDamage);
            }else
            {
                PHC.DoDamage(WithoutShieldDamage);
            }
        }
    }

    }
