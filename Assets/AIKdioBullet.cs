﻿using UnityEngine;
using System.Collections;

public class AIKdioBullet : MonoBehaviour {
    public float DistanceToGoOff = 2f;
    public float HitDamage = 20f;
    private bool GoinOff = false;
    public GameObject ExplosionParticle;
    public GameObject BolsasDeGas;
    public int NumBolsasDeGas;
    [Range(1f,20f)]
    public float ExplosionScale = 6.3f;
    public Rigidbody2D RB2D;
    [HideInInspector]
    public GameObject Player;
    public float CountDown = 2.7f;
    private Vector3 velocity;
    [Range(0.0f, 5f)]
    public float VelocidadInicial = 0.88f;
    [Range(0.05f, 3f)]
    public float SmoothTime = 1.62f;


    // Use this for initialization
    void Start () {
        if(ExplosionScale == 0)
        {
            ExplosionScale = 1F;
        }
        RB2D.velocity = new Vector3(-VelocidadInicial, 0);
        Player = GameObject.FindGameObjectWithTag("Player");
        StartCoroutine(Explosion(CountDown, NumBolsasDeGas));
    }

        IEnumerator Explosion(float Delay, int NumOfGas)
    {
        yield return new WaitForSeconds(Delay);
        GameObject Particula = (GameObject)Instantiate(ExplosionParticle, transform.position, transform.rotation);
        Particula.transform.localScale = new Vector3(ExplosionScale, ExplosionScale, ExplosionScale);
        Destroy(gameObject, 0.2f);

        for (int e = 0; e < NumOfGas; e++)
        {
            GameObject Gas = (GameObject)Instantiate(BolsasDeGas, transform.position, transform.rotation);
        }

    }

    void Update ()
    {
        
            if (Vector2.Distance(transform.position, Player.transform.position) < DistanceToGoOff && !GoinOff){
                GoinOff = true;
                StartCoroutine(Explosion(0.01f, NumBolsasDeGas));
                Player.GetComponent<PlayerHealthController>().DoDamage(HitDamage);
        }

        transform.position = Vector3.SmoothDamp(transform.position, Player.transform.position, ref velocity, 0.5f);

    }


    }
