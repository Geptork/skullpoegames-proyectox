﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AIKdio : MonoBehaviour
{
    public GameObject SmokeParticle = null;
    public GameObject ChispasParticle = null;
    private GameObject ShootPoint = null;
    public GameObject ParticlePoint1 = null;
    public GameObject ParticlePoint2 = null;
    public GameObject ParticlePoint3 = null;
    private bool DificultChanged = false;
    private Vector3 ShootPos = new Vector3(0,0,0);
    private Vector3 ParticlePos = new Vector3(0, 0, 0);
    public bool AwakeOnStart = true;
    public GameObject Bala;
    public SpriteAnimator _SpriteAnimator;
    public EnemyHealthControl _EnemyHealthControl;
    public SpriteRenderer _SpriteRenderer;
    private bool WithoudShield = false;
    private bool WithoudHealth = false;
    [HideInInspector]
    public bool ChangingState = false;
    private bool Naked = false;
    private bool Attacking = false;
    private float XpositionAfterGone = 5.17f;
    public GameObject Enjambre1;
    public GameObject Enjambre2;
    public GameObject Enjambre3;
    public GameObject Enjambre4;
    public GameObject Enjambre5;
    [Header("Primera fase")]
    [Range(0f, 1f)]
    public float ProbAttack = 0.5f;
    [Range(1f, 5f)]
    public float TimeBetweenThoughts = 2f;
    [Header("Segunda fase")]
    [Range(0f, 1f)]
    public float ProbAttack2 = 1f;
    [Range(1f, 5f)]
    public float TimeBetweenThoughts2 = 1.5f;



    void Start()
    {
        if (AwakeOnStart)
        {
            AwakeBeast();
            StartCoroutine(PuestaEnEscena(0.035f, 0.25f));
        }

        if (ShootPoint == null)
        {
            ShootPoint = GameObject.Find("TailShootPoint");
        }


    }

    IEnumerator ChangeAnimation(string AnimatorName, bool Loop, float Delay)
    {
        yield return new WaitForSeconds(Delay);
        _SpriteAnimator.ForcePlay(AnimatorName, Loop);
    }

    IEnumerator ActivarPeques(float Delay)
    {
        yield return new WaitForSeconds(Delay);
        Enjambre1.SetActive(true);
        Enjambre2.SetActive(true);
        Enjambre3.SetActive(true);
        Enjambre4.SetActive(true);
        Enjambre5.SetActive(true);
    }

    IEnumerator Desaparecer(float Delay)
    {
        GetComponent<Rigidbody2D>().velocity = new Vector3(3, 0, 0);
        yield return new WaitForSeconds(Delay);
        //_SpriteRenderer.enabled = false;
        StartCoroutine(Reaparecer(0.035f, 0.25f));

    }
    IEnumerator InstParticle(float Delay, GameObject ParticleToInst, GameObject ParticlePoint , float Scale)
    {
        ParticlePos = new Vector3(ParticlePoint.transform.position.x, ParticlePoint.transform.position.y, ParticlePoint.transform.position.z);
        yield return new WaitForSeconds(Delay);
        GameObject Particula = (GameObject)Instantiate(ParticleToInst, ParticlePos, transform.rotation);
        Particula.transform.localScale = new Vector3(Scale,Scale,Scale);
    }

    IEnumerator PuestaEnEscena(float DelayBewteenMovements, float speed)
    {
        yield return new WaitForSeconds(DelayBewteenMovements);
        transform.position = new Vector3(transform.position.x - speed, transform.position.y, transform.position.z);
        if (transform.position.x >= XpositionAfterGone)
        {
            StartCoroutine(PuestaEnEscena(DelayBewteenMovements, speed));
            ChangingState = true;
            gameObject.tag = "Untagged";
        }
        else
        {
            ChangingState = false;
            gameObject.tag = "Enemy";
        }
    }
    IEnumerator Reaparecer(float DelayBewteenMovements, float speed)
    {
        _SpriteRenderer.enabled = true;
        if (!Naked)
        {
            yield return new WaitForSeconds(3f);
            StartCoroutine(ChangeAnimation("Grande Desnudo Reposo", true, 0f));
            GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
            Naked = true;
        }
        yield return new WaitForSeconds(DelayBewteenMovements);
        transform.position = new Vector3(transform.position.x - speed, transform.position.y, transform.position.z);
        if (transform.position.x >= XpositionAfterGone)
        {
            StartCoroutine(Reaparecer(DelayBewteenMovements, speed));
        }
        else
        {
            ChangingState = false;
            gameObject.tag = "Enemy";
        }

    }


    void Update()
    {
        if (Naked && !DificultChanged)
        {
            DificultChanged = true;
            TimeBetweenThoughts = TimeBetweenThoughts2;
            ProbAttack = ProbAttack2;
        }

        if (!Attacking && !WithoudShield && (_EnemyHealthControl.GetShield() <= 0.0f))
        {
                gameObject.tag = "Untagged";
                ChangingState = true;
                StartCoroutine(ActivarPeques(1f));
            //PARTICULAS DE DESTRUCCION DEL ESCUDO
                StartCoroutine(InstParticle(0.4f, SmokeParticle, ParticlePoint1, 3f));
                StartCoroutine(InstParticle(0.6f, SmokeParticle, ParticlePoint2, 6f));
                StartCoroutine(InstParticle(0.8f, SmokeParticle, ParticlePoint3, 4f));
            WithoudShield = true;
                StartCoroutine(ChangeAnimation("Grande Chaleco Daño Muerte", false, 0.0f));
                StartCoroutine(Desaparecer(1.5f));

        }

        if (!Attacking && !WithoudHealth && (_EnemyHealthControl.GetHealth() <= 0.0f))
        {
            //PARTICULAS DE DESTRUCCION DEL BICHO
            StartCoroutine(InstParticle(0.4f, SmokeParticle, ParticlePoint1, 3f));
            StartCoroutine(InstParticle(0.6f, SmokeParticle, ParticlePoint2, 6f));
            StartCoroutine(InstParticle(0.8f, SmokeParticle, ParticlePoint3, 4f));
            ChangingState = true;
            WithoudHealth = true;
            StartCoroutine(ChangeAnimation("Grande Desnudo Muerte", false, 0.0f));
            Destroy(gameObject, 2.5f);
        }
        //Debug.text = "Escudo = " + _EnemyHealthControl.GetShield() + " Vida = " + _EnemyHealthControl.GetHealth();
    }

    private void AwakeBeast()
    {
        StartCoroutine(Brain(TimeBetweenThoughts));
    }

    IEnumerator Brain(float TimeBetweenThoughts)
    {
        yield return new WaitForSeconds(TimeBetweenThoughts);
        StartCoroutine(Brain(TimeBetweenThoughts));
        if (UnityEngine.Random.Range(0f, 1f) <= ProbAttack && !Attacking)
        {
            StartCoroutine(Attack());
        }
    }

    IEnumerator Attack()
    {
        if (!ChangingState)
        {
            Attacking = true;
            if (!WithoudShield)
            {
                StartCoroutine(ChangeAnimation("Grande Chaleco Ataque", false, 0.0f));
                StartCoroutine(InstParticle(0.95f, SmokeParticle, ShootPoint, 1.5f));
                StartCoroutine(InstanciateBullet(1f));
                yield return new WaitForSeconds(3f); // tiempo de ataque exacto
                StartCoroutine(ChangeAnimation("Grande Chaleco Reposo", true, 0.0f));
            }
            else
            {
                StartCoroutine(ChangeAnimation("Grande Desnudo Ataque", false, 0.0f));
                StartCoroutine(InstParticle(0.95f, SmokeParticle, ShootPoint, 1.5f));
                StartCoroutine(InstanciateBullet(1f));
                yield return new WaitForSeconds(3f); // tiempo de ataque exacto
                StartCoroutine(ChangeAnimation("Grande Desnudo Reposo", true, 0.0f));
            }

            Attacking = false;
        }

    }

    IEnumerator InstanciateBullet(float Delay)
    {
        ShootPos = new Vector3(ShootPoint.transform.position.x, ShootPoint.transform.position.y, ShootPoint.transform.position.z);
        yield return new WaitForSeconds(Delay);
        Instantiate(Bala, ShootPos, transform.rotation);

    }
}
