﻿using UnityEngine;
using System.Collections;

public class SmallKdioIA : MonoBehaviour {

    public EnemyHealthControl EHC;
    public Disappear DisScript;
    public GameObject ParticulaMuerte;
    private bool Dying = false;
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "PlayerShot")
        {
            GameObject Particula = (GameObject)Instantiate(ParticulaMuerte, transform.position, transform.rotation);
            Particula.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            EHC.DoDamage(other.GetComponent<bulletController2>().damage);
            if (other.GetComponent<bulletController2>().destroyOnImpact)
            {
                Destroy(other.gameObject);
            }
        }
    }

    void Update()
    {
        if (EHC.GetHealth() <= 0 && !Dying)
        {
            GameObject Particula = (GameObject)Instantiate(ParticulaMuerte, transform.position, transform.rotation);
            Particula.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
            DisScript.enabled = true;
            Dying = true;
        }
    }
}
